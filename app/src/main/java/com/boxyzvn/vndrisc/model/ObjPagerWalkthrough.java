package com.boxyzvn.vndrisc.model;

public class ObjPagerWalkthrough {
    private String title;
    private int image;

    public ObjPagerWalkthrough(String title, int image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
