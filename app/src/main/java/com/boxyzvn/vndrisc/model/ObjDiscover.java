package com.boxyzvn.vndrisc.model;

import java.io.Serializable;

public class ObjDiscover implements Serializable {
    public String name, address, hour;
    public int image;

    public ObjDiscover(int image, String name, String address, String hour) {
        this.image = image;
        this.name = name;
        this.address = address;
        this.hour = hour;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }
}
