package com.boxyzvn.vndrisc.model;

import java.io.Serializable;

public class ObjNhacNho implements Serializable {
    String time, loiNhac, loiNhacDefault;
    int day[];
    boolean isEnable;
    int code;

    public ObjNhacNho(String time, String loiNhac, int[] day, boolean isEnable, int code) {
        this.time = time;
        this.loiNhac = loiNhac;
        this.day = day;
        this.isEnable = isEnable;
        this.code = code;

    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLoiNhac() {
        return loiNhac;
    }

    public void setLoiNhac(String loiNhac) {
        this.loiNhac = loiNhac;
    }

    public int[] getDay() {
        return day;
    }

    public void setDay(int[] day) {
        this.day = day;
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void setEnable(boolean enable) {
        isEnable = enable;
    }

    public String getLoiNhacDefault() {
        return loiNhacDefault;
    }

    public void setLoiNhacDefault(String loiNhacDefault) {
        this.loiNhacDefault = loiNhacDefault;
    }
}
