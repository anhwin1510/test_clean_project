package com.boxyzvn.vndrisc.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ObjRecommend implements Serializable {
   int risk_level_id;
   String instruction;

    public int getRisk_level_id() {
        return risk_level_id;
    }

    public void setRisk_level_id(int risk_level_id) {
        this.risk_level_id = risk_level_id;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }
}
