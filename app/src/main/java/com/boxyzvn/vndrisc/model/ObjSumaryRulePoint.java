package com.boxyzvn.vndrisc.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ObjSumaryRulePoint implements Serializable {
    String risk_level;
    int risk_level_id;
    String progression_rate;

    public String getRisk_level() {
        return risk_level;
    }

    public void setRisk_level(String risk_level) {
        this.risk_level = risk_level;
    }

    public int getRisk_level_id() {
        return risk_level_id;
    }

    public void setRisk_level_id(int risk_level_id) {
        this.risk_level_id = risk_level_id;
    }

    public String getProgression_rate() {
        return progression_rate;
    }

    public void setProgression_rate(String progression_rate) {
        this.progression_rate = progression_rate;
    }
}
