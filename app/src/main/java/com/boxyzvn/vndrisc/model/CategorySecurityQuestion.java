package com.boxyzvn.vndrisc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategorySecurityQuestion {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("content")
    @Expose
    public String content;

    public CategorySecurityQuestion(int id, String content) {
        this.id = id;
        this.content = content;
    }
}
