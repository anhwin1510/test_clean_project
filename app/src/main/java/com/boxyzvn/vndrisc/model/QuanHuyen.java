package com.boxyzvn.vndrisc.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QuanHuyen {
    @JsonProperty("name_with_type")
    String nameWithType;
    @JsonProperty("name")
    String name;
    @JsonProperty("type")
    String type;

    public String getNameWithType() {
        return nameWithType;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public void setNameWithType(String nameWithType) {
        this.nameWithType = nameWithType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("slug")
    String slug;
    @JsonProperty("path")
    String path;
    @JsonProperty("path_with_type")
    String pathWithType;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("parent_code")
    String parentCode;
    @JsonProperty("code")
    String code;
}
