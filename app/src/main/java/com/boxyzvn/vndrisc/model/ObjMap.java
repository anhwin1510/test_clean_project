package com.boxyzvn.vndrisc.model;

import java.io.Serializable;

public class ObjMap implements Serializable {
    public double lat, lng;
    public int image;
    public float width, height;

    public ObjMap(double lat, double lng, int image, float width, float height) {
        this.lat = lat;
        this.lng = lng;
        this.image = image;
        this.width = width;
        this.height = height;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }
}
