package com.boxyzvn.vndrisc.model;

public class ObjMyCardTitle {
    public int image;
    public String name, content;

    public ObjMyCardTitle(int image, String name, String content) {
        this.image = image;
        this.name = name;
        this.content = content;
    }
}
