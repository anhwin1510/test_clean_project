package com.boxyzvn.vndrisc.model;

import com.fasterxml.jackson.databind.ser.Serializers;

import java.io.Serializable;
import java.util.ArrayList;

public class ObjQuestion implements Serializable {
    String title;
    String content;
    String answer;
    ArrayList<ObjQuestionOption> options;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ArrayList<ObjQuestionOption> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<ObjQuestionOption> options) {
        this.options = options;
    }
}
