package com.boxyzvn.vndrisc.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ObjSumaryRuleExplain implements Serializable {
    ArrayList<String> condition;
    ObjSumaryRulePoint point;

    public ArrayList<String> getCondition() {
        return condition;
    }

    public void setCondition(ArrayList<String> condition) {
        this.condition = condition;
    }

    public ObjSumaryRulePoint getPoint() {
        return point;
    }

    public void setPoint(ObjSumaryRulePoint point) {
        this.point = point;
    }
}
