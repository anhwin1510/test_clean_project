package com.boxyzvn.vndrisc.model;

import com.google.gson.Gson;

import java.io.Serializable;

public class UserInfo implements Serializable {

    private String pass;
    private String name;
    private boolean isCTV;
    private String date_of_birth, phone, province, district, ward, residential, address, code, ward_id,
            securityAnswer;
    private int securityId;

    public UserInfo() {
        phone = "";
        pass = "";
        name = "";
        date_of_birth = "";
        province = "";
        district = "";
        ward = "";
        address = "";
        residential = "";
        code = "";
        isCTV = false;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public UserInfo(boolean isCTV) {
        phone = "";
        pass = "";
        name = "";
        date_of_birth = "";
        province = "";
        district = "";
        ward = "";
        address = "";
        residential = "";
        code = "";
        this.isCTV = isCTV;
    }

    public UserInfo(String name, boolean isCTV, String birthDay, String phone, String diaChi) {
        this.pass = pass;
        this.name = name;
        this.isCTV = isCTV;
        this.date_of_birth = birthDay;
        this.phone = phone;
        this.province = province;
        this.district = district;
        this.ward = ward;
        this.residential = residential;
        this.address = diaChi;
    }

    public String getWard_id() {
        return ward_id;
    }

    public void setWard_id(String ward_id) {
        this.ward_id = ward_id;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResidential() {
        return residential;
    }

    public void setResidential(String residential) {
        this.residential = residential;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public boolean isCTV() {
        return isCTV;
    }

    public void setCTV(boolean CTV) {
        isCTV = CTV;
    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    public int getSecurityId() {
        return securityId;
    }

    public void setSecurityId(int securityId) {
        this.securityId = securityId;
    }
}
