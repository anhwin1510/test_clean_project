package com.boxyzvn.vndrisc.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TinhThanh {

    @JsonProperty("name_with_type")
    String nameWithType;
    @JsonProperty("name")
    String name;
    @JsonProperty("type")
    String type;
    @JsonProperty("slug")
    String slug;

    public String getNameProvince() {
        return nameWithType;
    }

    public void setNameProvince(String nameWithType) {
        this.nameWithType = nameWithType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("code")
    String code;

}
