package com.boxyzvn.vndrisc.model;

import java.util.ArrayList;

public class ObjMyCard {
    public String title;
    public ArrayList<ObjMyCardTitle> listMyCardTitle;

    public ObjMyCard(String title, ArrayList<ObjMyCardTitle> listMyCardTitle) {
        this.title = title;
        this.listMyCardTitle = listMyCardTitle;
    }


}
