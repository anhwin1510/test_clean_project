package com.boxyzvn.vndrisc.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ObjKetQua implements Serializable {
    String title;
    String content;
    String answer;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ObjKetQua(String title, String content, String answer) {
        this.title = title;
        this.content = content;
        this.answer = answer;
    }
}
