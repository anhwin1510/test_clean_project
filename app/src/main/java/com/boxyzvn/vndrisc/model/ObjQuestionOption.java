package com.boxyzvn.vndrisc.model;

import java.io.Serializable;

public class ObjQuestionOption implements Serializable {
    String name;
    String value;
    String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
