package com.boxyzvn.vndrisc.model;

import java.io.Serializable;

public class ObjKhuyenNghi implements Serializable {

    String name, value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
