package com.boxyzvn.vndrisc.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ObjQuestionRule implements Serializable {

    ArrayList<ObjQuestionRuleExplain> explain;
    ArrayList<Integer> question;

    public ArrayList<Integer> getQuestion() {
        return question;
    }

    public void setQuestion(ArrayList<Integer> question) {
        this.question = question;
    }

    public ArrayList<ObjQuestionRuleExplain> getExplain() {
        return explain;
    }

    public void setExplain(ArrayList<ObjQuestionRuleExplain> explain) {
        this.explain = explain;
    }
}
