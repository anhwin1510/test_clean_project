package com.boxyzvn.vndrisc.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ObjSumaryRule implements Serializable {
    ArrayList<ObjSumaryRuleExplain> explain;

    public ArrayList<ObjSumaryRuleExplain> getExplain() {
        return explain;
    }

    public void setExplain(ArrayList<ObjSumaryRuleExplain> explain) {
        this.explain = explain;
    }
}
