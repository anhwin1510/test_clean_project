package com.boxyzvn.vndrisc.utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

    public static String date2String(Date date, String format) {
        return date2String(date, format, true);
    }

    public static int getDateUnit(Date date, int unit) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        return calendar.get(unit);
    }

    public static String date2String(Date date, String format, boolean changeTimeZone) {
        if (date == null) {
            return "";
        }
        String result = "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if (changeTimeZone) {
            calendar.add(Calendar.HOUR, getTimeZoneOffset());
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.JAPAN);
        result = sdf.format(calendar.getTime());
        return result;
    }

    public static Date string2Date(String dateString, String format) {
        Date result = null;
        if (dateString == null) {
            return null;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            result = sdf.parse(dateString);
        } catch (ParseException e) {
            result = new Date();
        }

        return result;
    }

    public static int getTimeZoneOffset() {
        int offset = 0;
        offset = Calendar.getInstance().get(Calendar.ZONE_OFFSET) / 3600000;
        return offset;
    }

    public static Date getCurrentDate() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.add(Calendar.HOUR, -getTimeZoneOffset());
        return calendar.getTime();
    }

    public static Date getCurrentDate(String timezoneString) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(timezoneString));
        return calendar.getTime();
    }


    public static String milliSecondsToDateString(long milliSeconds, String pattern) {
        if (pattern == null) {
            pattern = "yyyy/MM/dd";
        }
        return android.text.format.DateFormat.format(pattern, milliSeconds).toString();
    }

    public static int getPartOfDate(Date date, int part) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        switch (part) {
            case Calendar.DATE:
                return cal.get(part);
            case Calendar.MONTH:
                return cal.get(part) + 1;
            case Calendar.YEAR:
                return cal.get(part);
            default:
                return -1;
        }
    }

    public static boolean isAnotherDay(String date1String, Date date2, String format) {
        long diff = 0;
        Date date1 = DateUtils.string2Date(date1String, format);
        Calendar calendar1 = new GregorianCalendar();
        calendar1.setTime(date1);
        int month1 = calendar1.get(Calendar.MONTH);
        int year1 = calendar1.get(Calendar.YEAR);
        int day1 = calendar1.get(Calendar.DAY_OF_MONTH);

        Calendar calendar2 = new GregorianCalendar();
        calendar2.setTime(date2);
        int month2 = calendar1.get(Calendar.MONTH);
        int year2 = calendar1.get(Calendar.YEAR);
        int day2 = calendar1.get(Calendar.DAY_OF_MONTH);
        if (year1 != year2) {
            return true;
        } else {
            if (month1 != month2) {
                return true;
            } else {
                if (day1 != day2) {
                    return true;
                } else {
                    return false;
                }
            }

        }

    }

    public static Date changeDateFormat(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String dateString = sdf.format(date);
        Date newDate = string2Date(dateString, format);
        return newDate;
    }

    public class DateFormat {
        public static final String yyyyMMdd = "yyyy/MM/dd";
    }

    public static long getNextDateMilli(int nextDay, int hourOfDay, int minute, int second) {
        Date today;
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        today = c.getTime();
        c.setTime(today);
        if (nextDay != -1) {
            c.set(Calendar.DAY_OF_WEEK, nextDay);
        }

        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        dt = c.getTime();
        long milli = dt.getTime() - today.getTime();
        Date daa = new Date();
        daa.setTime(milli);
        Log.e("HUNG DATE ", dt.toString());
        Log.e("HUNG DATE ", today.toString());
        Log.e("HUNG DATE ", milli + "");

        if (milli < 0) {
            if (nextDay != -1) {
                c.add(Calendar.DATE, 7);
            } else {
                c.add(Calendar.DATE, 1);
            }
            dt = c.getTime();
            milli = dt.getTime() - today.getTime();
            Log.e("HUNG DATE after ", dt.toString());
            Log.e("HUNG DATE after", today.toString());
            Log.e("HUNG DATE after", milli + "");
        }

        return milli;
    }

    public static long getNextDateMilli2(int nextDay, int hourOfDay, int minute, int second) {
        Date today;
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        today = c.getTime();
        c.setTime(dt);
//        c.add(Calendar.DATE, nextDay);
//        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
//        c.set(Calendar.MINUTE, minute);
        c.add(Calendar.SECOND, second);
        dt = c.getTime();
        long milli = dt.getTime() - today.getTime();
        return milli;
    }

    public static long getNextDateMilli(long from, int nextDay, int hourOfDay, int minute, int second) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(from);
        c.add(Calendar.DATE, nextDay);
        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, second);

        return c.getTimeInMillis();
    }


    /**
     * 年月日時分秒形式の文字列をタイムスタンプに変換する。
     *
     * @param date yyyyMMddHHmmss形式の文字列。
     * @return タイムスタンプ。
     */
    public static long calendarToTimeMillis(String date) {
        if (date == null) return -1;

        Calendar temp = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Tokyo"));

        try {
            temp.setTime(sdf.parse(date));
        } catch (ParseException e) {
            return -1;
        }
        temp.set(Calendar.MILLISECOND, 0);
        return temp.getTimeInMillis();
    }

}

