package com.boxyzvn.vndrisc.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import com.boxyzvn.vndrisc.model.ObjPagerWalkthrough;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class SharePref {
    public static final String NO_BACKUP_PREF_NAME = "APPNAME_no_backup";
    public static final String PREFNAME = "APPNAME";
    public static final String FCM_ID = "FCM_ID";

    public static void putBoolean(Context context, String key, boolean value) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        return sp.getBoolean(key, defaultValue);
    }

    public static void putArrayListData(Context context, ArrayList<ObjPagerWalkthrough> arrayList) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putString(Key.LIST_SHAKE_DATA, new Gson().toJson(arrayList));
        editor.apply();
    }

    public static ArrayList<ObjPagerWalkthrough> getArrayListData(Context context) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        String shakeData = prefi.getString(Key.LIST_SHAKE_DATA, null);
        String noDataShake = new Gson().toJson(new ArrayList<ObjPagerWalkthrough>());
        if (shakeData == noDataShake || shakeData == null) {
            return null;
        }
        Type type = new TypeToken<ArrayList<ObjPagerWalkthrough>>() {
        }.getType();
        ArrayList<ObjPagerWalkthrough> listShakeData = new Gson().fromJson(shakeData, type);
        return listShakeData;
    }

    public static void putArrayListLotteryClicked(Context context, ArrayList<String> arrayList) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putString(Key.LIST_LOTTERY_CLICKED, new Gson().toJson(arrayList));
        editor.apply();
    }

    public static ArrayList<String> getArrayListLotteryClicked(Context context) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        String shakeData = prefi.getString(Key.LIST_LOTTERY_CLICKED, null);
        if (shakeData == null) {
            return null;
        }
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        ArrayList<String> listLotteryClicked = new Gson().fromJson(shakeData, type);
        return listLotteryClicked;
    }

    public static void putInt(Context context, String key, int value) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static int getInt(Context context, String key, int defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        return sp.getInt(key, defaultValue);
    }

    public static void putString(Context context, String key, String value) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void putStringNoBackUp(Context context, String key, String value) {
        SharedPreferences prefi = context.getSharedPreferences(NO_BACKUP_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getString(Context context, String key, String defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        return sp.getString(key, defaultValue);
    }

    public static String getStringNoBackUp(Context context, String key, String defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(NO_BACKUP_PREF_NAME, Context.MODE_PRIVATE);
        return sp.getString(key, defaultValue);
    }

    public static boolean getBooleanNoBackUp(Context context, String key, boolean defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(NO_BACKUP_PREF_NAME, Context.MODE_PRIVATE);
        return sp.getBoolean(key, defaultValue);
    }

    public static void putBooleanNoBackUp(Context context, String key, boolean value) {
        SharedPreferences prefi = context.getSharedPreferences(NO_BACKUP_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void putLong(Context context, String key, long value) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public static long getLong(Context context, String key, long defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        return sp.getLong(key, defaultValue);
    }

    public static boolean isLogin(Context context) {
        String user = getString(context, Key.ID, "");
        String pass = getString(context, Key.PASS, "");
        return (user != null && user.length() > 0 && pass != null && pass.length() > 0);
    }

    public static void putFloat(Context context, String key, float value) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public static float getFloat(Context context, String key, float defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        return sp.getFloat(key, defaultValue);
    }

    public static void putDouble(Context context, final String key, final double value) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.putLong(key, Double.doubleToRawLongBits(value));
        editor.apply();
    }

    public static double getDouble(Context context, final String key, final double defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        return Double.longBitsToDouble(sp.getLong(key, Double.doubleToLongBits(defaultValue)));
    }

    public static String encrypt(String data, String key) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            byte[] output = cipher.doFinal(data.getBytes("UTF-8"));
            return Base64.encodeToString(output, Base64.NO_PADDING);
        } catch (Exception e) {
            Log.e(PREFNAME, e.toString());
        }

        return null;
    }

    public static String decrypt(String data, String key) {
        try {
            byte[] raw = Base64.decode(data, Base64.NO_PADDING);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            byte[] output = cipher.doFinal(raw);
            return new String(output, "UTF-8");
        } catch (Exception e) {
            Log.e(PREFNAME, e.toString());
        }

        return null;
    }

    public static void remove(Context context, final String key) {
        SharedPreferences prefi = context.getSharedPreferences(PREFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.remove(key);
        editor.apply();
    }

    public static void removeNoBackUP(Context context, final String key) {
        SharedPreferences prefi = context.getSharedPreferences(NO_BACKUP_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefi.edit();
        editor.remove(key);
        editor.apply();
    }

    public final class Key {
        public static final String ID = "ID";
        public static final String PASS = "PASS";
        public static final String IS_FIRST_INSTALLED = "IS_FIRST_INSTALLED";
        //GSON file
        public static final String LIST_SHAKE_DATA = "list_shaked_data";
        public static final String LIST_LOTTERY_CLICKED = "LIST_LOTTERY_CLICKED";


    }

}