package com.boxyzvn.vndrisc.utils;


import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;


import androidx.core.app.NotificationCompat;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.SplashActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.FormatterClosedException;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static android.app.NotificationManager.IMPORTANCE_DEFAULT;
import static android.content.Context.INPUT_METHOD_SERVICE;

public class UtilBase {

    private static final String TAG = UtilBase.class.getSimpleName();


    //TODO get size screen
    public static int getWithScreen(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getHeightScreen(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static int dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    // TODO hide SoftKeyboard and editext
    public static void hideSoftKeyboard(Context context, View view) {
        ((InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    // TODO hide SoftKeyboard and editext
    public static void hideSoftKeyboard(Context context, EditText editText) {
        ((InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    // TODO show SoftKeyboard and editext
    public static void showSoftKeyboard(Context context, EditText editText) {
        editText.requestFocus();
        ((InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE)).showSoftInput(editText, 1);
    }

    public static void sendNotification(Context context, Class<?> classTo, String titile, String messageBody) {
        Intent intent = new Intent(context, classTo);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(titile)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);

        for (ActivityManager.RunningServiceInfo runningServiceInfo : services) {
            Log.d(TAG, String.format("Service:%s", runningServiceInfo.service.getClassName()));
            if (runningServiceInfo.service.getClassName().equals(serviceClass.getName())) {
                return true;
            }
        }
        return false;
    }



    public static Date toDateFromString(String format, String stTime) {
        SimpleDateFormat ft =
                new SimpleDateFormat(format);
        try {
            return ft.parse(stTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String toStringDateJapan(Date date) {
        return toStringDate("yyyy年MM月dd日（E）", date);
    }

    public static String toStringDate(String format, Date date) {
        SimpleDateFormat ft =
                new SimpleDateFormat(format, Locale.JAPAN);
        try {
            return ft.format(date);
        } catch (FormatterClosedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String toStringDateBaseApp(Date date) {
        return toStringDate("yyyy-MM-dd HH:mm:ss", date);
    }

    public static String toStringDateNotice(Date date) {
        return toStringDate("dd/MM/yyyy", date);
    }

    public static Date toTimeDateBaseApp(String date) {
        return toDateFromString("yyyy-MM-dd HH:mm:ss", date);
    }



    public static void pushNotification(Context context, String textContent) {
        final String CHANNEL_ID = "vndrisc_channelId";
        Intent notificationIntent = new Intent(context, SplashActivity.class);
        notificationIntent.putExtra("check_notifi", true);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(SplashActivity.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(context);

        Notification notification = builder
                .setContentText(textContent)
                .setSmallIcon(R.drawable.icon_close)
                .setContentIntent(pendingIntent).build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID);
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    "Channel_VNDRISC",
                    IMPORTANCE_DEFAULT
            );
            notificationManager.createNotificationChannel(channel);
        }
        Random random = new Random();
        int idNotifi = random.nextInt(9999 - 1000) + 1000;
        notificationManager.notify(idNotifi, notification);
    }
}
