package com.boxyzvn.vndrisc.utils;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.ChonGioActivity;

import static com.boxyzvn.vndrisc.sharedpreferences.Constants.TIME_OUT_LOADING;

public class UIHelper {
    private static final String TAG = UIHelper.class.getSimpleName();

    private static android.os.Handler handlerHideProcess = new Handler();
    private static Context mContext;
    private static Runnable runnable = new Runnable() {
        @Override
        public void run() {
            UIHelper.hideProgress();
            GUIUtils.showAlertDialog(mContext, mContext.getString(R.string.timeout_loading));
        }
    };
    public static void getScreenSize(WindowManager wm, int[] size) {
        if (wm == null || size == null || size.length < 2) {
            return;
        }
        DisplayMetrics displaymetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        mScreenW = size[0] = displaymetrics.widthPixels;
        mScreenH = size[1] = displaymetrics.heightPixels;
    }

    private static int mScreenW = 0;
    private static int mScreenH = 0;
    public static int getScreenWidth(WindowManager wm) {
        if (mScreenW <= 0) {
            getScreenSize(wm, new int[2]);
        }
        return mScreenW;
    }
    public static int getScreenHeight(WindowManager wm) {
        if (mScreenH <= 0) {
            getScreenSize(wm, new int[2]);
        }
        return mScreenH;
    }

    private static ProgressDialog mDlg = null;

    private UIHelper() {}

    public static void showProgress(Context context) {
        mContext = context;
        clearHandler();
        hideProgress();
        showProgress(context, null);
        handlerHideProcess.postDelayed(runnable, TIME_OUT_LOADING);

    }

    public static void showProgress(Context context, String message) {
        try {
            hideProgress();
            mDlg = new ProgressDialog(context);
            mDlg.setMessage(message != null ? message : "");
            mDlg.setCancelable(false);
            mDlg.show();
        } catch (Throwable throwable) {}
    }

    public static void hideProgress() {
        if (mDlg != null) {
            clearHandler();
            mDlg.dismiss();
            mDlg = null;
        }
    }

    private static void clearHandler() {
        handlerHideProcess.removeCallbacksAndMessages(null);
        handlerHideProcess.removeCallbacks(runnable);
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
