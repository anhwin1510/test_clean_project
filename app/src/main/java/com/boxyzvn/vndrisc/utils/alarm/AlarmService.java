package com.boxyzvn.vndrisc.utils.alarm;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.boxyzvn.vndrisc.utils.MyApplication;

import static com.boxyzvn.vndrisc.utils.LocalNotificationReceiver.NOTIFICATION_MESSAGE;

public class AlarmService extends Service {
    private static final String TAG = AlarmService.class.getName();

    LocalBroadcastManager broadcastManager;


    public AlarmService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Called by the system when the service is first created.  Do not call this method directly.
     */
    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        String mess = intent.getStringExtra(NOTIFICATION_MESSAGE);
        PushNotificationManager.getInstance().init(getApplicationContext());
        int requestCode = intent.getIntExtra("requestCode", 0);
        if (MyApplication.isActivityVisible()) {
            sendBroadcastReceiver(mess, "", "2_1");
            switch (requestCode) {
                case 101: {

                }
                break;

            }
        } else {

            sendBroadcastReceiverNotification(mess, "", "22");
                    PushNotificationManager.getInstance().pushNotification(mess);
//                    AppApplication.getInstance().showDialogPush(PushNotificationManager.TypePush.PUSH_BEFORE_5M);

        }

        return Service.START_NOT_STICKY;
    }


    /**
     * Called by the system to notify a Service that it is no longer used and is being removed.  The
     * service should clean up any resources it holds (threads, registered
     * receivers, etc) at this point.  Upon return, there will be no more calls
     * in to this Service object and it is effectively dead.  Do not call this method directly.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public interface ConstantsAlarm {
        String AUTO_CHECKOUT_OK = "auto_checkout_ok";
        int REQUESTCODE_AUTOCHECKOUT = 1020;
        int REQUESTCODE_BEFORE_15S = 1021;
        int REQUESTCODE_AFTER_15M = 1022;
        int REQUESTCODE_BEFORE_TIME_END_15M = 1023;
        int REQUESTCODE_TIME_END_COME_BEFORE_15M = 1025;
        int REQUESTCODE_CHECK_IN = 1026;
        int REQUESTCODE_CHECK_OUT = 1027;
        String KEY_REQUEST = "requestCode";
        int COUNT_MINUTE_BEFORE_START = 5;
        int COUNT_MINUTE_BEFORE_END = 5;
    }

    private void sendBroadcastReceiver(String msg, String title, String popupID) {
        Intent intent = new Intent();
        intent.setAction("alarm");
        intent.putExtra("mess", msg);
        intent.putExtra("title", title);
        intent.putExtra("alarm", popupID);
        //type sendTo and url web view save in SharePref
        broadcastManager.sendBroadcast(intent);

    }

    private void sendBroadcastReceiverNotification(String msg, String title, String popupID) {
        Intent intent = new Intent();
        intent.setAction("notification");
        intent.putExtra("mess", msg);
        intent.putExtra("title", title);
        intent.putExtra("alarm", popupID);
        //type sendTo and url web view save in SharePref
        //broadcastManager.sendBroadcast(intent);

    }
}
