package com.boxyzvn.vndrisc.utils.alarm;

import android.content.Context;

import com.boxyzvn.vndrisc.utils.UtilBase;

public class PushNotificationManager {
    public static final int DELAY_TIME = 1000;
    private static PushNotificationManager INSTANCE;

    Context context;


    public static PushNotificationManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PushNotificationManager();
        }
        return INSTANCE;
    }

    public void init(Context context) {
        this.context = context;
    }

    public void pushNotification(String text) {

                UtilBase.pushNotification(context, text);


    }

    public enum TypePush {
        PUSH_BEFORE_5M,
        PUSH_AFTER_START_15M,
        PUSH_BEFORE_TIME_END_15M,
        PUSH_TIME_END_BOOK,
        PUSH_TIME_CHECK_IN,
        PUSH_REMIND_NEW_DAY,
        PUSH_LEFT_15_MIN
    }
}
