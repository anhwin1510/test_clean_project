package com.boxyzvn.vndrisc.utils.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static com.boxyzvn.vndrisc.utils.LocalNotificationReceiver.NOTIFICATION_MESSAGE;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        int code = intent.getIntExtra("requestCode", -1);
        String mess = intent.getStringExtra(NOTIFICATION_MESSAGE);
        Intent intent1 = new Intent(context, AlarmService.class);
        intent1.putExtra("requestCode", code);
        intent1.putExtra(NOTIFICATION_MESSAGE, mess);
        context.startService(intent1);

    }


}
