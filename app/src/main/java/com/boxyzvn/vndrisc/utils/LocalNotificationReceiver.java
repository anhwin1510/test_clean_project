package com.boxyzvn.vndrisc.utils;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.HomeActivity;
import com.boxyzvn.vndrisc.activity.SplashActivity;

import java.util.Calendar;
import java.util.Random;

import static android.app.NotificationManager.IMPORTANCE_DEFAULT;


/**
 * Created by Nguyen Tien Viet on 2/6/2018.
 */

public class LocalNotificationReceiver extends BroadcastReceiver {
    public static final String NOTIFICATION_REQUEST_CODE = "notification_code";
    public static final String FOR_MY_CARD = "スタジアムで選手カードを配信中！スタジアム内にある選手カードを探そう。";
    public final static String NOTIFICATION_ID = "notification-id";
    public final static String NOTIFICATION_MESSAGE = "noti-message";
    public final static String NOTIFICATION_TIME = "noti-time";
    public final static String NOTIFICATION = "notification";
    public final static String NOTIFICATION_LAPLAI = "notification-laplai";
    public final static long[] VIBRATION_PATTERN = {500, 600, 1000, 400, 200, 700};
    private LocalBroadcastManager broadcastManager;

    public void onReceive(Context context, Intent intent) {
        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        broadcastManager = LocalBroadcastManager.getInstance(context);
        int requestCode = intent.getIntExtra(NOTIFICATION_REQUEST_CODE, 0);
        String message = intent.getStringExtra(NOTIFICATION_MESSAGE);
        String time = intent.getStringExtra(NOTIFICATION_TIME);
        boolean lapLai = intent.getBooleanExtra(NOTIFICATION_LAPLAI, true);

        Intent notificationIntent = new Intent(context, LocalNotificationReceiver.class);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION, notification);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_MESSAGE, message);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_TIME, time);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_REQUEST_CODE, requestCode);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_LAPLAI, requestCode);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (lapLai) {
            long futureInMillis = Calendar.getInstance().getTimeInMillis() + 1000 * 60 * 60 * 24 * 7;
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, futureInMillis, pendingIntent);
        }

        handlePushNotify(context, notification, message, time, requestCode);

    }

    private void handlePushNotify(Context context, Notification notification, String mess, String time, int requestCode) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (MyApplication.isActivityVisible()) {
            sendBroadcastReceiver(mess, time);
        } else {
            showNotification(context, mess);
        }
    }

    private void sendBroadcastReceiver(String msg, String time) {
        Intent intent = new Intent();
        intent.setAction("alarm");
        intent.putExtra(NOTIFICATION_MESSAGE, msg);
        intent.putExtra(NOTIFICATION_TIME, time);
        //type sendTo and url web view save in SharePref
        broadcastManager.sendBroadcast(intent);

    }

    private void sendBroadcastReceiverNotification(String msg, String title, String popupID) {
        Intent intent = new Intent();
        intent.setAction("notification");
        intent.putExtra("mess", msg);
        intent.putExtra("title", title);
        intent.putExtra("alarm", popupID);
        //type sendTo and url web view save in SharePref
        broadcastManager.sendBroadcast(intent);

    }

    private void showNotification(Context context, String mess) {
        final String CHANNEL_ID = "vndrisc.channel";
        Intent notificationIntent = new Intent(context, SplashActivity.class);
        notificationIntent.putExtra("check_notifi", true);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(SplashActivity.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID);
        Uri uri =
                Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.sound);

        Notification notification = builder
                .setContentTitle("Nhắc Nhở")
                .setContentText(mess)
                .setAutoCancel(true)
                .setSound(uri)
                .setVibrate(VIBRATION_PATTERN)
                .setSmallIcon(R.drawable.quanlibenh)
                .setContentIntent(pendingIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    "Notification",
                    IMPORTANCE_DEFAULT
            );
            channel.setSound(uri, audioAttributes);
            channel.enableVibration(true);
            channel.setVibrationPattern(VIBRATION_PATTERN);
            notificationManager.createNotificationChannel(channel);

        }
        Random random = new Random();
        int idNotifi = random.nextInt(9999 - 1000) + 1000;
        notificationManager.notify(idNotifi, notification);
    }

}
