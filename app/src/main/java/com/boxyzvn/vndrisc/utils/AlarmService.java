package com.boxyzvn.vndrisc.utils;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.HomeActivity;
import com.boxyzvn.vndrisc.model.ObjNhacNho;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AlarmService extends Service {
    public AlarmService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.


        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        setAlarm();
        return START_NOT_STICKY;
    }

    public void setAlarm() {
        ArrayList<ObjNhacNho> mArrayNhacNho = new Gson().fromJson(SharePref.getInstance().getListLoiNhac(), new TypeToken<ArrayList<ObjNhacNho>>() {
        }.getType());
        for (int i = 0; i < mArrayNhacNho.size(); i++) {
            cancleLocalNotification(mArrayNhacNho.get(i).getCode());
            Date date = DateUtils.string2Date(mArrayNhacNho.get(i).getTime(), "HH:mm");
            Calendar calendar = Calendar.getInstance();
            String hour = mArrayNhacNho.get(i).getTime().substring(0, 2);
            String min = mArrayNhacNho.get(i).getTime().substring(3, 5);
            Log.e("HUNG loi nhac code", hour + min);
            int a = Integer.parseInt(min) + 1;
            Log.e("HUNG loi nhac code min ", a + "");
            for (int j = 0; j <= 6; j++) {

            }
            long milli = DateUtils.getNextDateMilli(1, Integer.parseInt(hour), a, 0);
            scheduleNotification(getNotification(mArrayNhacNho.get(i).getLoiNhac(), "CHANNEL"), mArrayNhacNho.get(i).getLoiNhac(), milli, mArrayNhacNho.get(i).getCode());
        }
    }
    public void cancleLocalNotification(int requestCode) {
        Intent intent = new Intent(this, LocalNotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, requestCode, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }

    public void scheduleNotification(Notification notification, String message, long delay, int requestCode) {
        Intent notificationIntent = new Intent(this, LocalNotificationReceiver.class);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION, notification);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_MESSAGE, message);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_REQUEST_CODE, requestCode);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, requestCode, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT
        );

        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    /**
     * create notification
     *
     * @param message
     * @return
     */
    public Notification getNotification(String message, String notificationID) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.putExtra("PUSH_NOTIFY_ID", notificationID);
        if (message.equals(LocalNotificationReceiver.FOR_MY_CARD)) {
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("SEND_TO", 7);
        } else {

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        Bitmap iconLarge = BitmapFactory.decodeResource(this.getResources(),
                R.drawable.icon_close);

        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);
        int iconId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
        String appName = this.getString(R.string.app_name);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "CHANNEL_VNDRISC")
                .setSmallIcon(iconId)
                //.setLargeIcon(iconLarge)
                .setContentTitle(appName)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message);
        builder.setContentIntent(contentIntent);
        builder.setAutoCancel(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            builder.setColor(Color.BLACK);


        Notification notification = builder.build();
        // LED点灯のフラグを追加する
        notification.flags |= Notification.FLAG_SHOW_LIGHTS;
        //デフォルトの音と振動を指定
        notification.defaults |= Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
        return notification;
    }

}
