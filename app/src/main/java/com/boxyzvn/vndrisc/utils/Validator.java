package com.boxyzvn.vndrisc.utils;

import com.boxyzvn.vndrisc.R;

import static android.text.TextUtils.isEmpty;

public class Validator {
    public static final String REGEX_EMAIL = "^[A-Za-z0-9+_.-]+@(.+)$";
    public static final String REGEX_PASSWORD = "^[a-zA-Z0-9]+$";
    public static final String REGEX_CONTAIN_NUMBER = ".*\\d.*";
    public static final String REGEX_CONTAIN_ALPHABET = ".*[a-zA-Z].*";
    public static final int PASSWORD_MIN_LENGTH = 8;
    public static final int PASSWORD_MAX_LENGTH = 32;

    private String errorMessage;
    private int stringError;
    private boolean valid = true;

    /**
     * @param email
     * @return
     */
    public Validator validateEmail(String email) {
        return validateEmail(email, false);
    }


    /**
     * @param phone
     * @return
     */
    public Validator validatePhone(String phone) {
        return validatePhoneNumber(phone, false);
    }

    /**
     * @param email
     * @param acceptEmpty
     * @return
     */
    public Validator validateEmail(String email, boolean acceptEmpty) {
        if (acceptEmpty && isEmpty(email)) {
            return this;
        }
        if (email != null && !email.matches(REGEX_EMAIL)) {
            valid = false;
        }
        stringError = R.string.validate_email_error;
        return this;
    }

    /**
     * @param phone
     * @param acceptEmpty
     * @return
     */
    public Validator validatePhoneNumber(String phone, boolean acceptEmpty) {
        if (acceptEmpty && isEmpty(phone)) {
            return this;
        }
        if (phone != null && (phone.length() != 10)) {
            valid = false;
        }
        stringError = R.string.validate_phone_error;
        return this;
    }

    /**
     * @param password
     * @return
     */
    public Validator validatePassword(String password) {
        if (password == null || password.length() < PASSWORD_MIN_LENGTH
                || password.length() > PASSWORD_MAX_LENGTH) {
            valid = false;
        } else {
            if (!password.matches(REGEX_PASSWORD) || !password.matches(REGEX_CONTAIN_NUMBER)
                    || !password.matches(REGEX_CONTAIN_ALPHABET)) {
                valid = false;
            }
        }
        stringError = R.string.validate_password_error;
        return this;
    }

    public int getStringError() {
        return stringError;
    }

    public void setStringError(int stringError) {
        this.stringError = stringError;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
