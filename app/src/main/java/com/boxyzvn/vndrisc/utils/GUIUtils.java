package com.boxyzvn.vndrisc.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;


import com.boxyzvn.vndrisc.R;

import java.io.IOException;

public class GUIUtils {

    public static AlertDialog showAlertDialog(Context context, String message) {
        return showAlertDialog(context, message, "Đóng");
    }

    public static AlertDialog showAlertDialog(Context context, String message, String buttonText) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        return alertDialog;
    }

    public static void showAlertDialog(Context context, String message, String buttonText,
                                       DialogInterface.OnClickListener clickButton) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, buttonText, clickButton);
        alertDialog.show();
    }

    public static void closeKeyboard(Activity activity) {
        if (activity != null && activity.getCurrentFocus() != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static AlertDialog createConfirmDialog(Context context, String message,
                                                  String btnPositive, DialogInterface.OnClickListener clickPositive,
                                                  String btnNegative, DialogInterface.OnClickListener clickNegative) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, btnPositive, clickPositive);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, btnNegative, clickNegative);
        return alertDialog;
    }

    public static Dialog createMessageDialog(Activity activity, String title, String message,
                                             String btnPositive, View.OnClickListener clickPositive) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v;
        if(title == null) {
            v = inflater.inflate(R.layout.dialog_message_no_title, null);
        } else {
            v = inflater.inflate(R.layout.dialog_message, null);
            ((TextView) v.findViewById(R.id.dialog_tvTitle)).setText(title);
        }
        ((TextView) v.findViewById(R.id.dialog_tvMessage)).setText(message);
        Button btnOk = v.findViewById(R.id.dialog_btnOK);
        btnOk.setText(btnPositive);
        btnOk.setOnClickListener(clickPositive);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(v);
        Dialog dialog = builder.create();
        dialog.setCancelable(false);
        return dialog;
    }

    public static boolean isNetworkError(Throwable t) {
        return t instanceof IOException;
    }

}
