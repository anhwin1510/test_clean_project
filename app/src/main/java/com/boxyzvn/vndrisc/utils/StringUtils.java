package com.boxyzvn.vndrisc.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class StringUtils {
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    public static boolean isEmpty(CharSequence s) {
        return s == null || s.length() == 0;
    }

    public static boolean isEmpty(String s) {
        return s == null || s.length() == 0;
    }


    public static String calendarToString(Calendar calendar) {
        return calendarToString(calendar, DEFAULT_DATE_FORMAT);
    }

    public static String calendarToString(Calendar calendar, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.JAPANESE);
        return dateFormat.format(calendar.getTime());
    }

    public static Calendar stringToCalendar(String source) throws ParseException {
        return stringToCalendar(source, DEFAULT_DATE_FORMAT);
    }

    public static Calendar stringToCalendar(String source, String format) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.JAPANESE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse(source));
        return calendar;
    }

    public static String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    public static String changeDateFormat(String source, String formatSource, String formatDest) throws ParseException {
        Calendar calendar = stringToCalendar(source, formatSource);
        return calendarToString(calendar, formatDest);
    }

    public static String getSHA1(String clearString) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.update(clearString.getBytes("UTF-8"));
            byte[] bytes = messageDigest.digest();
            StringBuilder buffer = new StringBuilder();
            for (byte b : bytes) {
                buffer.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }
            return buffer.toString();
        } catch (Exception ignored) {
            ignored.printStackTrace();
            return null;
        }
    }
}
