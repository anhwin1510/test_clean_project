package com.boxyzvn.vndrisc.utils.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static com.boxyzvn.vndrisc.utils.LocalNotificationReceiver.NOTIFICATION_MESSAGE;

public class MyAlarmManager {
    public final static long INTERVAL_DAY = AlarmManager.INTERVAL_DAY;
    private static final String TAG = MyAlarmManager.class.getName();
    private Context context;
    private Intent alarmIntent;

    public MyAlarmManager(Context context) {
        this.context = context;
        this.alarmIntent = new Intent(context, AlarmReceiver.class);
    }

    public MyAlarmManager(Intent intent) {

        this.alarmIntent = intent;
    }

    public MyAlarmManager() {
        this.alarmIntent = new Intent(context, AlarmReceiver.class);
    }


    public void startAlarm(int requestCode, long timeMilis, String mess) {
        boolean checkAlarm = false;

        clearAlarm(requestCode);
        alarmIntent.putExtra("requestCode", requestCode);
        alarmIntent.putExtra(NOTIFICATION_MESSAGE, mess);
        PendingIntent pendingIntent = null;
        pendingIntent = PendingIntent.getBroadcast(context, requestCode, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        long futureInMillis = Calendar.getInstance().getTimeInMillis() + timeMilis;
        Date date = new Date(futureInMillis);
        Log.d("HUNG", date.toString() + "code " + requestCode);
        Log.d("HUNG", date.toString() + "FF: " + futureInMillis);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, futureInMillis, 1000 * 60, pendingIntent);
        } else {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, futureInMillis, 1000 * 60, pendingIntent);
        }
    }

    public void clearAlarm(int requestCode) {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, alarmIntent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }


}