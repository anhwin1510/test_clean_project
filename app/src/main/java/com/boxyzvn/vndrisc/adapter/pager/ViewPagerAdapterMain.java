package com.boxyzvn.vndrisc.adapter.pager;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.boxyzvn.vndrisc.model.ObjTab;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewPagerAdapterMain extends FragmentPagerAdapter {

    private ArrayList<ObjTab> listTabs = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    public Map<Integer, Fragment> mFragmentMap = new HashMap<>();
    private final List<Fragment> mFragmentList = new ArrayList<>();

    public ViewPagerAdapterMain(FragmentManager manager) {
        super(manager);
    }

    //This populates your Fragment reference map:
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
        mFragmentMap.put(position, createdFragment);
        return createdFragment;
    }

    @Override
    public Fragment getItem(int position) {
        return listTabs.get(position).getFragment();
    }

    @Override
    public int getCount() {
        return listTabs.size();
    }

    public void addFragment(ArrayList<ObjTab> listViewPager) {
        listTabs = listViewPager;
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return listTabs.get(position).getTitle();
    }
}

