package com.boxyzvn.vndrisc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.model.QuanHuyen;
import com.boxyzvn.vndrisc.model.TinhThanh;

import java.util.ArrayList;

public class SimpleAddressAdapter extends RecyclerView.Adapter<SimpleAddressAdapter.SimpleViewHolder> {

    private ArrayList<TinhThanh> listAddress;
    private ArrayList<QuanHuyen> listQuanHuyen;
    private ArrayList<QuanHuyen> listXa;
    private Context context;
    private int level;
    private SimpleAddressAdapter.ItemClickListener mClickListener;

    public SimpleAddressAdapter(Context context, ArrayList<TinhThanh> listAddress, int level) {
        this.context = context;
        this.listAddress = listAddress;
        this.level = level;

    }


    public SimpleAddressAdapter(ArrayList<QuanHuyen> listQuanHuyen, Context context, int level) {
        this.listQuanHuyen = listQuanHuyen;
        this.context = context;
        this.level = level;
    }

    @NonNull
    @Override
    public SimpleAddressAdapter.SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address, parent, false);
        return new SimpleAddressAdapter.SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, int position) {
        switch (level) {
            case 1:
                String address = listAddress.get(position).getNameProvince();
                holder.address.setText(address);
                break;
            case 2:
                String quan = listQuanHuyen.get(position).getNameWithType();
                holder.address.setText(quan);
                break;
        }

    }

    @Override
    public int getItemCount() {
        if (listAddress != null) {
            return listAddress.size();
        } else return listQuanHuyen.size();

    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView address;

        public SimpleViewHolder(@NonNull View itemView) {
            super(itemView);
            address = (TextView) itemView.findViewById(R.id.tvItemAddress);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) {
                mClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    // allows clicks events to be caught
    public void setClickListener(SimpleAddressAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
