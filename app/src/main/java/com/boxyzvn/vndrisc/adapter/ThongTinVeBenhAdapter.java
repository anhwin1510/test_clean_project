package com.boxyzvn.vndrisc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.model.ObjDiseaseInfo;
import com.boxyzvn.vndrisc.utils.UtilBase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ThongTinVeBenhAdapter extends RecyclerView.Adapter<ThongTinVeBenhAdapter.DiseaseInfoViewHolder> {

    private List<ObjDiseaseInfo> mListDiseaseInfo;
    private Context context;
    private ItemClickListener mClickListener;

    public ThongTinVeBenhAdapter(Context context, ArrayList<ObjDiseaseInfo> mListDiseaseInfo) {
        this.context = context;
        this.mListDiseaseInfo = mListDiseaseInfo;
    }

    @NonNull
    @Override
    public DiseaseInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_disease_info_list, parent, false);
        return new DiseaseInfoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DiseaseInfoViewHolder holder, int position) {
        ObjDiseaseInfo diseaseInfo = mListDiseaseInfo.get(position);
        holder.title.setText(diseaseInfo.title);
        holder.content.setText(diseaseInfo.description);
        Date date = UtilBase.toDateFromString("yyyy-MM-dd", diseaseInfo.date);
        holder.date.setText(UtilBase.toStringDateNotice(date));
    }

    @Override
    public int getItemCount() {
        return mListDiseaseInfo.size();
    }

    public class DiseaseInfoViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        public TextView title, content, date;

        public DiseaseInfoViewHolder(@NonNull View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tvTitle);
            content = (TextView) itemView.findViewById(R.id.tvContent);
            date = (TextView) itemView.findViewById(R.id.tvDate);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) {
                mClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }
    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
