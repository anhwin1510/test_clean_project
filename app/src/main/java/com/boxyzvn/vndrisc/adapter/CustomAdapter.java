package com.boxyzvn.vndrisc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.Toast;

import com.boxyzvn.vndrisc.R;

public class CustomAdapter extends BaseAdapter {
    int[] days;
    Context context;
    LayoutInflater inflter;
    String value;

    public CustomAdapter(Context context, int[] days) {
        this.context = context;
        this.days = days;
        inflter = (LayoutInflater.from(context));

    }

    @Override
    public int getCount() {
        return days.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public int[] getDays() {
        return days;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.item_ngay, null);
        final CheckedTextView simpleCheckedTextView = (CheckedTextView) view.findViewById(R.id.cTextThu2);
        if (days[position] == 1) {
            simpleCheckedTextView.setCheckMarkDrawable(R.drawable.yes_no);
            simpleCheckedTextView.setChecked(true);
        }
        setTextListView(position, simpleCheckedTextView);
// perform on Click Event Listener on CheckedTextView
        simpleCheckedTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (simpleCheckedTextView.isChecked()) {
// set cheek mark drawable and set checked property to false
                    value = "un-Checked";
                    simpleCheckedTextView.setCheckMarkDrawable(0);
                    simpleCheckedTextView.setChecked(false);
                    days[position] = 0;
                } else {
// set cheek mark drawable and set checked property to true
                    value = "Checked";
                    simpleCheckedTextView.setCheckMarkDrawable(R.drawable.yes_no);
                    simpleCheckedTextView.setChecked(true);
                    days[position] = 1;
                }
            }
        });
        return view;
    }

    private void setTextListView(int position, CheckedTextView simpleCheckedTextView) {
        if (position == 0)
            simpleCheckedTextView.setText("Thứ 2");
        if (position == 1)
            simpleCheckedTextView.setText("Thứ 3");
        if (position == 2)
            simpleCheckedTextView.setText("Thứ 4");
        if (position == 3)
            simpleCheckedTextView.setText("Thứ 5");
        if (position == 4)
            simpleCheckedTextView.setText("Thứ 6");
        if (position == 5)
            simpleCheckedTextView.setText("Thứ 7");
        if (position == 6)
            simpleCheckedTextView.setText("Chủ Nhật");
    }
}