package com.boxyzvn.vndrisc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.boxyzvn.vndrisc.activity.NhacNhoActivity;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.model.ObjNhacNho;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class LoiNhacAdapter extends RecyclerView.Adapter<LoiNhacAdapter.NhacNhoViewHolder> {

    public static final int ENABLE_ALARM = 1;
    public static final int SUNDAY = 6;
    private List<ObjNhacNho> mListDiscover;
    private Context context;
    private LoiNhacAdapter.ItemClickListener mClickListener;

    public LoiNhacAdapter(Context context, ArrayList<ObjNhacNho> mListDiscover) {
        this.context = context;
        this.mListDiscover = mListDiscover;

    }

    @NonNull
    @Override
    public NhacNhoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nhac_nho, parent, false);
        return new NhacNhoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NhacNhoViewHolder holder, int position) {
        ObjNhacNho discover = mListDiscover.get(position);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(20));
        holder.time.setText(mListDiscover.get(position).getTime());
        holder.loiNhac.setText(mListDiscover.get(position).getLoiNhac());
        int day[] = mListDiscover.get(position).getDay();
        String ngayNhacNho = "";

        //Biến check có chọn ngày không
        boolean checkDay = false;

        boolean isAllDay = true;
        for (int i = 0; i < day.length; i++) {
            if (day[i] == ENABLE_ALARM) {
                checkDay = true;
                if (i == SUNDAY) {
                    ngayNhacNho += "CN";
                } else {
                    ngayNhacNho += "Thứ " + (i + 2) + " ";
                }
            } else {
                isAllDay = false;
            }
        }
        if (isAllDay) {
            holder.day.setText("Hằng ngày");
        } else if (!checkDay) {
            holder.day.setText("Không lặp lại");
        } else {
            holder.day.setText(ngayNhacNho);
        }
        if (mListDiscover.get(position).isEnable()) {
            setStatusLayout(holder, true, R.color.black);
        } else {
            setStatusLayout(holder, false, R.color.gray_dark);
        }

        holder.switchCompat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.switchCompat.isChecked()) {
                    mListDiscover.get(position).setEnable(false);
                    setStatusLayout(holder, false, R.color.gray_dark);
                    SharePref.getInstance().setListLoiNhac(new Gson().toJson(mListDiscover));
                    ((NhacNhoActivity) context).refreshAlarm();
                } else {
                    mListDiscover.get(position).setEnable(true);
                    setStatusLayout(holder, true, R.color.black);
                    SharePref.getInstance().setListLoiNhac(new Gson().toJson(mListDiscover));
                    ((NhacNhoActivity) context).refreshAlarm();
                }
            }
        });
    }

    private void setStatusLayout(@NonNull NhacNhoViewHolder holder, boolean b, int p) {
        holder.switchCompat.setChecked(b);
        holder.time.setTextColor(context.getColor(p));
        holder.loiNhac.setTextColor(context.getColor(p));
        holder.day.setTextColor(context.getColor(p));
    }

    @Override
    public int getItemCount() {
        return mListDiscover.size();
    }

    public List<ObjNhacNho> getListItem() {
        return mListDiscover;
    }

    public class NhacNhoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public SwitchCompat switchCompat;
        public TextView time, day, loiNhac;
        public RelativeLayout relativeLayout, linearLayout;

        NhacNhoViewHolder(@NonNull View itemView) {
            super(itemView);
            time = (TextView) itemView.findViewById(R.id.tvTime);
            day = (TextView) itemView.findViewById(R.id.tvDay);
            loiNhac = (TextView) itemView.findViewById(R.id.tvLoiNhac);
            switchCompat = (SwitchCompat) itemView.findViewById(R.id.swLoiNhac);
            linearLayout = (RelativeLayout) itemView.findViewById(R.id.layout_foreground);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.layout_background);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) {
                mClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    // allows clicks events to be caught
    public void setClickListener(LoiNhacAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void removeItem(int position) {
        mListDiscover.remove(position);
        notifyItemRemoved(position);
    }
}
