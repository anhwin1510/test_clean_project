package com.boxyzvn.vndrisc.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.boxyzvn.vndrisc.R;

public class Temp2Fragment extends Fragment {

    public Activity activity;
    private View view;

    public Temp2Fragment() {

    }

    public static Temp2Fragment newInstance() {

        Bundle args = new Bundle();

        Temp2Fragment fragment = new Temp2Fragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup viewGroupParent = (ViewGroup) view.getParent();
            if (viewGroupParent != null)
                viewGroupParent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_2_map, container, false);
        } catch (Exception e) {

        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setViewID();
    }



    private void setViewID() {
        activity = getActivity();
    }

}

