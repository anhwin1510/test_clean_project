package com.boxyzvn.vndrisc.fragment.register.registerCTV;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.HomeActivity;

import com.boxyzvn.vndrisc.activity.HomeCTVActivity;
import com.boxyzvn.vndrisc.activity.login.LoginCTVActivity;
import com.boxyzvn.vndrisc.activity.register.ctv.RegisterCTVActivity;
import com.boxyzvn.vndrisc.activity.register.ctv.RegisterCTVAddressActivity;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBack;
import com.boxyzvn.vndrisc.model.ObjMap;
import com.boxyzvn.vndrisc.model.TinhThanh;
import com.boxyzvn.vndrisc.model.UserInfo;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.GUIUtils;
import com.boxyzvn.vndrisc.utils.UIHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import static com.boxyzvn.vndrisc.utils.StringUtils.loadJSONFromAsset;

public class RegisterInfoCTVFragment extends Fragment {

    public static final int REGISTER_ADDRESS = 100;
    public static final String EDIT_PHUONG = "PHUONG";
    public ArrayList<ObjMap> arrayList = new ArrayList<>();
    private TextView tvThanhPho, tvQuan, tvPhuong;
    public Activity activity;
    private View view;
    private Calendar selectedDate;
    private TextView edtBirhDay, edtName;
    private Button btnNext;
    private List<String> tinhThanh = new ArrayList<>();

    public RegisterInfoCTVFragment() {

    }

    public static RegisterInfoCTVFragment newInstance() {

        Bundle args = new Bundle();

        RegisterInfoCTVFragment fragment = new RegisterInfoCTVFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup viewGroupParent = (ViewGroup) view.getParent();
            if (viewGroupParent != null)
                viewGroupParent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_register_ctv_info, container, false);
            tvThanhPho = view.findViewById(R.id.tvTinh);
            btnNext = view.findViewById(R.id.btnCompleteRegister);
            edtName = view.findViewById(R.id.edtName);
            tvQuan = view.findViewById(R.id.tvQuan);
            tvPhuong = view.findViewById(R.id.tvPhuong);
            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserInfo userInfo = ((RegisterCTVActivity) getActivity()).getUserInfo();
                    userInfo.setName(edtName.getText().toString());
                    userInfo.setDate_of_birth(edtBirhDay.getText().toString());
                    if (userInfo.getName().isEmpty() || userInfo.getDistrict().isEmpty() || userInfo.getWard().isEmpty()
                            || userInfo.getDate_of_birth().isEmpty()) {
                        GUIUtils.showAlertDialog(getActivity(), getString(R.string.validate_info_error));
                    } else {

                        String appVer = "";
                        try {
                            appVer = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName;
                        } catch (PackageManager.NameNotFoundException e) {

                        }

                        String androidId = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                        //TODO save userInfo
                        ApiServiceManager.getInstance().registerCTV(userInfo, appVer, androidId, new CallBack() {
                            @Override
                            public void onComplete() {
                                showDialogSuccess();
                            }

                            @Override
                            public void onError(String errorMsg) {
                                GUIUtils.showAlertDialog(getActivity(), errorMsg);
                                Log.d("HUNG error", errorMsg);
                            }
                        });


                    }
                }
            });

            tvQuan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UIHelper.showProgress(getContext());
                    Intent intent = new Intent(getContext(), RegisterCTVAddressActivity.class);
                    UserInfo userInfo = ((RegisterCTVActivity) getActivity()).getUserInfo();
                    userInfo.getPhone();
                    intent.putExtra("userPhone", userInfo);
                    startActivityForResult(intent, REGISTER_ADDRESS);
                }
            });

            tvPhuong.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!tvQuan.getText().toString().equals("")) {
                        UIHelper.showProgress(getActivity());
                        Intent intent = new Intent(getContext(), RegisterCTVAddressActivity.class);
                        intent.setAction(EDIT_PHUONG);
                        intent.putExtra("userPhone", ((RegisterCTVAddressActivity) getActivity()).getUserInfo());
                        startActivityForResult(intent, REGISTER_ADDRESS);
                    } else {
                        Toast.makeText(activity, "Vui lòng chọn Quận/Huyện", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            edtBirhDay = view.findViewById(R.id.edtBirthDay);
            setupDatetimePicker();

        } catch (Exception e) {

        }
        setupComboxProvince();

        return view;
    }

    public void showDialogSuccess(){
        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setMessage(getString(R.string.content_register_success));
        alertDialog.setTitle(getString(R.string.title_register_success));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(getActivity(), LoginCTVActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        startActivity(intent);
                    }
                });
        alertDialog.show();
    }

    private void setupComboxProvince() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JSONObject data = new JSONObject(loadJSONFromAsset(getContext(), "tinhthanh.json"));

            Iterator<String> iter = data.keys();
            while (iter.hasNext()) {
                String spot = iter.next();
                JSONObject value = data.getJSONObject(spot);
                TinhThanh entity = mapper.readValue(value.toString(), TinhThanh.class);
                Log.d("HUNG ", entity.getNameProvince());
                tinhThanh.add(entity.getNameProvince());

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, tinhThanh);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            }
        } catch (JSONException | IOException e) {
            Log.d("HUNG ", e.toString());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setViewID();
    }

    private void setupDatetimePicker() {
        final DatePickerDialog.OnDateSetListener callbackSetDate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                selectedDate.set(Calendar.YEAR, year);
                selectedDate.set(Calendar.MONTH, monthOfYear);
                selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                monthOfYear++;
                edtBirhDay.setText("" + year + "-" + monthOfYear + "-" + dayOfMonth);
            }

        };

        edtBirhDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedDate = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        android.R.style.Theme_Holo_Light_Dialog_NoActionBar, callbackSetDate,
                        selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH),
                        selectedDate.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
    }

    private void setViewID() {
        activity = getActivity();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK)
            return;
        if (requestCode == REGISTER_ADDRESS) {
            UIHelper.hideProgress();
            UserInfo userAddress = (UserInfo) data.getSerializableExtra("userAddress");

            if (tvQuan.getText().toString().isEmpty())
                tvQuan.setText(userAddress.getDistrict());
            tvPhuong.setText(userAddress.getWard());
            Log.d("HUNG data", new Gson().toJson(userAddress));
            ((RegisterCTVActivity) getActivity()).setUserInfo(userAddress);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        UIHelper.hideProgress();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}

