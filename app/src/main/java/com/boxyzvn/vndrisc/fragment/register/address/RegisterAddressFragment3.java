package com.boxyzvn.vndrisc.fragment.register.address;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.register.user.RegisterAddressActivity;
import com.boxyzvn.vndrisc.adapter.SimpleAddressAdapter;
import com.boxyzvn.vndrisc.model.QuanHuyen;
import com.boxyzvn.vndrisc.model.TinhThanh;
import com.boxyzvn.vndrisc.model.UserInfo;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.UIHelper;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import static com.boxyzvn.vndrisc.utils.StringUtils.loadJSONFromAsset;

public class RegisterAddressFragment3 extends Fragment implements SimpleAddressAdapter.ItemClickListener {

    private RecyclerView rvAddress;
    private ArrayList<TinhThanh> tinhThanh = new ArrayList<>();
    private ArrayList<QuanHuyen> quanHuyens = new ArrayList<>();
    public Activity activity;
    private String parentCode;
    private int level = 0;
    private View view;

    public RegisterAddressFragment3() {

    }

    public static RegisterAddressFragment3 newInstance() {

        Bundle args = new Bundle();

        RegisterAddressFragment3 fragment = new RegisterAddressFragment3();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup viewGroupParent = (ViewGroup) view.getParent();
            if (viewGroupParent != null)
                viewGroupParent.removeView(view);
        }
        parentCode = SharePref.getInstance().quanSelected();
        try {
            view = inflater.inflate(R.layout.fragment_user_info_address, container, false);
            rvAddress = view.findViewById(R.id.rv_address);
            Button btnNext = view.findViewById(R.id.btnNext);
            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        } catch (Exception e) {

        }
        setupRecycleView();

        return view;
    }

    private void setupRecycleView() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JSONObject data = new JSONObject(loadJSONFromAsset(getContext(), "xa_phuong.json"));

            Iterator<String> iter = data.keys();
            while (iter.hasNext()) {
                String spot = iter.next();
                JSONObject value = data.getJSONObject(spot);
                QuanHuyen entity = mapper.readValue(value.toString(), QuanHuyen.class);
                if (entity.getParentCode().equals(parentCode)) {
                    quanHuyens.add(entity);
                    Log.d("HUNG ", entity.getNameWithType());
                }

                SimpleAddressAdapter simpleAdapter = new SimpleAddressAdapter(quanHuyens, getContext(), 2);
                rvAddress.setLayoutManager(new LinearLayoutManager(getContext()));
                simpleAdapter.setClickListener(RegisterAddressFragment3.this);
                rvAddress.setAdapter(simpleAdapter);

            }
        } catch (JSONException | IOException e) {
            Log.d("HUNG ", e.toString());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setViewID();
    }

    private void setViewID() {
        activity = getActivity();
    }

    @Override
    public void onItemClick(View view, int position) {
        UserInfo userInfo = ((RegisterAddressActivity) getActivity()).getUserInfo();
        userInfo.setWard(quanHuyens.get(position).getName());
        ((RegisterAddressActivity) getActivity()).setUserInfo(userInfo);
        Intent intent = new Intent();
        intent.putExtra("userAddress", userInfo);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        UIHelper.hideProgress();
    }
}

