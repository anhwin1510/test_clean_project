package com.boxyzvn.vndrisc.fragment.forgot;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.ForgotPassActivity;
import com.boxyzvn.vndrisc.activity.login.LoginActivity;
import com.boxyzvn.vndrisc.activity.login.LoginCTVActivity;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBack;
import com.boxyzvn.vndrisc.utils.GUIUtils;
import com.boxyzvn.vndrisc.utils.Validator;

public class CreatePassFragment extends Fragment {
    private View view;
    private EditText edtPassword, edtRePassword;
    private Button btnSave;
    private ImageView imgHidePass;
    private ImageView imgHideRePass;

    public CreatePassFragment() {

    }

    public static CreatePassFragment newInstance() {

        Bundle args = new Bundle();

        CreatePassFragment fragment = new CreatePassFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Validator validatePass() {
        return new Validator()
                .validatePassword(edtPassword.getText().toString());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup viewGroupParent = (ViewGroup) view.getParent();
            if (viewGroupParent != null)
                viewGroupParent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_create_pass, container, false);
            edtPassword = view.findViewById(R.id.edtPassword);
            edtRePassword = view.findViewById(R.id.edtRePassword);

            imgHidePass = view.findViewById(R.id.imgHidePass);
            imgHideRePass = view.findViewById(R.id.imgHideRePass);
            imgHidePass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkStatusEdtPass(edtPassword, imgHidePass);

                }
            });

            imgHideRePass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkStatusEdtPass(edtRePassword, imgHideRePass);

                }
            });

            btnSave = view.findViewById(R.id.btnSave);
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Validator validatorPass = validatePass();
                    if (edtPassword.getText().toString().isEmpty() || edtRePassword.getText().toString().isEmpty()) {
                        GUIUtils.showAlertDialog(getContext(), getString(R.string.validate_info_error));
                    } else if (!edtPassword.getText().toString().equals(edtRePassword.getText().toString())) {
                        GUIUtils.showAlertDialog(getContext(), getString(R.string.error_pass));
                    } else if (!validatorPass.isValid()) {
                        GUIUtils.showAlertDialog(getContext(), getString(validatorPass.getStringError()));
                    } else {
                        callApiResetPass();
                    }
                }
            });

        } catch (Exception e) {

        }

        return view;
    }

    private void checkStatusEdtPass(EditText edtPass, ImageView imgHidePass) {
        if (edtPass.getInputType() != InputType.TYPE_CLASS_TEXT) {
            edtPass.setInputType(InputType.TYPE_CLASS_TEXT);
            imgHidePass.setImageResource(R.drawable.login_icon_eye_on_a);
        } else {
            edtPass.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            imgHidePass.setImageResource(R.drawable.login_icon_eye_off_a);
        }
    }

    private void callApiResetPass() {
        String phone = ((ForgotPassActivity) getActivity()).getPhone();
        int securityCode = ((ForgotPassActivity) getActivity()).getSecurityCode();
        String appVer = "";
        try {
            appVer = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        String androidId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().resetPass(androidId, appVer, phone, securityCode,
                edtPassword.getText().toString(), edtRePassword.getText().toString(), new CallBack() {
                    @Override
                    public void onComplete() {
                        showDialogSuccess();
                    }

                    @Override
                    public void onError(String errorMsg) {
                        GUIUtils.showAlertDialog(getActivity(), getString(R.string.timeout_loading));
                    }
                }
        );
    }

    public void showDialogSuccess() {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setMessage(getString(R.string.content_change_pass_success));
        alertDialog.setTitle(getString(R.string.title_change_pass_success));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        int viewLogin = ((ForgotPassActivity) getActivity()).getViewLogin();
                        Intent intent;
                        if (viewLogin == 0) {
                            intent = new Intent(getActivity(), LoginActivity.class);
                        } else {
                            intent = new Intent(getActivity(), LoginCTVActivity.class);
                        }
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
        alertDialog.show();
    }
}
