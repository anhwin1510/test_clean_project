package com.boxyzvn.vndrisc.fragment.register;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.register.ctv.RegisterCTVActivity;
import com.boxyzvn.vndrisc.activity.register.user.RegisterActivity;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBackCategorySecurityQuestion;
import com.boxyzvn.vndrisc.model.CategorySecurityQuestion;
import com.boxyzvn.vndrisc.model.UserInfo;
import com.boxyzvn.vndrisc.utils.GUIUtils;
import com.boxyzvn.vndrisc.utils.Validator;

import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class RegisterEmailFragment extends Fragment {

    public Activity activity;
    private View view;
    private EditText edtPhoneNumber;
    private EditText edtPass;
    private EditText edtRePass;
    private EditText edtAnswer;
    private ImageView imgHidePass;
    private ImageView imgHideRePass;
    private TextView tvCTV;
    private Spinner spinnerSecurityQuestion;
    private ArrayList<CategorySecurityQuestion> listtemCategoriesObjs = new ArrayList<>();
    private int securityID;
    private TextView tvPleseSelect;
    private int positionCategory;

    private Validator validatePhone() {
        return new Validator()
                .validatePhone(edtPhoneNumber.getText().toString());
    }

    public RegisterEmailFragment() {

    }

    public static RegisterEmailFragment newInstance() {

        Bundle args = new Bundle();

        RegisterEmailFragment fragment = new RegisterEmailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Validator validatePass() {
        return new Validator()
                .validatePassword(edtPass.getText().toString());

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup viewGroupParent = (ViewGroup) view.getParent();
            if (viewGroupParent != null)
                viewGroupParent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_register_email, container, false);
            Button btnNext = view.findViewById(R.id.btnNext);
            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Validator validatorPhone = validatePhone();
                    Validator validatorPass = validatePass();
                    if (edtPass.getText().toString().isEmpty() || edtPhoneNumber.getText().toString().isEmpty()
                            || edtRePass.getText().toString().isEmpty()) {
                        GUIUtils.showAlertDialog(getContext(), getString(R.string.validate_info_error));
                    } else if (!validatorPhone.isValid()) {
                        GUIUtils.showAlertDialog(getContext(), getString(validatorPhone.getStringError()));
                    } else if (!edtPass.getText().toString().equals(edtRePass.getText().toString())) {
                        GUIUtils.showAlertDialog(getContext(), getString(R.string.error_pass));
                    } else if (!validatorPass.isValid()) {
                        GUIUtils.showAlertDialog(getContext(), getString(validatorPass.getStringError()));
                    } else if (positionCategory == 0 || edtAnswer.getText().toString().equals("")) {
                        GUIUtils.showAlertDialog(getContext(), getString(R.string.validate_info_error));
                    } else {
                        gotoNextStep();
                    }

                }
            });
            edtPhoneNumber = view.findViewById(R.id.edtPhoneRegister);
            tvCTV = view.findViewById(R.id.tvCTV);
            edtPass = view.findViewById(R.id.edtPassword);
            edtRePass = view.findViewById(R.id.edtRePassword);
            edtAnswer = view.findViewById(R.id.edtAnswer);
            spinnerSecurityQuestion = view.findViewById(R.id.spinnerSecurity);
            tvPleseSelect = view.findViewById(R.id.tvPleseSelect);
            imgHidePass = view.findViewById(R.id.imgHidePass);
            imgHideRePass = view.findViewById(R.id.imgHideRePass);
            imgHidePass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkStatusEdtPass(edtPass, imgHidePass);

                }
            });

            imgHideRePass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkStatusEdtPass(edtRePass, imgHideRePass);

                }
            });

            tvCTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), RegisterCTVActivity.class));
                    getActivity().finish();
                }
            });

            callApiCategorySecurity();

        } catch (Exception e) {

        }

        return view;
    }

    private void callApiCategorySecurity() {
        String appVer = "";
        try {
            appVer = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        String androidId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().getInfoSecurityQuestions(androidId, appVer, new CallBackCategorySecurityQuestion() {
            @Override
            public void onComplete(ArrayList<CategorySecurityQuestion> list) {
                listtemCategoriesObjs = list;
                setAdapter();
            }

            @Override
            public void onError(String errorMsg) {

            }
        });
    }

    private void gotoNextStep() {
        UserInfo userInfo = new UserInfo();
        userInfo.setPass(edtPass.getText().toString());
        userInfo.setPhone(edtPhoneNumber.getText().toString());
        userInfo.setSecurityId(securityID);
        userInfo.setSecurityAnswer(edtAnswer.getText().toString());
        ((RegisterActivity) getActivity()).setUserInfo(userInfo);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft_add = fm.beginTransaction();
        ft_add.replace(R.id.frame_layout, RegisterInfoFragment.newInstance());
        ft_add.addToBackStack(TAG);
        ft_add.commit();
    }

    private void checkStatusEdtPass(EditText edtPass, ImageView imgHidePass) {
        if (edtPass.getInputType() != InputType.TYPE_CLASS_TEXT) {
            edtPass.setInputType(InputType.TYPE_CLASS_TEXT);
            imgHidePass.setImageResource(R.drawable.login_icon_eye_on_a);
        } else {
            edtPass.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            imgHidePass.setImageResource(R.drawable.login_icon_eye_off_a);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setViewID();
    }


    private void setViewID() {
        activity = getActivity();
    }

    private void setAdapter() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_security_question,
                R.id.txt_Item_Spinner, convertListSpinner(listtemCategoriesObjs));
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerSecurityQuestion.setAdapter(adapter);
        spinnerSecurityQuestion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                positionCategory = position;
                if (position == 0) {
                    tvPleseSelect.setText(getString(R.string.security_question));
                    tvPleseSelect.setTextColor(getActivity().getColor(R.color.color_text_hint));
                } else {
                    securityID = listtemCategoriesObjs.get(position - 1).id;
                    tvPleseSelect.setText(listtemCategoriesObjs.get(position - 1).content);
                    tvPleseSelect.setTextColor(getActivity().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public ArrayList<String> convertListSpinner(ArrayList<CategorySecurityQuestion> listCategoriesObj) {
        ArrayList<String> list = new ArrayList<>();
        list.add(getString(R.string.security_question));
        for (CategorySecurityQuestion s : listCategoriesObj) {
            list.add(s.content);
        }
        return list;
    }
}

