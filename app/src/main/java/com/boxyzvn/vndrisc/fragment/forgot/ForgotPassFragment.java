package com.boxyzvn.vndrisc.fragment.forgot;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.ForgotPassActivity;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBackCategorySecurityQuestion;
import com.boxyzvn.vndrisc.api.callback.CallBackCheck;
import com.boxyzvn.vndrisc.model.CategorySecurityQuestion;
import com.boxyzvn.vndrisc.utils.GUIUtils;
import com.boxyzvn.vndrisc.utils.Validator;

import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ForgotPassFragment extends Fragment {

    private View view;
    private EditText edtPhone, edtAnswer;
    private Spinner spinnerSecurityQuestion;
    private ArrayList<CategorySecurityQuestion> listtemCategoriesObjs = new ArrayList<>();
    private int securityID;
    private TextView tvPleseSelect;
    private Button btnSend;
    private String appVer, androidId;
    private int positionCategory;

    public ForgotPassFragment() {

    }

    public static ForgotPassFragment newInstance() {

        Bundle args = new Bundle();

        ForgotPassFragment fragment = new ForgotPassFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Validator validatePhone() {
        return new Validator()
                .validatePhone(edtPhone.getText().toString());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup viewGroupParent = (ViewGroup) view.getParent();
            if (viewGroupParent != null)
                viewGroupParent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_forgot_pass, container, false);
            edtPhone = view.findViewById(R.id.edtUsername);
            edtAnswer = view.findViewById(R.id.edtAnswer);
            spinnerSecurityQuestion = view.findViewById(R.id.spinnerSecurity);
            tvPleseSelect = view.findViewById(R.id.tvPleseSelect);
            btnSend = view.findViewById(R.id.btnSend);
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Validator validatorPhone = validatePhone();
                    if (edtPhone.getText().toString().isEmpty()) {
                        GUIUtils.showAlertDialog(getContext(), getString(R.string.validate_info_error));
                    } else if (!validatePhone().isValid()) {
                        GUIUtils.showAlertDialog(getContext(), getString(validatorPhone.getStringError()));
                    } else if (positionCategory == 0 || edtAnswer.getText().toString().equals("")) {
                        GUIUtils.showAlertDialog(getContext(), getString(R.string.validate_info_error));
                    } else {
                        callApiCheckSecurity();
                    }
                }
            });
            callApiCategorySecurity();

        } catch (Exception e) {

        }

        return view;
    }

    private void callApiCheckSecurity() {
        ApiServiceManager.getInstance().checkSecurityQuestion(appVer, androidId,
                edtPhone.getText().toString(), securityID, edtAnswer.getText().toString(),
                new CallBackCheck() {
                    @Override
                    public void onComplete(int securityCode) {
                        ((ForgotPassActivity) getActivity()).setSecurityCode(securityCode);
                        ((ForgotPassActivity) getActivity()).setPhone(edtPhone.getText().toString());
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft_add = fm.beginTransaction();
                        ft_add.replace(R.id.frame_layout, CreatePassFragment.newInstance());
                        ft_add.addToBackStack(TAG);
                        ft_add.commit();
                    }

                    @Override
                    public void onError(String errorMsg) {
                        GUIUtils.showAlertDialog(getActivity(), getString(R.string.timeout_loading));
                    }
                });
    }

    private void callApiCategorySecurity() {
        appVer = "";
        try {
            appVer = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        androidId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().getInfoSecurityQuestions(androidId, appVer, new CallBackCategorySecurityQuestion() {
            @Override
            public void onComplete(ArrayList<CategorySecurityQuestion> list) {
                listtemCategoriesObjs = list;
                setAdapter();
            }

            @Override
            public void onError(String errorMsg) {

            }
        });
    }

    private void setAdapter() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_security_question,
                R.id.txt_Item_Spinner, convertListSpinner(listtemCategoriesObjs));
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerSecurityQuestion.setAdapter(adapter);
        spinnerSecurityQuestion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                positionCategory = position;
                if (position == 0) {
                    tvPleseSelect.setText(getString(R.string.security_question));
                    tvPleseSelect.setTextColor(getActivity().getColor(R.color.color_text_hint));
                } else {
                    securityID = listtemCategoriesObjs.get(position - 1).id;
                    tvPleseSelect.setText(listtemCategoriesObjs.get(position - 1).content);
                    tvPleseSelect.setTextColor(getActivity().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public ArrayList<String> convertListSpinner(ArrayList<CategorySecurityQuestion> listCategoriesObj) {
        ArrayList<String> list = new ArrayList<>();
        list.add(getString(R.string.security_question));
        for (CategorySecurityQuestion s : listCategoriesObj) {
            list.add(s.content);
        }
        return list;
    }
}
