package com.boxyzvn.vndrisc.fragment.register.address;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.register.user.RegisterAddressActivity;
import com.boxyzvn.vndrisc.adapter.SimpleAddressAdapter;
import com.boxyzvn.vndrisc.model.QuanHuyen;
import com.boxyzvn.vndrisc.model.UserInfo;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.UIHelper;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import static com.boxyzvn.vndrisc.fragment.register.addressCTV.RegisterAddressCTVFragment2.DA_NANG_PARENT_CODE;
import static com.boxyzvn.vndrisc.utils.StringUtils.loadJSONFromAsset;

public class RegisterAddressFragment2 extends Fragment implements SimpleAddressAdapter.ItemClickListener {

    private RecyclerView rvAddress;
    private ArrayList<QuanHuyen> quanHuyens = new ArrayList<>();
    public Activity activity;
    private String parentCode;
    private int level = 0;
    private View view;

    public RegisterAddressFragment2() {

    }

    public static RegisterAddressFragment2 newInstance() {

        Bundle args = new Bundle();

        RegisterAddressFragment2 fragment = new RegisterAddressFragment2();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup viewGroupParent = (ViewGroup) view.getParent();
            if (viewGroupParent != null)
                viewGroupParent.removeView(view);
        }
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            parentCode = DA_NANG_PARENT_CODE;
        }
        try {
            view = inflater.inflate(R.layout.fragment_user_info_address, container, false);
            rvAddress = view.findViewById(R.id.rv_address);
            Button btnNext = view.findViewById(R.id.btnNext);
            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        } catch (Exception e) {

        }
        setupRecycleView();

        return view;
    }

    private void setupRecycleView() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JSONObject data = new JSONObject(loadJSONFromAsset(getContext(), "quanhuyen.json"));

            Iterator<String> iter = data.keys();
            while (iter.hasNext()) {
                String spot = iter.next();
                JSONObject value = data.getJSONObject(spot);
                QuanHuyen entity = mapper.readValue(value.toString(), QuanHuyen.class);
                if (entity.getParentCode().equals(parentCode)) {
                    quanHuyens.add(entity);

                }

                SimpleAddressAdapter simpleAdapter = new SimpleAddressAdapter(quanHuyens, getContext(), 2);
                rvAddress.setLayoutManager(new LinearLayoutManager(getContext()));
                simpleAdapter.setClickListener(RegisterAddressFragment2.this);
                rvAddress.setAdapter(simpleAdapter);

            }
        } catch (JSONException | IOException e) {
            Log.d("HUNG ", e.toString());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setViewID();
    }

    private void setViewID() {
        activity = getActivity();
    }

    @Override
    public void onItemClick(View view, int position) {
        UIHelper.showProgress(getContext());
        UserInfo userInfo = ((RegisterAddressActivity) getActivity()).getUserInfo();
        userInfo.setDistrict(quanHuyens.get(position).getName());
        SharePref.getInstance().setQuan(quanHuyens.get(position).getCode());
        ((RegisterAddressActivity) getActivity()).setUserInfo(userInfo);
        RegisterAddressFragment3 fragment = RegisterAddressFragment3.newInstance();
//        Bundle bundle = new Bundle();
//        bundle.putString("parentCode", quanHuyens.get(position).getCode());
//        fragment.setArguments(bundle);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_layout_address, fragment, "address")
                .addToBackStack("address").commit();

    }

    @Override
    public void onResume() {
        super.onResume();
        UIHelper.hideProgress();
    }
}

