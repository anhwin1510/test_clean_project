package com.boxyzvn.vndrisc;

import android.annotation.SuppressLint;
import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;

public class ProgressLoading {
    MaterialDialog.Builder materialDialogBuilder;
    MaterialDialog materialDialog;
    Context mContext;

    @SuppressLint("ResourceAsColor")
    public ProgressLoading(Context context) {
        mContext = context;
        this.materialDialogBuilder = new MaterialDialog.Builder(context).customView(R.layout.progress, false);
        this.materialDialogBuilder.backgroundColor(android.R.color.transparent);
        materialDialogBuilder.cancelable(false);
        show();
        hide();
    }

    public void show() {
        materialDialog = this.materialDialogBuilder.show();
    }

    public void hide() {
        this.materialDialog.dismiss();
    }

    public boolean isShow() {
        return materialDialog.isShowing();
    }
}
