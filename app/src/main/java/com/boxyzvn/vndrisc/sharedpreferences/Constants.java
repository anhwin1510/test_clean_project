package com.boxyzvn.vndrisc.sharedpreferences;

public interface Constants {
    String SHARED_PREFERENCES_NAME = "preference_vndrisc_v1.0.0";
    String IS_LOGIN = "IS_LOGIN";
    String TOKEN_LOGIN = "TOKEN_LOGIN";
    String IS_CTV = "IS_CTV";
    String QUAN_DA_CHON = "QUAN_DA_CHON";
    String LIST_LOI_NHAC = "LIST_LOI_NHAC";
    String CODE_NHAC_NHO = "CODE_NHAC_NHO";
    String KEY_INFO_USER_LGOIN = "KEY_INFO_USER_LGOIN";
    String KEY_OBJ_SUMMARY = "KEY_OBJ_SUMMARY";
    String KEY_POINT = "KEY_POINT";
    String KEY_RECOMMEND = "KEY_RECOMMEND";
    String KEY_IS_SEND_DA_CHAN_DOAN = "KEY_IS_SEND_DA_CHAN_DOAN";
    String KEY_IS_SEND_CHUA_CHAN_DOAN = "KEY_IS_SEND_CHUA_CHAN_DOAN";
    String KEY_JSON_KET_QUA_CHUA_CHAN_DOAN = "KEY_JSON_KET_QUA_CHUA_CHAN_DOAN";
    String KEY_JSON_KET_QUA_DA_CHAN_DOAN = "KEY_JSON_KET_QUA_DA_CHAN_DOAN";
    int MAX_RETRY = 3;
    int TIME_OUT_LOADING = 30000;
}
