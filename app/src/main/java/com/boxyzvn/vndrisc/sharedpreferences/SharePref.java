package com.boxyzvn.vndrisc.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.boxyzvn.vndrisc.model.ObjSumaryRuleExplain;
import com.boxyzvn.vndrisc.model.UserInfo;
import com.google.gson.Gson;

public class SharePref implements Constants {

    private static SharePref suInstance;
    SharedPreferences sharedPreferences;
    android.content.SharedPreferences.Editor editor;

    public static SharePref getInstance() {
        if (suInstance == null) {
            suInstance = new SharePref();
        }
        return suInstance;
    }

    public void init(Context context) {
        sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void reset() {
        editor.clear();
        save();
    }

    private void save() {
        editor.commit();
    }

    public void setLoginStatus(boolean key) {
        editor.putBoolean(IS_LOGIN, key);
        save();
    }

    public boolean isLogin() {
        boolean value = sharedPreferences.getBoolean(IS_LOGIN, false);
        return value;
    }

    public void setSendDaChanDoanSuccess(boolean key) {
        editor.putBoolean(KEY_IS_SEND_DA_CHAN_DOAN, key);
        save();
    }

    public boolean isSendDaChanDoanSuccess() {
        boolean value = sharedPreferences.getBoolean(KEY_IS_SEND_DA_CHAN_DOAN, true);
        return value;
    }

    public void setSendChuaChanDoanSuccess(boolean key) {
        editor.putBoolean(KEY_IS_SEND_CHUA_CHAN_DOAN, key);
        save();
    }

    public boolean isSendChuaChanDoanSuccess() {
        boolean value = sharedPreferences.getBoolean(KEY_IS_SEND_CHUA_CHAN_DOAN, true);
        return value;
    }
    public void setCTVStatus(boolean key) {
        editor.putBoolean(IS_CTV, key);
        save();
    }

    public boolean isCTV() {
        boolean value = sharedPreferences.getBoolean(IS_CTV, false);
        return value;
    }

    public String getTokenLogin() {
        String value = sharedPreferences.getString(TOKEN_LOGIN, "");
        return value;
    }

    public void setTokenLogin(String key) {
        editor.putString(TOKEN_LOGIN, key);
        save();
    }

    public String quanSelected() {
        String value = sharedPreferences.getString(QUAN_DA_CHON, "");
        return value;
    }

    public void setQuan(String key) {
        editor.putString(QUAN_DA_CHON, key);
        save();
    }

    public String getListLoiNhac() {
        String value = sharedPreferences.getString(LIST_LOI_NHAC, "");
        return value;
    }

    public void setListLoiNhac(String key) {
        editor.putString(LIST_LOI_NHAC, key);
        save();
    }

    public int getCodeNhacNho() {
        int value = sharedPreferences.getInt(CODE_NHAC_NHO, 0);
        return value;
    }

    public void setCodeNhacNho(int key) {
        editor.putInt(CODE_NHAC_NHO, key);
        save();
    }
    public void saveInfoLogin(UserInfo user) {
        editor.putString(this.KEY_INFO_USER_LGOIN, user.toJson());
        save();
    }
    public UserInfo getInfoLogin() {
        String value = sharedPreferences.getString(this.KEY_INFO_USER_LGOIN, "");
        return value.equals("") ? null : new Gson().fromJson(value, UserInfo.class);
    }

    public void saveObjecSummary(ObjSumaryRuleExplain summary) {
        editor.putString(this.KEY_OBJ_SUMMARY, new Gson().toJson(summary));
        save();
    }
    public ObjSumaryRuleExplain getObjecSummary() {
        String value = sharedPreferences.getString(this.KEY_OBJ_SUMMARY, "");
        return value.equals("") ? null : new Gson().fromJson(value, ObjSumaryRuleExplain.class);
    }

    public String getRecommend() {
        String value = sharedPreferences.getString(KEY_RECOMMEND, "");
        return value;
    }

    public void setRecommend(String key) {
        editor.putString(KEY_RECOMMEND, key);
        save();
    }

    public String getJsonKetQuaDaChanDoan() {
        String value = sharedPreferences.getString(KEY_JSON_KET_QUA_DA_CHAN_DOAN, "");
        return value;
    }

    public void saveJsonKetQuaDaChanDoan(String key) {
        editor.putString(KEY_JSON_KET_QUA_DA_CHAN_DOAN, key);
        save();
    }

    public String getJsonKetQuaChuaChanDoan() {
        String value = sharedPreferences.getString(KEY_JSON_KET_QUA_CHUA_CHAN_DOAN, "");
        return value;
    }

    public void saveJsonKetQuaChuaChanDoan(String key) {
        editor.putString(KEY_JSON_KET_QUA_CHUA_CHAN_DOAN, key);
        save();
    }

    public int getPoint() {
        int value = sharedPreferences.getInt(KEY_POINT, -1);
        return value;
    }

    public void setPoint(int key) {
        editor.putInt(KEY_POINT, key);
        save();
    }
}