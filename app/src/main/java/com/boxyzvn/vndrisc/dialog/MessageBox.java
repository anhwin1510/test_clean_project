package com.boxyzvn.vndrisc.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.boxyzvn.vndrisc.R;

/**
 * push通知受診時に表示するMessageBox
 */
public class MessageBox extends Dialog {
    private Activity activity;
    private Dialog dialog;
    private String msg;
    private String time;
    private String popUpID;
    Button buttonClose, buttonCheck;
    ImageView btnCheckout, btnCancelCheckout;

    public MessageBox(final Activity activity, String msg, String time, String popUpID) {
        super(activity);
        this.activity = activity;
        this.msg = msg;
        this.time = time;
        this.popUpID = popUpID;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_message_no_title);

        TextView tvMessage = (TextView) findViewById(R.id.dialog_tvMessage);
        TextView tvTime = (TextView) findViewById(R.id.dialog_tvTime);
        tvMessage.setText(msg, TextView.BufferType.SPANNABLE);
        tvTime.setText(time, TextView.BufferType.SPANNABLE);
        buttonClose = (Button) findViewById(R.id.dialog_btnOK);

        buttonClose.setVisibility(View.VISIBLE);


        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();

            }
        });

        //change UI 2018 2 2


        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

}
