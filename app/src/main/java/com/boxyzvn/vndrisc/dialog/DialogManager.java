package com.boxyzvn.vndrisc.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.widget.Button;
import android.widget.TextView;

import com.boxyzvn.vndrisc.R;

public class DialogManager implements Constant {
    private static DialogManager INSTANCE;
    Context context;

    public static DialogManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DialogManager();
        }
        return INSTANCE;
    }

    public void init(Context context) {
        this.context = context;
    }

    public void dialogCheckinError(final int DIALOG_ERROR_TYPE) {
        int layout = R.layout.dialog_checkin_error;
        final BaseDialog baseDialog = new BaseDialog(context, layout, R.id.icClose) {
            @Override
            public void getViewCustom(Dialog dialog) {
                switch (DIALOG_ERROR_TYPE) {
                    case DIALOG_CHECKIN_ERROR:
                        setDialogCheckinError(dialog);
                        break;
                }
            }

            @Override
            public void clickButtonLeft() {
                switch (DIALOG_ERROR_TYPE) {
                    case DIALOG_CHECKIN_ERROR:
                        this.hide();
                        break;
                }

            }

            @Override
            public void clickButtonRight() {

            }
        };
        baseDialog.show();
    }

    public void dialogError(final int DIALOG_ERROR_TYPE) {
        int layout = R.layout.dialog_error;
        final BaseDialog baseDialog = new BaseDialog(context, layout, R.id.btnClose, R.id.btnSetting) {
            @Override
            public void getViewCustom(Dialog dialog) {
                switch (DIALOG_ERROR_TYPE) {
                    case DIALOG_LOCATION_ERROR:
                        setDialogLocationError(dialog);
                        break;
                }
            }

            @Override
            public void clickButtonLeft() {
                switch (DIALOG_ERROR_TYPE) {
                    case DIALOG_LOCATION_ERROR:
                        this.hide();
                        break;
                }

            }

            @Override
            public void clickButtonRight() {
                switch (DIALOG_ERROR_TYPE) {
                    case DIALOG_LOCATION_ERROR:
                        this.hide();
                        Intent intentOpenLocationSettings = new Intent();
                        intentOpenLocationSettings.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(intentOpenLocationSettings);
                        break;
                }
            }
        };
        baseDialog.show();
    }

    private void setDialogCheckinError(Dialog dialogCheckinError) {

    }

    private void setDialogLocationError(Dialog dialogLocationError) {
        setContentDialogError(dialogLocationError, context.getString(R.string.title_location_error_dialog),
                context.getString(R.string.content_location_error_dialog));
        setContentButtonDialogError(dialogLocationError, context.getString(R.string.button_ok_location_error_dialog),
                context.getString(R.string.button_cancel_location_error_dialog));
    }

    private void setContentDialogError(Dialog dialog, String title, String content) {
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        TextView tvContent = (TextView) dialog.findViewById(R.id.tvContent);
        tvTitle.setText(title);
        tvContent.setText(content);
    }

    private void setContentButtonDialogError(Dialog dialog, String txtOK, String txtCancel) {
        Button btnSetting = (Button) dialog.findViewById(R.id.btnSetting);
        Button btnClose = (Button) dialog.findViewById(R.id.btnClose);
        btnSetting.setText(txtOK);
        btnClose.setText(txtCancel);
    }
}
