package com.boxyzvn.vndrisc.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

public abstract class BaseDialog {
    Dialog mSelectDialog;
    Context mContext;

    // Dialog one button
    public BaseDialog(Context context, int layout, int idButtonClick) {
        mContext = context;
        mSelectDialog = new Dialog(context);
        mSelectDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mSelectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setLayout(layout);
        sizeDefault();
        getViewCustom(mSelectDialog);
        if (idButtonClick == -1) return;
        View button = (View) mSelectDialog.findViewById(idButtonClick);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickButtonLeft();
            }
        });
    }

    // Dialog two button
    public BaseDialog(Context context, int layout, int idButtonLeft, int idButtonRight) {
        mContext = context;
        mSelectDialog = new Dialog(context);
        mSelectDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mSelectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setLayout(layout);
        sizeDefault();
        getViewCustom(mSelectDialog);

        //click button lefft
        mSelectDialog.findViewById(idButtonLeft).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickButtonLeft();
            }
        });

        //click button right
        mSelectDialog.findViewById(idButtonRight).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickButtonRight();
            }
        });
    }

    // set view layout
    public void setLayout(int layout) {
        mSelectDialog.setContentView(layout);
    }

    public void sizeDefault() {
        mSelectDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
    }

    public Dialog getDialog() {
        return this.mSelectDialog;
    }

    public abstract void getViewCustom(Dialog dialog);

    public abstract void clickButtonLeft();

    public abstract void clickButtonRight();


    public void show() {
        mSelectDialog.show();
    }

    public void hide() {
        mSelectDialog.dismiss();
    }

    public interface CallbackClickButtonDiaglog {
        void clickButtonLeft();

        void clickButtonRight();
    }


}

