package com.boxyzvn.vndrisc.activity;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.boxyzvn.vndrisc.ProgressLoading;
import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.utils.LocalNotificationReceiver;

public class BaseHomeActivity extends AppCompatActivity {

    private ProgressLoading progressLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.header_simple);
        progressLoading = new ProgressLoading(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.text_home));
        }
    }

    public void setTextTitle(String title, boolean isShow) {
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        ImageView ivBack = (ImageView) findViewById(R.id.ivBack);
        tvTitle.setText(title);
        ImageView ivAddLich = (ImageView) findViewById(R.id.ivAddLich);
        if (isShow) {
            ivAddLich.setVisibility(View.VISIBLE);
        } else {
            ivAddLich.setVisibility(View.GONE);
        }
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });
    }

    /**
     * set alarm for notification
     *
     * @param notification set up local notify
     * @param delay        after delay
     * @param requestCode  code to cancle notify
     */
    public void scheduleNotification(Notification notification, long delay, int requestCode) {
        Intent notificationIntent = new Intent(this, LocalNotificationReceiver.class);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION, notification);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_REQUEST_CODE, requestCode);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, requestCode, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT
        );

        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }
}
