package com.boxyzvn.vndrisc.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.adapter.CustomAdapter;
import com.boxyzvn.vndrisc.model.ObjNhacNho;

import static com.boxyzvn.vndrisc.activity.ChonGioActivity.NGAY_NHAC_NHO;
import static com.boxyzvn.vndrisc.activity.ChonGioActivity.NOI_DUNG_NHAC_NHO;

public class ChonNgayActivity extends BaseActivity {

    Button btnSave;
    CheckedTextView cbView1, cbView2;
    ListView recyclerView;
    CustomAdapter customAdapter;
    ObjNhacNho objNhacNho;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_chon_ngay;
        super.onCreate(savedInstanceState);
        btnSave = findViewById(R.id.btnSaveNgay);
        cbView2 = findViewById(R.id.cTextThu2);
        recyclerView = findViewById(R.id.rcNgayNhacNho);

        setTextTitle("Chọn ngày lặp lại", false);

        objNhacNho = (ObjNhacNho) getIntent().getSerializableExtra(NGAY_NHAC_NHO);

        customAdapter = new CustomAdapter(getApplicationContext(), objNhacNho.getDay());
        recyclerView.setAdapter(customAdapter);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objNhacNho.setDay(customAdapter.getDays());
                Intent intent = new Intent();
                intent.putExtra(NGAY_NHAC_NHO, objNhacNho);
                setResult(Activity.RESULT_OK, intent);
                finish();

            }
        });
    }
}
