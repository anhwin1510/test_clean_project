package com.boxyzvn.vndrisc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.boxyzvn.vndrisc.ProgressLoading;
import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.login.LoginActivity;

public class SignupActivity extends AppCompatActivity {

    public ProgressLoading progressLoading;
    public Button btnSignUp;
    public ImageView icBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        progressLoading = new ProgressLoading(this);
        setViewID();
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressLoading.show();
                startActivity(new Intent(SignupActivity.this, LoginActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        icBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressLoading.show();
                startActivity(new Intent(SignupActivity.this, LoginActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

    }

    public void setViewID(){
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        icBack = (ImageView) findViewById(R.id.icBack);
    }

//    public void callAPI(UserLogin loginUser) {
//        UIHelper.showProgress(this);
//
//        ApiServiceManager.getInstance().login(loginUser.getUserName(), loginUser.getPassWord(), "12321", "1.0.5", new CallBackLogin() {
//            @Override
//            public void onComplete(String userName, String pass) {
//                UIHelper.hideProgress();
//                startActivity(new Intent(SignupActivity.this, MainActivity.class));
//            }
//
//            @Override
//            public void onError(String errorCode) {
//                UIHelper.hideProgress();
//            }
//        });
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (progressLoading.isShow()) {
//            progressLoading.hide();
//        }
//    }
}
