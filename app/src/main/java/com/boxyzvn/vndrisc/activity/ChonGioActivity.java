package com.boxyzvn.vndrisc.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.login.LoginActivity;
import com.boxyzvn.vndrisc.model.ObjNhacNho;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.DateUtils;
import com.boxyzvn.vndrisc.utils.GUIUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;

import static com.boxyzvn.vndrisc.activity.NhacNhoActivity.OBJ_NHAC_NHO;
import static com.boxyzvn.vndrisc.activity.NhacNhoActivity.POSITION_CLICKED;

public class ChonGioActivity extends BaseActivity {

    public static final int REQUEST_NOI_DUNG_NHAC_NHO = 4;
    public static final int REQUEST_NGAY_NHAC_NHO = 5;
    public static final String NOI_DUNG_NHAC_NHO = "noiDungNhacNho";
    public static final String NOI_DUNG_DEFAULT = "noiDungDefault";
    public static final String NGAY_NHAC_NHO = "ngayNhacNho";
    TextView tvNoiDungNhacNho, tvNgayNhacNho;
    TimePicker timePicker;
    ObjNhacNho objNhacNho;
    Button btnSave;
    int positionClicked = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_chon_gio;
        super.onCreate(savedInstanceState);
        tvNoiDungNhacNho = findViewById(R.id.tvNoiDungNhacNho);
        tvNgayNhacNho = findViewById(R.id.tvChonNgayNhacNho);
        btnSave = findViewById(R.id.btnSaveGioNhacNho);

        setTextTitle("Đặt lịch nhắc nhở", false);
        timePicker = (TimePicker) findViewById(R.id.timerPicker);
        timePicker.setIs24HourView(true);
        objNhacNho = (ObjNhacNho) getIntent().getSerializableExtra(OBJ_NHAC_NHO);

        if (objNhacNho != null) {
            Date date = DateUtils.string2Date(objNhacNho.getTime(), "HH:mm");
            timePicker.setHour(date.getHours());
            timePicker.setMinute(date.getMinutes());
            setTextNgayNhacNho(objNhacNho);
            positionClicked = getIntent().getIntExtra(POSITION_CLICKED, 0);
        } else {
            int days[] = {1, 1, 1, 1, 1, 1, 1};
            int code = SharePref.getInstance().getCodeNhacNho();
            code++;
            objNhacNho = new ObjNhacNho("", "", days, true, code);
            setTextNgayNhacNho(objNhacNho);
        }
        tvNoiDungNhacNho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChonGioActivity.this, NhapThongTinNhacNhoActivity.class);
                intent.putExtra(NOI_DUNG_NHAC_NHO, tvNoiDungNhacNho.getText().toString());
                startActivityForResult(intent, REQUEST_NOI_DUNG_NHAC_NHO);
            }
        });

        tvNgayNhacNho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChonGioActivity.this, ChonNgayActivity.class);
                intent.putExtra(NGAY_NHAC_NHO, objNhacNho);
                startActivityForResult(intent, REQUEST_NGAY_NHAC_NHO);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String minute = "";
                String hour = "";
                if (timePicker.getHour() >= 10) {
                    hour = timePicker.getHour() + "";
                } else {
                    hour = "0" + timePicker.getHour();
                }
                if (timePicker.getMinute() >= 10) {
                    minute = timePicker.getMinute() + "";
                } else {
                    minute = "0" + timePicker.getMinute();
                }

                objNhacNho.setTime(hour + ":" + minute);
                String strListLoiNhac = SharePref.getInstance().getListLoiNhac();
                ArrayList<ObjNhacNho> listdata;
                if (strListLoiNhac.isEmpty()) {
                    listdata = new ArrayList<>();
                } else {
                    listdata = new Gson().fromJson(strListLoiNhac, new TypeToken<ArrayList<ObjNhacNho>>() {
                    }.getType());
                }
                if (getIntent().getAction() != null && getIntent().getAction().equals("ADD")) {
                    listdata.add(objNhacNho);
                } else {
                    listdata.set(positionClicked, objNhacNho);
                }

                if (!objNhacNho.getLoiNhac().isEmpty()) {
                    SharePref.getInstance().setCodeNhacNho(objNhacNho.getCode());
                    SharePref.getInstance().setListLoiNhac(new Gson().toJson(listdata));
                    finish();
                } else {
                    GUIUtils.showAlertDialog(ChonGioActivity.this, "Vui lòng nhập đủ thông tin");
                }

            }
        });

    }

    private void setTextNgayNhacNho(ObjNhacNho objNhacNho) {
        int day[] = objNhacNho.getDay();
        String ngayNhacNho = "";
        boolean isAllDay = true;
        if (objNhacNho.getLoiNhac().equals("")) {
            tvNoiDungNhacNho.setText(R.string.nhap_loi_nhac);
        } else {
            tvNoiDungNhacNho.setText(objNhacNho.getLoiNhac());
        }
        boolean checkDay = false;
        for (int i = 0; i < day.length; i++) {
            if (day[i] == 1) {
                checkDay = true;
                if (i == 6) {
                    ngayNhacNho += "CN";
                } else {
                    ngayNhacNho += "Thứ " + (i + 2) + " ";
                }
            } else {
                isAllDay = false;
            }
        }
        if (isAllDay) {
            tvNgayNhacNho.setText("Hằng ngày");
        } else if (!checkDay) {
            tvNgayNhacNho.setText("Không lặp lại");
        } else {
            tvNgayNhacNho.setText(ngayNhacNho);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK)
            return;
        if (requestCode == REQUEST_NOI_DUNG_NHAC_NHO) {
            tvNoiDungNhacNho.setText(data.getStringExtra(NOI_DUNG_NHAC_NHO));
            String sd = data.getStringExtra(NOI_DUNG_DEFAULT);
            objNhacNho.setLoiNhac(data.getStringExtra(NOI_DUNG_NHAC_NHO));
            objNhacNho.setLoiNhacDefault(data.getStringExtra(NOI_DUNG_DEFAULT));
        }

        if (requestCode == REQUEST_NGAY_NHAC_NHO) {
            objNhacNho = (ObjNhacNho) data.getSerializableExtra(NGAY_NHAC_NHO);
            setTextNgayNhacNho(objNhacNho);
        }

    }
}
