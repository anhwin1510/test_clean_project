package com.boxyzvn.vndrisc.activity.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.boxyzvn.vndrisc.ProgressLoading;
import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.BaseActivity;
import com.boxyzvn.vndrisc.activity.ForgotPassActivity;
import com.boxyzvn.vndrisc.activity.HomeActivity;
import com.boxyzvn.vndrisc.activity.HomeCTVActivity;
import com.boxyzvn.vndrisc.activity.MainActivity;
import com.boxyzvn.vndrisc.activity.register.ctv.RegisterCTVActivity;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBackLogin;
import com.boxyzvn.vndrisc.model.ObjMap;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.GUIUtils;
import com.boxyzvn.vndrisc.utils.UIHelper;
import com.boxyzvn.vndrisc.utils.Validator;

import java.util.ArrayList;

public class LoginCTVActivity extends BaseActivity {

    public ProgressLoading progressLoading;
    public Button btnLogin, btnRegister;
    public TextView tvChangeMode, tvQuen;
    private EditText edtPass, edtPhoneNumber;
    private CheckBox cbSaveLogin;
    private ImageView imgHidePass;
    ArrayList<ObjMap> arrayList = new ArrayList<>();
    private boolean checkPass = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_login_ctv;
        super.onCreate(savedInstanceState);
        setViewID();
        setViewClick();

        Window window = this.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.gray));
    }

    public void setViewID() {
        progressLoading = new ProgressLoading(this);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvChangeMode = (TextView) findViewById(R.id.tvChangeMode);
        tvQuen = (TextView) findViewById(R.id.tvQuen);
        edtPass = findViewById(R.id.edtPassword);
        edtPhoneNumber = findViewById(R.id.edtUsername);
        btnRegister = findViewById(R.id.btnRegister);
        cbSaveLogin = findViewById(R.id.cbSaveLogin);
        imgHidePass = findViewById(R.id.imgHidePass);
        cbSaveLogin.setButtonTintList(ColorStateList.valueOf(getColor(R.color.colorAccent)));
        imgHidePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkStatusEdtPass(edtPass, imgHidePass);
            }
        });
        tvQuen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginCTVActivity.this, ForgotPassActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("view", 1);
                intent.putExtra("data", bundle);
                startActivity(intent);
            }
        });
    }

    private void checkStatusEdtPass(EditText edtPass, ImageView imgHidePass) {
        if (checkPass) {
            edtPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            imgHidePass.setImageResource(R.drawable.login_icon_eye_on_a);
            checkPass = false;
        } else {
            edtPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
            imgHidePass.setImageResource(R.drawable.login_icon_eye_off_a);
            checkPass = true;
        }
    }

    private Validator validatePhone() {
        return new Validator()
                .validatePhone(edtPhoneNumber.getText().toString());

    }

    private Validator validatePass() {
        return new Validator()
                .validatePassword(edtPass.getText().toString());

    }

    public void setViewClick() {
        btnLogin.setOnClickListener(view -> {
            Validator validatorPhone = validatePhone();
            Validator validatorPass = validatePass();
            if (edtPass.getText().toString().isEmpty() || edtPhoneNumber.getText().toString().isEmpty()) {
                GUIUtils.showAlertDialog(LoginCTVActivity.this, getString(R.string.validate_phone_and_password_empty));
            } else if (!validatorPhone.isValid()) {
                GUIUtils.showAlertDialog(LoginCTVActivity.this, getString(validatorPhone.getStringError()));
            } else {
                if (!validatorPass.isValid()) {
                    GUIUtils.showAlertDialog(LoginCTVActivity.this, getString(validatorPass.getStringError()));
                } else {
                    if (checkNetworkStatus()) {
                        btnLogin.setEnabled(false);
                        callAPI(edtPhoneNumber.getText().toString(), edtPass.getText().toString());
                    } else {
                        GUIUtils.showAlertDialog(LoginCTVActivity.this, getString(R.string.network_error));
                    }
                }
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginCTVActivity.this, RegisterCTVActivity.class);
                startActivity(intent);
            }
        });
        tvChangeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginCTVActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }

    public void callAPI(String phone, String password) {
        UIHelper.showProgress(this);
        String appVer = "";
        try {
            appVer = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().loginCTV(phone, password, androidId, appVer, new CallBackLogin() {
            @Override
            public void onComplete(String token) {
                SharePref.getInstance().setTokenLogin(token);
                if (cbSaveLogin.isChecked()) {
                    SharePref.getInstance().setLoginStatus(true);
                    SharePref.getInstance().setCTVStatus(true);
                }
                UIHelper.hideProgress();
                startActivity(new Intent(LoginCTVActivity.this, HomeCTVActivity.class).
                        setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }

            @Override
            public void onError(String errorMsg) {
                UIHelper.hideProgress();
                btnLogin.setEnabled(true);
                GUIUtils.showAlertDialog(LoginCTVActivity.this, getString(R.string.login_error));
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (progressLoading.isShow()) {
            progressLoading.hide();
        }
    }
}