package com.boxyzvn.vndrisc.activity;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boxyzvn.vndrisc.NonSwipeableViewPager;
import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBackGetJson;
import com.boxyzvn.vndrisc.model.ObjQuestion;
import com.boxyzvn.vndrisc.model.ObjQuestionOption;
import com.boxyzvn.vndrisc.utils.UIHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;

import static com.boxyzvn.vndrisc.utils.StringUtils.loadJSONFromAsset;

public class SanLocActivity extends BaseActivity {
    public static final int HEIGH_VIEW = 130;
    public static final String KET_QUA = "ket_qua";
    public static final String VALUE_KET_QUA = "value_ket_qua";
    public static final String DA_CHAN_DOAN = "da_chan_doan";
    public static final String KHUYEN_NGHI = "khuyen_nghi";
    public static final String KET_QUA_CHECK_BOX = "ket_qua_check_box";
    public static final String QUESTION_LIST = "question";
    public static final String JSON_CHUA_CHAN_DOAN = "json_chua_chan_doan";
    public static final String SUMARY_OBJ = "sumary_obj";
    public static final String TOTAL_SCORE = "TOTAL_SCORE";
    public static final String QUESTION_RULE_LIST = "question_rule";
    private NonSwipeableViewPager viewPager;
    private SanLocActivity.MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private ArrayList<ObjQuestion> listQuestion = new ArrayList<>();
    private ArrayList<String> listAnswer = new ArrayList<>();
    private ArrayList<String> listValue = new ArrayList<>();
    private View viewProcess;
    private String answer, value;
    private Calendar selectedDate;
    private String questionData;
    private String questionRule;
    private String jsonChuaChanDoan;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_chua_chan_doan;
        super.onCreate(savedInstanceState);
        setTitleTwoLine("Chưa Được Chẩn Đoán", "Trả lời các câu hỏi sau");
//        boolean isFirstInstall = SharePref.getBoolean(this, SharePref.Key.IS_FIRST_INSTALLED, true);
//        if (!isFirstInstall) {
//            launchHomeScreen();
//            return;
//        }

        initView();
        if (checkNetworkStatus()) {
            addPager();
        } else {
            try {
                JSONObject data = new JSONObject(loadJSONFromAsset(this, "chuachandoan.json"));
                setUpDataJson(data);
            } catch (JSONException e) {
                Log.e("HUNG", e.toString());
            }
        }

        // adding bottom dots


        View.OnClickListener nextScreen = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchHomeScreen();
            }
        };
    }

    private void initView() {
        viewPager = (NonSwipeableViewPager) findViewById(R.id.pagerWalkthrough);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        viewProcess = (View) findViewById(R.id.viewProcess);

    }

    private void addPager() {

        String token = com.boxyzvn.vndrisc.sharedpreferences.SharePref.getInstance().getTokenLogin();
        String appVer = "";
        try {
            appVer = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().getJSONChuaChanDoan(token, androidId, appVer, new CallBackGetJson() {
            @Override
            public void onComplete(JSONObject json) {
                setUpDataJson(json);
            }

            @Override
            public void onError(String errorMsg) {

            }
        });
    }

    private void setUpDataJson(JSONObject json) {
        JSONObject data = json;
        JSONArray dataQuestion;
        JSONObject dataRule;
        jsonChuaChanDoan = json.toString();
        try {
            dataQuestion = data.getJSONArray("questions");
            dataRule = data.getJSONObject("rule");
            Log.e("HUNGG", dataRule.toString());
            questionData = dataQuestion.toString();
            questionRule = dataRule.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type type = new TypeToken<ArrayList<ObjQuestion>>() {
        }.getType();

        ArrayList<ObjQuestion> listQuestionData = new Gson().fromJson(questionData, type);
        listQuestion.addAll(listQuestionData);
        float percent = (float) 1 / (float) listQuestion.size();
        Log.e("HUNGG", percent + "");

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, 10, (float) percent);
        viewProcess.setLayoutParams(params);
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
    }


    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        startActivity(new Intent(SanLocActivity.this, KetQuaActivity.class)
                .putExtra(KET_QUA, listAnswer)
                .putExtra(VALUE_KET_QUA, listValue)
                .putExtra(QUESTION_LIST, questionData)
                .putExtra(JSON_CHUA_CHAN_DOAN, jsonChuaChanDoan)
                .putExtra(QUESTION_RULE_LIST, questionRule));
        finish();
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };


    private void setupDatetimePicker() {


    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;
        Button btnNext;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.layout_question, container, false);
            container.addView(view);
            String titleQuestion = listQuestion.get(position).getTitle();
            String content = listQuestion.get(position).getContent();
            String clicked = "";
            View view1;
            String text = "<font color=#068855>" + titleQuestion + ": " + "</font> <font color=#707070>" + content + "</font>";

            btnNext = view.findViewById(R.id.btnNext);
            btnNext.setVisibility(View.GONE);

            TextView title = view.findViewById(R.id.tvTitleQuestion);
            title.setText(Html.fromHtml(text));
            LinearLayout linearLayoutRoot = view.findViewById(R.id.layoutContentQuestion);
            ArrayList<ObjQuestionOption> listOption = listQuestion.get(position).getOptions();
            for (int i = 0; i < listOption.size(); i++) {
                if (listOption.get(i).getType().equals("button")) {
                    createButton(position, linearLayoutRoot, listOption, i, btnNext);
                }
                if (listOption.get(i).getType().equals("input")) {
                    createEdittextView(linearLayoutRoot, listOption, i, btnNext);
                }

                if (listOption.get(i).getType().equals("checkbox")) {
                    createCheckbox(linearLayoutRoot, listOption, i);
                }
                if (listOption.get(i).getType().equals("date")) {
                    createTextView(linearLayoutRoot, listOption, i, btnNext);
                }

            }

            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int current = getItem(+1);
                    float percent = (float) getItem(+2) / (float) listQuestion.size();
                    Log.e("HUNGG", percent + "");
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, 10, (float) percent);
                    viewProcess.setLayoutParams(params);
                    viewPager.setCurrentItem(current);
                    listValue.add(value);
                    UIHelper.hideKeyboard(SanLocActivity.this);
                    if (current < listQuestion.size()) {
                        // move to next screen
                        viewPager.setCurrentItem(current);
                        listAnswer.add(answer);
                    } else {
                        listAnswer.add(answer);
                        launchHomeScreen();
                    }
                }
            });

            return view;
        }

        private void createButton(int position, LinearLayout linearLayoutRoot, ArrayList<ObjQuestionOption> listOption, int i,
                                  Button btnNext) {
            Button button = new Button(getApplicationContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    Math.round(HEIGH_VIEW * scale)
            );
            params.gravity = Gravity.CENTER_HORIZONTAL;

            params.setMargins(Math.round(50 * scale), 0, Math.round(50 * scale), Math.round(50 * scale));
            button.setLayoutParams(params);
            button.setPadding(0, 0, 0, 0);
            button.setId(Integer.parseInt(position + "" + i));
            button.setBackgroundResource(R.drawable.main_button);
            button.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_button));
            button.setTextColor(getResources().getColorStateList(R.color.color_text_change));
            //button.setTextColor(R.drawable.text_white_to_green);
            button.setText(listOption.get(i).getName());
            int finalI = i;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnNext.setVisibility(View.VISIBLE);
                    resetButton(listOption, position, finalI);
                }
            });
            linearLayoutRoot.addView(button);
        }

        private void createEdittextView(LinearLayout linearLayoutRoot, ArrayList<ObjQuestionOption> listOption, int i,
                                        Button btnNext) {
            EditText edtInput = new EditText(getApplicationContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    Math.round(130 * scale)
            );
            params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
            params.setMargins(Math.round(50 * scale), 0, Math.round(50 * scale), Math.round(50 * scale));
            edtInput.setLayoutParams(params);
            edtInput.setPadding(Math.round(50 * scale), 0, Math.round(50 * scale), 0);
            edtInput.setBackgroundResource(R.drawable.custom_edit_text);
            edtInput.setTextColor(getResources().getColorStateList(R.color.color_text_change));
            edtInput.setInputType(InputType.TYPE_CLASS_NUMBER);
            edtInput.setHint(listOption.get(i).getName());
            edtInput.setGravity(Gravity.CENTER_VERTICAL);
            edtInput.requestFocus();
            edtInput.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtInput, InputMethodManager.SHOW_FORCED);
            edtInput.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE && !edtInput.getText().toString().equals("")) {
                        //do here your stuff f
                        UIHelper.hideKeyboard(SanLocActivity.this);

                        return true;
                    }
                    return false;
                }
            });
            edtInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    answer = s.toString();
                    value = s.toString();
                    if (!s.toString().equals("")) {
                        btnNext.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            linearLayoutRoot.addView(edtInput);
        }

        private void createTextView(LinearLayout linearLayoutRoot, ArrayList<ObjQuestionOption> listOption, int i,
                                    Button btnNext) {
            TextView textView = new TextView(getApplicationContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    Math.round(130 * scale)
            );

            params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
            params.setMargins(Math.round(50 * scale), 0, Math.round(50 * scale), Math.round(50 * scale));
            textView.setLayoutParams(params);
            textView.setPadding(Math.round(50 * scale), 0, Math.round(50 * scale), 0);
            textView.setBackgroundResource(R.drawable.custom_edit_text);
            textView.setTextColor(getResources().getColorStateList(R.color.color_text_change));
            textView.setHint(listOption.get(i).getName());
            textView.setGravity(Gravity.CENTER_VERTICAL);
            textView.setInputType(InputType.TYPE_CLASS_TEXT);

            final DatePickerDialog.OnDateSetListener callbackSetDate = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    selectedDate.set(Calendar.YEAR, year);
                    selectedDate.set(Calendar.MONTH, monthOfYear);
                    selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    monthOfYear++;
                    textView.setText("" + year + "-" + monthOfYear + "-" + dayOfMonth);
                    answer = textView.getText().toString();
                    int yearOld = Calendar.getInstance().get(Calendar.YEAR) - year;
                    value = yearOld + "";
                    btnNext.setVisibility(View.VISIBLE);
                }

            };

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    selectedDate = Calendar.getInstance();
                    DatePickerDialog datePickerDialog = new DatePickerDialog(SanLocActivity.this,
                            android.R.style.Theme_Holo_Light_Dialog_NoActionBar, callbackSetDate,
                            selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH),
                            selectedDate.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    datePickerDialog.show();
                }
            });


            linearLayoutRoot.addView(textView);
        }


        private void resetButton(ArrayList<ObjQuestionOption> listOption, int position, int currentButton) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < listOption.size(); i++) {
                        if (listOption.get(i).getType().equals("button")) {
                            int id = position;
                            Button button = viewPager.findViewById(Integer.parseInt(id + "" + i));
                            //button.setId(R.id.btnLv1);
                            if (currentButton != i) {
                                button.setBackgroundResource(R.drawable.main_button);
                                button.setTextColor(getResources().getColorStateList(R.color.color_text_change));
                            } else {
                                answer = listOption.get(i).getName();
                                value = listOption.get(i).getValue();
                                button.setBackgroundResource(R.drawable.button_clicked);
                                button.setTextColor(getColor(R.color.white));
                            }
                        }
                    }
                }
            });

        }

        @Override
        public int getCount() {
            return listQuestion.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    private void createCheckbox(LinearLayout linearLayoutRoot, ArrayList<ObjQuestionOption> listOption, int i) {
        CheckBox checkBox = new CheckBox(getApplicationContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                Math.round(130 * scale)
        );
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.setMargins(Math.round(50 * scale), 0, Math.round(50 * scale), Math.round(50 * scale));
        checkBox.setLayoutParams(params);
        checkBox.setPadding(30, 0, 30, 0);
        //checkBox.setBackgroundResource(R.drawable.custom_edit_text);
        checkBox.setTextColor(getResources().getColorStateList(R.color.color_text_change));
        //button.setTextColor(R.drawable.text_white_to_green);
        checkBox.setText(listOption.get(i).getName());
        linearLayoutRoot.addView(checkBox);
    }
}
