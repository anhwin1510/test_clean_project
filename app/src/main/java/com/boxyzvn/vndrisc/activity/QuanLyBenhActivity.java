package com.boxyzvn.vndrisc.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.boxyzvn.vndrisc.R;

public class QuanLyBenhActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_quan_ly_benh;
        super.onCreate(savedInstanceState);
        setTextTitle("Quản lý bệnh", false);
        Button btnDaiThaoDuong = findViewById(R.id.btnDaiThaoDuong);
        btnDaiThaoDuong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(QuanLyBenhActivity.this, KhaoSatActivity.class));
            }
        });
    }
}
