package com.boxyzvn.vndrisc.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.boxyzvn.vndrisc.R;

import static com.boxyzvn.vndrisc.activity.WebviewActivity.PREVENTION_HIGH;
import static com.boxyzvn.vndrisc.activity.WebviewActivity.PREVENTION_LIGHT;
import static com.boxyzvn.vndrisc.activity.WebviewActivity.PREVENTION_LOW;
import static com.boxyzvn.vndrisc.activity.WebviewActivity.PREVENTION_MEDIUM;
import static com.boxyzvn.vndrisc.activity.WebviewActivity.PREVENTION_VERY_HIGH;

public class HuongDanActivity extends BaseActivity {
    private Button btnLv1, btnLv2, btnLv3, btnLv4, btnLv5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_huong_dan;
        super.onCreate(savedInstanceState);
        setTextTitle(getString(R.string.phong_chua_benh), false);
        btnLv1 = findViewById(R.id.btnLv1);
        btnLv2 = findViewById(R.id.btnLv2);
        btnLv3 = findViewById(R.id.btnLv3);
        btnLv4 = findViewById(R.id.btnLv4);
        btnLv5 = findViewById(R.id.btnLv5);
        btnLv1.setOnClickListener(mClickListener);
        btnLv2.setOnClickListener(mClickListener);
        btnLv3.setOnClickListener(mClickListener);
        btnLv4.setOnClickListener(mClickListener);
        btnLv5.setOnClickListener(mClickListener);

    }
    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btnLv1:
                    openWebview(PREVENTION_LOW, btnLv1.getText().toString());
                    break;
                case R.id.btnLv2:
                    openWebview(PREVENTION_LIGHT, btnLv2.getText().toString());
                    break;
                case R.id.btnLv3:
                    openWebview(PREVENTION_MEDIUM, btnLv3.getText().toString());
                    break;
                case R.id.btnLv4:
                    openWebview(PREVENTION_HIGH, btnLv4.getText().toString());
                    break;
                case R.id.btnLv5:
                    openWebview(PREVENTION_VERY_HIGH, btnLv5.getText().toString());
                    break;
            }
        }
    };
}
