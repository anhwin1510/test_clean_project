package com.boxyzvn.vndrisc.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        SharePref.getInstance().init(this);
        Handler handlerLogin = new Handler();

        String versionApp = "";
        try {
            versionApp = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }

        handlerLogin.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!SharePref.getInstance().isLogin()) {
                    startActivity(new Intent(SplashActivity.this, WalkThroughActivity.class));
                    finish();
                } else {
                    if (SharePref.getInstance().isCTV()) {
                        startActivity(new Intent(SplashActivity.this, HomeCTVActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    }
                    finish();
                }
            }
        }, 2000);
    }
}
