package com.boxyzvn.vndrisc.activity;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.login.LoginActivity;
import com.boxyzvn.vndrisc.adapter.pager.ViewPagerAdapterMain;
import com.boxyzvn.vndrisc.dialog.DialogManager;
import com.boxyzvn.vndrisc.model.ObjTab;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    public TextView mHeaderTitle;
    public ImageView icMenu, tabIcon;
    public FrameLayout fragmentMap;
    public ViewPager viewPager;
    public TabLayout tabLayout;

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ArrayList<ObjTab> listViewPager = new ArrayList<>();
    LinearLayout tabLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = (R.layout.menu_view);
        super.onCreate(savedInstanceState);
        setViewID();
        setFragment();
    }

    private void setViewID() {
        mHeaderTitle = findViewById(R.id.tvTitle);
        icMenu = findViewById(R.id.icMenu);
        viewPager = findViewById(R.id.pager);
        tabLayout = findViewById(R.id.tabLayout);
        fragmentMap = findViewById(R.id.fragmentMap);
    }

    private void setFragment() {
        setupHeader(getString(R.string.home), R.drawable.menu, R.drawable.search);
        setClickBtnSearch();
        setClickBtnMenu();
        setMenu();
    }

    public void setMenu() {
        setupViewPager(viewPager);

    }

    private void setupViewPager(ViewPager viewPager) {
        //Muon save lai thong tin cua bao nhieu page
        viewPager.setOffscreenPageLimit(3);

        ViewPagerAdapterMain adapter = new ViewPagerAdapterMain(getSupportFragmentManager());

        adapter.addFragment(listViewPager);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        customLayoutTab();
        //ShowDialog
       /* DialogManager.getInstance().init(this);
        DialogManager.getInstance().dialogError(Constant.DIALOG_LOCATION_ERROR);*/
    }

    private void customLayoutTab() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            tabLinearLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabIcon = (ImageView) tabLinearLayout.findViewById(R.id.icon);
            tabIcon.setImageResource(listViewPager.get(i).getIcon());
            tabLayout.getTabAt(i).setCustomView(tabLinearLayout);
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int positon = tab.getPosition();
                mHeaderTitle.setText(listViewPager.get(positon).getTitle());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void addNewTab(Fragment fragment, String title, int icon) {
        ObjTab objTab = new ObjTab(fragment, title, icon);
        listViewPager.add(objTab);
    }

    private void handleViewHome() {
        setupHeader(getString(R.string.home), R.drawable.menu, R.drawable.search);
        fragmentMap.setVisibility(View.VISIBLE);
    }

    private void setClickBtnSearch() {
    }

    private void setClickBtnMenu() {
        icMenu.setOnClickListener(v -> {
            if (!icMenu.getTag().equals(R.drawable.back)) {
                navigationView.getMenu().getItem(0).setChecked(false);
                navigationView.getMenu().getItem(1).setChecked(false);
                navigationView.getMenu().getItem(2).setChecked(false);
                navigationView.getMenu().getItem(3).setChecked(false);
                navigationView.getMenu().getItem(4).setChecked(false);
                drawerLayout.openDrawer(GravityCompat.START);
            } else {
                onBackPressed();
                setupHeader(getString(R.string.mycard), R.drawable.menu, R.drawable.search);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        DialogManager.getInstance().init(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        DialogManager.getInstance().init(null);

        // stopLocationService();
    }

    public boolean checkLocationOn() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return GpsStatus;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        switch (id) {
            case R.id.nav_home:
                viewPager.setCurrentItem(0);
                handleViewHome();
                break;
            case R.id.nav_bookmark:
                startActivity(new Intent(MainActivity.this, BookmarkActivity.class));
                break;

            case R.id.nav_about:
                startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
                break;
            case R.id.nav_logout:
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
        }

        return true;
    }

    private void setupViewFragment(Class fragmentData) {
        Class fragmentClass;
        fragmentClass = fragmentData;
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragmentMap, fragment, "new").commit();
    }

    public void setupHeader(String title, int imageMenu, int imageSearch) {
        mHeaderTitle.setText(title);
        icMenu.setImageResource(imageMenu);
        icMenu.setTag(imageMenu);
    }


}