package com.boxyzvn.vndrisc.activity.register.user;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.BaseActivity;
import com.boxyzvn.vndrisc.fragment.register.address.RegisterAddressFragment2;
import com.boxyzvn.vndrisc.fragment.register.address.RegisterAddressFragment3;
import com.boxyzvn.vndrisc.model.UserInfo;

public class RegisterAddressActivity extends BaseActivity {
    FragmentManager fm;
    UserInfo userInfo;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_address;
        super.onCreate(savedInstanceState);


        Intent intent = getIntent();
        initializeFullHeader("Đăng Ký", "Chọn địa chỉ");
        userInfo = (UserInfo) intent.getSerializableExtra("userPhone");
        if (userInfo != null)
            userInfo.setProvince("Đà nẵng");
        fm = getSupportFragmentManager();
        FragmentTransaction ft_add = fm.beginTransaction();
        if (intent.getAction() != null) {
            ft_add.add(R.id.frame_layout_address, RegisterAddressFragment3.newInstance());
        } else {
            ft_add.add(R.id.frame_layout_address, RegisterAddressFragment2.newInstance());
        }

        ft_add.commit();

    }
}
