package com.boxyzvn.vndrisc.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBack;
import com.boxyzvn.vndrisc.model.ObjKetQua;
import com.boxyzvn.vndrisc.model.ObjQuestion;
import com.boxyzvn.vndrisc.model.ObjQuestionRule;
import com.boxyzvn.vndrisc.model.ObjQuestionRuleExplain;
import com.boxyzvn.vndrisc.model.ObjSumaryRule;
import com.boxyzvn.vndrisc.model.ObjSumaryRuleExplain;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import static com.boxyzvn.vndrisc.activity.SanLocActivity.DA_CHAN_DOAN;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.HEIGH_VIEW;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.JSON_CHUA_CHAN_DOAN;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.KET_QUA;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.KET_QUA_CHECK_BOX;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.KHUYEN_NGHI;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.QUESTION_LIST;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.QUESTION_RULE_LIST;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.SUMARY_OBJ;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.TOTAL_SCORE;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.VALUE_KET_QUA;
import static com.boxyzvn.vndrisc.sharedpreferences.Constants.MAX_RETRY;

public class KetQuaActivity extends BaseActivity {

    boolean isDaChanDoan;
    ArrayList<String> listAnswer;
    ArrayList<String> listValue;
    String khuyenNghi;
    String jsonChuaChanDoan;
    String riskLevel;
    ArrayList<String> listAnswerCheckBox;
    ArrayList<ObjKetQua> listKetQua = new ArrayList<>();
    private int totalScore = 0;
    private int retryTimes = 0;
    private boolean isMale = false;
    private RelativeLayout popupKetQua;
    private TextView tvHeader, tvContent, tvPoint;
    private Button btnNext;
    private ObjSumaryRuleExplain objSumary;
    private RelativeLayout layoutHeaderPopup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_ket_qua;
        super.onCreate(savedInstanceState);
        popupKetQua = (RelativeLayout) findViewById(R.id.popupKetqua);
        layoutHeaderPopup = (RelativeLayout) findViewById(R.id.layoutHeader);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvContent = (TextView) findViewById(R.id.tvContent);
        tvPoint = (TextView) findViewById(R.id.tvPoint);
        btnNext = (Button) findViewById(R.id.btnNextKhuyenCao);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KetQuaActivity.this, KhuyenCaoActivity.class);
                intent.putExtra(JSON_CHUA_CHAN_DOAN, jsonChuaChanDoan);
                intent.putExtra(SUMARY_OBJ, objSumary);
                intent.putExtra(TOTAL_SCORE, totalScore);
                startActivity(intent);

            }
        });
        setTextTitle("Xem lại phần trả lời", false);
        String action = getIntent().getStringExtra(DA_CHAN_DOAN);
        if (new Gson().toJson(listAnswer).contains("Nam")) {
            isMale = true;
        }
        listAnswer = (ArrayList<String>) getIntent().getSerializableExtra(KET_QUA);
        listValue = (ArrayList<String>) getIntent().getSerializableExtra(VALUE_KET_QUA);

        if (action != null) {
            isDaChanDoan = true;
            listAnswerCheckBox = (ArrayList<String>) getIntent().getSerializableExtra(KET_QUA_CHECK_BOX);
            khuyenNghi = getIntent().getStringExtra(KHUYEN_NGHI);
        } else {
            isDaChanDoan = false;
            jsonChuaChanDoan = getIntent().getStringExtra(JSON_CHUA_CHAN_DOAN);
            String jsonQuestionRule = getIntent().getStringExtra(QUESTION_RULE_LIST);
            Log.e("HUNG", "rule " + jsonQuestionRule);
            tinhDiem(jsonQuestionRule);
        }

        String jsonQuestion = getIntent().getStringExtra(QUESTION_LIST);
        Type type = new TypeToken<ArrayList<ObjQuestion>>() {
        }.getType();

        Log.e("HUNG", "rule value" + listValue);
        ArrayList<ObjQuestion> listQuestionData = new Gson().fromJson(jsonQuestion, type);

        LinearLayout linearLayout = findViewById(R.id.layoutKetqua);
        if (listAnswer.size() == listQuestionData.size())
            for (int i = 0; i < listQuestionData.size(); i++) {
                TextView tvQuestion = new TextView(getApplicationContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );

                //params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
                params.setMargins(Math.round(20 * scale), 0, Math.round(20 * scale), 0);
                tvQuestion.setLayoutParams(params);
                tvQuestion.setTextColor(getColor(R.color.black));
                tvQuestion.setText(listQuestionData.get(i).getTitle() + ": " + listQuestionData.get(i).getContent());
                tvQuestion.setGravity(Gravity.LEFT);
                tvQuestion.setTextSize(20);

                linearLayout.addView(tvQuestion);
                if (!listQuestionData.get(i).getOptions().get(0).getType().equals("checkbox")) {
                    TextView tvAnswer = new TextView(getApplicationContext());
                    LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    );

                    //params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
                    params2.setMargins(Math.round(20 * scale), 0, Math.round(20 * scale), Math.round(30 * scale));
                    tvAnswer.setLayoutParams(params2);
                    tvAnswer.setTextColor(getColor(R.color.colorAccent));
                    tvAnswer.setText("・" + listAnswer.get(i));
                    tvAnswer.setTextSize(22);
                    tvAnswer.setGravity(Gravity.LEFT);
                    linearLayout.addView(tvAnswer);
                    ObjKetQua objKetQua = new ObjKetQua(listQuestionData.get(i).getTitle(), listQuestionData.get(i).getContent(), listAnswer.get(i));
                    listKetQua.add(objKetQua);
                } else {
                    String anwser = "";
                    for (int j = 0; j < listAnswerCheckBox.size(); j++) {
                        TextView tvAnswer = new TextView(getApplicationContext());
                        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                        );
                        if (j == listAnswerCheckBox.size() - 1) {
                            anwser += "・" + listAnswerCheckBox.get(j);
                        } else {
                            anwser += "・" + listAnswerCheckBox.get(j) + "\n";
                        }
                        //params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
                        params2.setMargins(Math.round(20 * scale), 0, Math.round(20 * scale), Math.round(30 * scale));
                        tvAnswer.setLayoutParams(params2);
                        tvAnswer.setTextColor(getColor(R.color.colorAccent));
                        tvAnswer.setText("・" + listAnswerCheckBox.get(j));
                        tvAnswer.setTextSize(22);
                        tvAnswer.setGravity(Gravity.LEFT);
                        linearLayout.addView(tvAnswer);
                    }
                    ObjKetQua objKetQua = new ObjKetQua(listQuestionData.get(i).getTitle(), listQuestionData.get(i).getContent(), anwser);
                    listKetQua.add(objKetQua);
                }

            }
        Button button = new Button(getApplicationContext());
        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                Math.round(HEIGH_VIEW * scale)
        );
        params3.gravity = Gravity.CENTER_HORIZONTAL;

        params3.setMargins(Math.round(50 * scale), 0, Math.round(50 * scale), Math.round(50 * scale));
        button.setLayoutParams(params3);
        button.setBackgroundResource(R.drawable.custom_green_button);
        button.setTextColor(getResources().getColorStateList(R.color.white_to_green));
        button.setText("HOÀN THÀNH");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String token = com.boxyzvn.vndrisc.sharedpreferences.SharePref.getInstance().getTokenLogin();
                String appVer = "";
                try {
                    appVer = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {

                }
                String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                Log.e("KetQua", new Gson().toJson(listKetQua));
                Log.e("KetQua", "Điểm: " + totalScore);
                if (isDaChanDoan) {
                    SharePref.getInstance().setSendDaChanDoanSuccess(false);
                    SharePref.getInstance().saveJsonKetQuaDaChanDoan(new Gson().toJson(listKetQua));
                    if (checkNetworkStatus()) {
                        callAPISendDaChanDoan(token, appVer, androidId);
                    } else {
                        startActivity(new Intent(KetQuaActivity.this, KhuyenNghiActivity.class).putExtra(KHUYEN_NGHI, khuyenNghi));
                        finish();
                    }
                } else {
                    SharePref.getInstance().setSendChuaChanDoanSuccess(false);
                    SharePref.getInstance().saveJsonKetQuaChuaChanDoan(new Gson().toJson(listKetQua));
                    if (checkNetworkStatus()) {
                        callAPISendChuaChanDoan(token, appVer, androidId);
                    } else {
                        setupDataToPopup();
                    }
                }
            }
        });

        linearLayout.addView(button);
    }

    private void callAPISendDaChanDoan(String token, String appVer, String androidId) {
        ApiServiceManager.getInstance().sendDaChanDoan(token, androidId, appVer, new Gson().toJson(listKetQua), new CallBack() {
            @Override
            public void onComplete() {
                SharePref.getInstance().setSendDaChanDoanSuccess(true);
                SharePref.getInstance().saveJsonKetQuaDaChanDoan("");
                startActivity(new Intent(KetQuaActivity.this, KhuyenNghiActivity.class).putExtra(KHUYEN_NGHI, khuyenNghi));
                finish();
            }

            @Override
            public void onError(String errorMsg) {
                if (retryTimes < MAX_RETRY) {
                    retryTimes++;
                    callAPISendDaChanDoan(token, appVer, androidId);
                }
            }
        });
    }

    private void callAPISendChuaChanDoan(String token, String appVer, String androidId) {
        ApiServiceManager.getInstance().sendChuaChanDoan(token, androidId, appVer, new Gson().toJson(listKetQua), totalScore, new CallBack() {
            @Override
            public void onComplete() {
                SharePref.getInstance().setSendChuaChanDoanSuccess(true);
                SharePref.getInstance().saveJsonKetQuaChuaChanDoan("");
                setupDataToPopup();
            }

            @Override
            public void onError(String errorMsg) {
                if (retryTimes < MAX_RETRY) {
                    retryTimes++;
                    callAPISendChuaChanDoan(token, appVer, androidId);
                }
            }
        });
    }

    private void setupDataToPopup() {
        try {
            JSONObject jsonObject = new JSONObject(jsonChuaChanDoan);
            JSONObject dataSumary = jsonObject.getJSONObject("summary");
            Type typeRule = new TypeToken<ObjSumaryRule>() {
            }.getType();
            ObjSumaryRule sumaryRule = new Gson().fromJson(dataSumary.toString(), typeRule);
            ArrayList<ObjSumaryRuleExplain> arrayList = sumaryRule.getExplain();
            for (ObjSumaryRuleExplain objSumaryRuleExplain : arrayList) {
                String condition = objSumaryRuleExplain.getCondition().get(0);
                ScriptEngineManager factory = new ScriptEngineManager();
                ScriptEngine engine = factory.getEngineByName("js");
                Boolean isCorrectCondition = (Boolean) engine.eval(totalScore + condition);
                if (isCorrectCondition) {
                    riskLevel = objSumaryRuleExplain.getPoint().getRisk_level();
                    objSumary = objSumaryRuleExplain;
                    setColorTitle();
                    tvHeader.setText(riskLevel);
                    tvPoint.setText(totalScore + " Điểm");
                    popupKetQua.setVisibility(View.VISIBLE);
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }

    private void tinhDiem(String jsonQuestionRule) {
        try {
            JSONObject json = new JSONObject(Objects.requireNonNull(jsonQuestionRule));
            Iterator<String> iter = json.keys();
            while (iter.hasNext()) {
                String spot = iter.next();
                JSONObject valueJson = json.getJSONObject(spot);
                Log.e("HUNG", "rule " + valueJson.toString());
                Type typeRule = new TypeToken<ObjQuestionRule>() {
                }.getType();

                ObjQuestionRule questionRule = new Gson().fromJson(valueJson.toString(), typeRule);

                //cau hoi don gian
                if (questionRule.getQuestion().size() == 1) {
                    int id = questionRule.getQuestion().get(0);
                    String value = listValue.get(id - 1);

                    for (ObjQuestionRuleExplain objQuestionRuleExplain : questionRule.getExplain()) {
                        //truong hợp có 1 condition

                        Log.e("HUNG", "vaulee " + value);
                        if (objQuestionRuleExplain.getCondition().size() == 1) {
                            if (objQuestionRuleExplain.getCondition().toString().contains(">") || objQuestionRuleExplain.getCondition().toString().contains("<")) {
                                String condition = objQuestionRuleExplain.getCondition().get(0);
                                ScriptEngineManager factory = new ScriptEngineManager();
                                ScriptEngine engine = factory.getEngineByName("js");
                                Boolean isCorrectCondition = (Boolean) engine.eval(value + condition);
                                Log.e("HUNG", "condition " + value + condition);
                                if (isCorrectCondition) {
                                    totalScore += objQuestionRuleExplain.getPoint();
                                    Log.e("HUNG", "rule total point" + totalScore);
                                    break;
                                }

                            } else {
                                if (value.equals(objQuestionRuleExplain.getCondition().get(0))) {
                                    totalScore += objQuestionRuleExplain.getPoint();
                                    Log.e("HUNG", "rule total point" + totalScore);
                                }
                            }

                        }
                    }
                }
                //cau hoi bmi vong bung
                else {
                    //bmi
                    if (spot.equals("bmi")) {
                        int idHeight = questionRule.getQuestion().get(0);
                        String valueHeight = listValue.get(idHeight - 1);
                        int idWeight = questionRule.getQuestion().get(1);
                        String valueWeight = listValue.get(idWeight - 1);
                        float bmi = Float.parseFloat(valueWeight) / (Float.parseFloat(valueHeight) * Float.parseFloat(valueHeight)) * 10000;
                        for (ObjQuestionRuleExplain objQuestionRuleExplain : questionRule.getExplain()) {
                            String condition = objQuestionRuleExplain.getCondition().get(0);
                            ScriptEngineManager factory = new ScriptEngineManager();
                            ScriptEngine engine = factory.getEngineByName("js");
                            Boolean isCorrectCondition = (Boolean) engine.eval(bmi + condition);
                            if (isCorrectCondition) {
                                totalScore += objQuestionRuleExplain.getPoint();
                                Log.e("HUNG", "rule total point bmi" + totalScore);
                                break;
                            }
                            Log.e("HUNG", "bmi: " + bmi + "\n H: " + valueHeight + "W: " + valueWeight);
                        }
                    }
                    //vong bung
                    else {
                        for (ObjQuestionRuleExplain objQuestionRuleExplain : questionRule.getExplain()) {
                            if (isMale) {
                                String sex = objQuestionRuleExplain.getCondition().get(1);
                                if (sex.equals("male")) {
                                    //lấy vị trí câu hỏi vòng bụng [1,5]
                                    int idHeight = questionRule.getQuestion().get(1);
                                    String valueVongBung = listValue.get(idHeight - 1);
                                    String condition = objQuestionRuleExplain.getCondition().get(0);
                                    ScriptEngineManager factory = new ScriptEngineManager();
                                    ScriptEngine engine = factory.getEngineByName("js");
                                    Boolean isCorrectCondition = (Boolean) engine.eval(valueVongBung + condition);
                                    if (isCorrectCondition) {
                                        totalScore += objQuestionRuleExplain.getPoint();
                                        Log.e("HUNG", "rule value VongBung" + totalScore);
                                        break;
                                    }
                                }
                            } else {
                                String sex = objQuestionRuleExplain.getCondition().get(1);
                                if (sex.equals("female")) {
                                    //lấy vị trí câu hỏi vòng bụng [1,5]
                                    int idHeight = questionRule.getQuestion().get(1);
                                    String valueVongBung = listValue.get(idHeight - 1);
                                    String condition = objQuestionRuleExplain.getCondition().get(0);
                                    ScriptEngineManager factory = new ScriptEngineManager();
                                    ScriptEngine engine = factory.getEngineByName("js");
                                    Boolean isCorrectCondition = (Boolean) engine.eval(valueVongBung + condition);
                                    if (isCorrectCondition) {
                                        totalScore += objQuestionRuleExplain.getPoint();
                                        Log.e("HUNG", "rule value VongBung" + totalScore);
                                        break;
                                    }
                                }
                            }

                        }
                    }

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }

    private void setColorTitle() {
        switch (objSumary.getPoint().getRisk_level_id()) {
            case 1:
                layoutHeaderPopup.setBackgroundTintList(getColorStateList(R.color.lv1));
                break;
            case 2:
                layoutHeaderPopup.setBackgroundTintList(getColorStateList(R.color.lv2));
                break;
            case 3:
                layoutHeaderPopup.setBackgroundTintList(getColorStateList(R.color.lv3));
                break;
            case 4:
                layoutHeaderPopup.setBackgroundTintList(getColorStateList(R.color.lv4));
                break;
            case 5:
                layoutHeaderPopup.setBackgroundTintList(getColorStateList(R.color.lv5));

                break;

        }
    }
}
