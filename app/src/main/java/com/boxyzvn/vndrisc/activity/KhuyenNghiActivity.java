package com.boxyzvn.vndrisc.activity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.model.ObjKhuyenNghi;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.boxyzvn.vndrisc.activity.SanLocActivity.KHUYEN_NGHI;

public class KhuyenNghiActivity extends BaseActivity {
    private String khuyenNghi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_khuyen_nghi;
        super.onCreate(savedInstanceState);
        setTextTitle("Khuyến Nghị", false);

        khuyenNghi = getIntent().getStringExtra(KHUYEN_NGHI);
        Type type = new TypeToken<ArrayList<ObjKhuyenNghi>>() {
        }.getType();

        ArrayList<ObjKhuyenNghi> listKhuyenNghi = new Gson().fromJson(khuyenNghi, type);
        LinearLayout linearLayout = findViewById(R.id.layoutKhuyenNghi);
        for (ObjKhuyenNghi objKhuyenNghi : listKhuyenNghi) {
            TextView tvQuestion = new TextView(getApplicationContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(Math.round(20 * scale), 0, Math.round(20 * scale), 0);
            tvQuestion.setLayoutParams(params);
            tvQuestion.setTextColor(getColor(R.color.black));
            tvQuestion.setText(objKhuyenNghi.getName() + ": ");
            tvQuestion.setGravity(Gravity.LEFT);
            tvQuestion.setTextSize(25);

            linearLayout.addView(tvQuestion);
            TextView tvAnswer = new TextView(getApplicationContext());
            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );

            params2.setMargins(Math.round(20 * scale), 0, Math.round(20 * scale), Math.round(30 * scale));
            tvAnswer.setLayoutParams(params2);
            tvAnswer.setTextColor(getColor(R.color.colorAccent));
            tvAnswer.setText("・" + objKhuyenNghi.getValue());
            tvAnswer.setTextSize(22);
            tvAnswer.setGravity(Gravity.LEFT);
            linearLayout.addView(tvAnswer);
        }
    }
}
