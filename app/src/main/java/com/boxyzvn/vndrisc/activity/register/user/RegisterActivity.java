package com.boxyzvn.vndrisc.activity.register.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.BaseActivity;
import com.boxyzvn.vndrisc.activity.login.LoginActivity;
import com.boxyzvn.vndrisc.fragment.register.RegisterEmailFragment;
import com.boxyzvn.vndrisc.model.UserInfo;

public class RegisterActivity extends BaseActivity {

    public static final String TAG = "register";
    FragmentManager fm;
    UserInfo userInfo;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_register;
        super.onCreate(savedInstanceState);
        initializeFullHeader("Đăng Ký", "Điền vào những thông tin sau");

        ImageView mBackBtn = (ImageView) findViewById(R.id.btn_back);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            }
        });
        fm = getSupportFragmentManager();
        FragmentTransaction ft_add = fm.beginTransaction();
        ft_add.add(R.id.frame_layout, RegisterEmailFragment.newInstance());
        ft_add.commit();
    }


}
