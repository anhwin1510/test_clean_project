package com.boxyzvn.vndrisc.activity;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.boxyzvn.vndrisc.R;

/**
 * ドロワーに表示する内容を設定するクラス
 */
public class MenuView extends RelativeLayout {
    Context mContext;
    View viewGroup;
    TextView tvHome, tvThongTinBenh, tvQuanLyBenh, tvThucPham, tvNhacNho, tvLogout, tvVersion;

    public MenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        setupView();
    }

    private void setupView() {
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewGroup = mInflater.inflate(R.layout.menu_view, this);
        tvHome = viewGroup.findViewById(R.id.tvHome);
        tvThongTinBenh = viewGroup.findViewById(R.id.tvTermOfUse);
        tvThucPham = viewGroup.findViewById(R.id.tvAppInfo);
        tvQuanLyBenh = viewGroup.findViewById(R.id.tvPolicy);
        tvLogout = viewGroup.findViewById(R.id.tvLogout);
        tvVersion = viewGroup.findViewById(R.id.tvBuild);
        tvHome.setOnClickListener(mOncOnClickListener);
        tvThongTinBenh.setOnClickListener(mOncOnClickListener);
        tvThucPham.setOnClickListener(mOncOnClickListener);
        tvQuanLyBenh.setOnClickListener(mOncOnClickListener);
        tvLogout.setOnClickListener(mOncOnClickListener);

        try {
            int versionCode = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionCode;
            String versionName = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionName;
            tvVersion.setText("Build " + versionCode + " - " + "v"+versionName);

        } catch (PackageManager.NameNotFoundException e) {

        }

    }

    private OnClickListener mOncOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

//    private void setStatusButton(final View view) {
//        view.setClickable(false);
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                view.setClickable(true);
//            }
//        }, TIME_RESET_BUTTON);
//    }

}
