package com.boxyzvn.vndrisc.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.boxyzvn.vndrisc.R;

public class DubaoActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_dubao;
        super.onCreate(savedInstanceState);
        setTextTitle("Dự báo nguy cơ mắc bệnh", false);

        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DubaoActivity.this, SanLocActivity.class));
            }
        });
    }
}
