package com.boxyzvn.vndrisc.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.login.LoginActivity;
import com.boxyzvn.vndrisc.model.ObjPagerWalkthrough;
import com.boxyzvn.vndrisc.utils.SharePref;

import java.util.ArrayList;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;


public class WalkThroughActivity extends BaseActivity {
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private ArrayList<ObjPagerWalkthrough> layouts = new ArrayList<>();
    private Button btnSkip, btnNext, btnComplete;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.acvitity_walkthrough;
        super.onCreate(savedInstanceState);

        boolean isFirstInstall = SharePref.getBoolean(this, SharePref.Key.IS_FIRST_INSTALLED, true);
        if (!isFirstInstall) {
            launchHomeScreen();
            return;
        }

        initView();

        addPager();
        // adding bottom dots
        addBottomDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        View.OnClickListener nextScreen = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchHomeScreen();
            }
        };
        btnSkip.setOnClickListener(nextScreen);
        btnComplete.setOnClickListener(nextScreen);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < layouts.size()) {
                    // move to next screen
                    viewPager.setCurrentItem(current);
                } else {
                    launchHomeScreen();
                }
            }
        });
    }

    private void initView() {
        viewPager = (ViewPager) findViewById(R.id.pagerWalkthrough);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        btnSkip = (Button) findViewById(R.id.btn_skip);
        btnNext = (Button) findViewById(R.id.btn_next);
        btnComplete = (Button) findViewById(R.id.btn_complete);
    }

    private void addPager() {
        layouts.add(new ObjPagerWalkthrough("Quản lý bệnh", R.drawable.onboaring1));
        layouts.add(new ObjPagerWalkthrough(getString(R.string.phong_chua_benh), R.drawable.onboaring2));
        layouts.add(new ObjPagerWalkthrough("Thông tin về bệnh", R.drawable.onboaring3));
        layouts.add(new ObjPagerWalkthrough("Nhắc nhở", R.drawable.onboaring4));
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.size()];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        SharePref.putBoolean(this, SharePref.Key.IS_FIRST_INSTALLED, false);
        startActivity(new Intent(WalkThroughActivity.this, LoginActivity.class));
        finish();
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.size() - 1) {
                // last page. make button text to GOT IT
                btnNext.setVisibility(View.GONE);
                btnSkip.setVisibility(View.GONE);
                btnComplete.setVisibility(View.VISIBLE);
            } else {
                // still pages are left
                //btnSkip.setVisibility(View.VISIBLE);
                btnNext.setVisibility(View.VISIBLE);
                btnComplete.setVisibility(View.GONE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.walkthrough_1, container, false);
            container.addView(view);
            TextView title = view.findViewById(R.id.tv_title_walkthrough);
            title.setText(layouts.get(position).getTitle());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                title.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
            }
            ImageView imageView = view.findViewById(R.id.img_walkthrough);
            imageView.setImageResource(layouts.get(position).getImage());
            return view;
        }

        @Override
        public int getCount() {
            return layouts.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
