package com.boxyzvn.vndrisc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.boxyzvn.vndrisc.R;

public class KhaoSatActivity extends BaseActivity {
    private Button btnDaChanDoan, btnChuaChanDoan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_khao_sat;
        super.onCreate(savedInstanceState);
        setTextTitle("Khảo Sát", false);
        btnChuaChanDoan = findViewById(R.id.btnChuaChanDoan);
        btnDaChanDoan = findViewById(R.id.btnDaChanDoan);
        btnDaChanDoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(KhaoSatActivity.this, DaChanDoanActivity.class));
            }
        });
        btnChuaChanDoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(KhaoSatActivity.this, ChuaDuocChanDoanActivity.class));
            }
        });

    }
}
