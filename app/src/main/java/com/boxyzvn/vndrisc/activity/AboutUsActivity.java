package com.boxyzvn.vndrisc.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.boxyzvn.vndrisc.R;

public class AboutUsActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutId = (R.layout.activity_about_us);
        setTitleTwoLine("About","");
    }
}
