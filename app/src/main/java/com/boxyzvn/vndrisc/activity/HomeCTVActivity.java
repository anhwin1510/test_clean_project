package com.boxyzvn.vndrisc.activity;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.login.LoginActivity;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBack;
import com.boxyzvn.vndrisc.api.callback.CallBackListPatients;
import com.boxyzvn.vndrisc.model.UserInfo;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.UIHelper;
import com.google.gson.Gson;

import java.util.ArrayList;

public class HomeCTVActivity extends BaseActivity {
    private ImageView imgMenu;
    private DrawerLayout mDrawLayout;
    private TextView tvHome, tvTermOfUse, tvPolicy, tvAppInfo, tvLogout, tvUserName, tvEmpty;
    private TableLayout tbLayout;
    private ScrollView scrollView;
    private ArrayList<UserInfo> arrayListUserInfo = new ArrayList<>();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 1;
    private int lastPage = -1;
    private String wardID = "";
    protected MenuView menuView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_home_ctv;
        super.onCreate(savedInstanceState);
        menuView = (MenuView) findViewById(R.id.menu_view);
        mDrawLayout = (DrawerLayout) findViewById(R.id.drawerLayoutMain);

        tbLayout = findViewById(R.id.tableView);
        tvUserName = findViewById(R.id.txt_UserMenu);
        scrollView = findViewById(R.id.scrollViewCTV);
        tvEmpty = findViewById(R.id.tvEmpty);
        tvHome = findViewById(R.id.tvHome);
        tvTermOfUse = findViewById(R.id.tvTermOfUse);
        tvAppInfo = findViewById(R.id.tvAppInfo);
        tvPolicy = findViewById(R.id.tvPolicy);
        tvLogout = findViewById(R.id.tvLogout);
        tvHome.setOnClickListener(mOncOnClickListener);
        tvTermOfUse.setOnClickListener(mOncOnClickListener);
        tvAppInfo.setOnClickListener(mOncOnClickListener);
        tvPolicy.setOnClickListener(mOncOnClickListener);
        tvLogout.setOnClickListener(mOncOnClickListener);
        imgMenu = findViewById(R.id.imgMenu);
        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawLayout.openDrawer(GravityCompat.START);
            }
        });
        if (checkNetworkStatus()) {
            getUserInfo();
        }

        mSwipeRefreshLayout = findViewById(R.id.refreshView);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //TODO call API LIST BENH NHAN
                mSwipeRefreshLayout.setRefreshing(false);
                if (checkNetworkStatus()) {
                    recreate();
                }

                //Toast.makeText(HomeCTVActivity.this, "OK", Toast.LENGTH_SHORT).show();
            }
        });

        scrollView.getViewTreeObserver()
                .addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {
                        mSwipeRefreshLayout.setEnabled(false);
                        View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
                        int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                        // if diff is zero, then the bottom has been reached
                        if (diff <= 10)
                            // do stuff
                            scrollView.post(new Runnable() {

                                @Override
                                public void run() {
                                    //scrollView.fullScroll(View.FOCUS_UP);
                                    currentPage++;
                                    if (currentPage > lastPage) {
                                        //Toast.makeText(HomeCTVActivity.this, "Đã đến cuối danh sách", Toast.LENGTH_SHORT).show();
                                    } else {
                                        getListPatientsInfo(wardID);
                                    }
                                    Log.e("Scrollview", "bottom");
                                }

                            });
                        if (!scrollView.canScrollVertically(-1)) {
                            // Top of scroll view.
                            mSwipeRefreshLayout.setEnabled(true);
                        }


                    }
                });

    }

    private void createRow(ArrayList<UserInfo> arrayListUserInfo) {
        for (UserInfo userInfo : arrayListUserInfo) {
            TableRow tr = new TableRow(this);
            TableLayout.LayoutParams trParam = new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            tr.setLayoutParams(trParam);

            TextView b3 = new TextView(this);
            b3.setText(userInfo.getName());
            b3.setTextColor(getColor(R.color.colorAccent));
            b3.setPadding(10, 10, 10, 10);
            b3.setTextSize(17);
            tr.addView(b3);

            TextView b4 = new TextView(this);
            b4.setPadding(10, 10, 10, 10);
            b4.setTextSize(17);
            b4.setText(userInfo.getAddress());
            b4.setTextColor(getColor(R.color.colorAccent));
            tr.addView(b4);

            TextView b5 = new TextView(this);
            b5.setPadding(10, 10, 10, 10);
            b5.setText(userInfo.getPhone());

            b5.setTextColor(getColor(R.color.colorAccent));
            b5.setTextSize(17);
            tr.addView(b5);

            CheckBox b6 = new CheckBox(this);
            TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            b6.setPadding(10, 10, 10, 10);
            b6.setChecked(userInfo.isCTV());
            b6.setClickable(false);
            b6.setButtonTintList(ColorStateList.valueOf(getColor(R.color.colorAccent)));
            //b6.setGravity(Gravity.CENTER_VERTICAL);
            b6.setLayoutParams(params);
            tr.addView(b6);

            TextView b7 = new TextView(this);
            b7.setPadding(10, 10, 10, 10);
            b7.setText(userInfo.getDate_of_birth());
            b7.setTextColor(getColor(R.color.colorAccent));
            b7.setTextSize(17);
            tr.addView(b7);

            TextView b8 = new TextView(this);
            b8.setPadding(10, 10, 10, 10);
            b8.setText(userInfo.getWard());
            b8.setTextColor(getColor(R.color.colorAccent));
            b8.setTextSize(17);
            tr.addView(b8);

            TextView b9 = new TextView(this);
            b9.setPadding(10, 10, 10, 10);
            b9.setText(userInfo.getDistrict());
            b9.setTextColor(getColor(R.color.colorAccent));
            b9.setTextSize(17);
            tr.addView(b9);

            TextView b10 = new TextView(this);
            b10.setPadding(10, 10, 10, 10);
            b10.setText(userInfo.getProvince());
            b10.setTextColor(getColor(R.color.colorAccent));
            b10.setTextSize(17);
            tr.addView(b10);

            TextView b11 = new TextView(this);
            b11.setPadding(10, 10, 10, 10);
            b11.setText(userInfo.getResidential());
            b11.setTextColor(getColor(R.color.colorAccent));
            b11.setTextSize(17);
            b11.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            tr.addView(b11);

            final View vline = new View(this);
            vline.setLayoutParams(new
                    TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
            vline.setBackgroundColor(getColor(R.color.colorAccent));
            // add line below heading
            tbLayout.addView(tr);
            tbLayout.addView(vline);
        }
    }

    private View.OnClickListener mOncOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mDrawLayout.closeDrawer((GravityCompat.START));
            switch (v.getId()) {
                case R.id.tvHome:
                    break;
                case R.id.tvTermOfUse:
                    openWebview("https://termofuse.boxyzvn.com", "");
                    break;
                case R.id.tvAppInfo:
                    startActivity(new Intent(HomeCTVActivity.this, AppInfoActivity.class));
                    break;
                case R.id.tvPolicy:
                    openWebview("https://privacy.boxyzvn.com/", "");
                    break;
                case R.id.tvLogout:
                    Intent intent = new Intent(HomeCTVActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    SharePref.getInstance().setLoginStatus(false);
                    SharePref.getInstance().setCTVStatus(false);
                    break;
                default:

                    break;
            }
        }
    };

    public void getUserInfo() {
        String token = SharePref.getInstance().getTokenLogin();
        UIHelper.showProgress(this);
        String appVer = "";
        try {
            appVer = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().getUserInfo(token, androidId, appVer, new CallBack() {
            @Override
            public void onComplete() {
                UIHelper.hideProgress();
                UserInfo userInfo = SharePref.getInstance().getInfoLogin();
                wardID = userInfo.getWard_id();
                tvUserName.setText(userInfo.getName());
                Log.e("USERRR", new Gson().toJson(userInfo));
                getListPatientsInfo(wardID);
            }

            @Override
            public void onError(String errorMsg) {

                UIHelper.hideProgress();
                //Toast.makeText(HomeCTVActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getListPatientsInfo(String wardID) {
        String token = SharePref.getInstance().getTokenLogin();
        UIHelper.showProgress(this);
        String appVer = "";
        try {
            appVer = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().getListBenhNhan(token, androidId, appVer, currentPage, wardID, new CallBackListPatients() {
            @Override
            public void onComplete(ArrayList<UserInfo> userInfoArrayList, int totalPage) {
                if (userInfoArrayList.size() > 0) {
                    tvEmpty.setVisibility(View.GONE);
                    scrollView.setVisibility(View.VISIBLE);
                    createRow(userInfoArrayList);
                } else {
                    tvEmpty.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                }
                lastPage = totalPage;
                UIHelper.hideProgress();

            }

            @Override
            public void onError(String errorMsg) {
                UIHelper.hideProgress();
                //Toast.makeText(HomeCTVActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
