package com.boxyzvn.vndrisc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.login.LoginActivity;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.GUIUtils;

public class ChuaDuocChanDoanActivity extends BaseActivity {

    private Button btnDuBao, btnTheoDoi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_chua_duoc_chan_doan;
        super.onCreate(savedInstanceState);
        setTextTitle("Chưa Được Chẩn Đoán", false);
        btnDuBao = findViewById(R.id.btnDubao);
        btnTheoDoi = findViewById(R.id.btnTheoDoi);

        btnDuBao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChuaDuocChanDoanActivity.this, DubaoActivity.class));
            }
        });
        btnTheoDoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int point = SharePref.getInstance().getPoint();
                if (point != -1)
                    startActivity(new Intent(ChuaDuocChanDoanActivity.this, KhuyenCaoActivity.class));
                else {
                    GUIUtils.showAlertDialog(ChuaDuocChanDoanActivity.this, getString(R.string.chua_co_du_lieu_chan_doan));
                }
            }
        });
    }
}
