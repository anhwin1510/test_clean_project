package com.boxyzvn.vndrisc.activity;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.adapter.ThongTinVeBenhAdapter;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBackInfoDiseaseList;
import com.boxyzvn.vndrisc.model.ObjDiseaseInfo;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.UIHelper;

import java.util.ArrayList;

public class ThongTinVeBenhActivity extends BaseActivity implements ThongTinVeBenhAdapter.ItemClickListener {

    private RecyclerView listDiseaseInfo;
    private ThongTinVeBenhAdapter mAdapterDiseaseInfo;
    private ArrayList<ObjDiseaseInfo> mArrayDiseaseInfo;
    private int currentPage = 1;
    private int lastPage = -1;
    private int vissibleThreshold = 2;
    private TextView tvEmpty;
    private SwipeRefreshLayout pullToRefresh;
    private Handler handlerRefresh = new Handler();
    private boolean checkPullToRefresh = false;
    private boolean checkSetupLoadmore = true;
    private boolean isRunning = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_thong_tin_ve_benh;
        super.onCreate(savedInstanceState);
        setTextTitle(getString(R.string.thong_tin_ve_benh), false);
        setViewID();
        setupList();
        setPullToRefresh();
    }

    private void setupList() {
        currentPage = 1;
        mArrayDiseaseInfo = new ArrayList<>();
        mAdapterDiseaseInfo = new ThongTinVeBenhAdapter(this, mArrayDiseaseInfo);
        listDiseaseInfo.setAdapter(mAdapterDiseaseInfo);
        mAdapterDiseaseInfo.setClickListener(this);
        listDiseaseInfo.setLayoutManager(new LinearLayoutManager(this));
        getInfoDiseaseList();
        if (checkSetupLoadmore) {
            setupLoadMore();
        }
    }

    private void getInfoDiseaseList() {
        String token = SharePref.getInstance().getTokenLogin();
        UIHelper.showProgress(this);
        String appVer = "";
        try {
            appVer = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().getInfoDiseaseList(token, androidId, appVer, currentPage, new CallBackInfoDiseaseList() {
            @Override
            public void onComplete(ArrayList<ObjDiseaseInfo> userInfoArrayList, int pageCurent, int totalPage) {
                if (userInfoArrayList.size() > 0) {
                    tvEmpty.setVisibility(View.GONE);
                    listDiseaseInfo.setVisibility(View.VISIBLE);
                    mArrayDiseaseInfo.addAll(userInfoArrayList);
                    mAdapterDiseaseInfo.notifyDataSetChanged();
                } else {
                    tvEmpty.setVisibility(View.VISIBLE);
                    listDiseaseInfo.setVisibility(View.GONE);
                }
                checkSetupLoadmore = true;
                lastPage = totalPage;
                currentPage = pageCurent;
                UIHelper.hideProgress();
            }

            @Override
            public void onError(String errorMsg) {
                UIHelper.hideProgress();
            }
        });
    }

    public void setupLoadMore() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) listDiseaseInfo.getLayoutManager();
        listDiseaseInfo.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCOunt = linearLayoutManager.getItemCount();
                int lastVissibleItemPOs = linearLayoutManager.findLastVisibleItemPosition();
                if (checkPullToRefresh || totalItemCOunt == 0) {
                    checkPullToRefresh = false;
                } else if (totalItemCOunt <= (lastVissibleItemPOs + vissibleThreshold)) {
                    if (currentPage < lastPage) {
                        currentPage++;
                        getInfoDiseaseList();
                    }
                }
            }
        });
    }

    public void setPullToRefresh() {
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mArrayDiseaseInfo.clear();
                checkPullToRefresh = true;
                setupList();
                handlerRefresh.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (pullToRefresh.isRefreshing()) {
                            pullToRefresh.setRefreshing(false);
                        }
                    }
                }, 1000);
            }
        });
    }

    public void setViewID() {
        listDiseaseInfo = (RecyclerView) findViewById(R.id.listDiseaseInfo);
        tvEmpty = (TextView) findViewById(R.id.tvEmpty);
        pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullToRefresh);
    }

    @Override
    public void onItemClick(View view, int position) {
        if (!isRunning) {
            isRunning = true;
            UIHelper.showProgress(this);
            ObjDiseaseInfo objDiseaseInfo = mArrayDiseaseInfo.get(position);
            openWebview(objDiseaseInfo.link, getString(R.string.thong_tin_ve_benh));
        }
        handlerRefresh.postDelayed(new Runnable() {
            @Override
            public void run() {
                isRunning = false;
            }
        }, 1000);
    }
}
