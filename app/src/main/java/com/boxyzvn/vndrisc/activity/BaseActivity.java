package com.boxyzvn.vndrisc.activity;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.dialog.MessageBox;
import com.boxyzvn.vndrisc.model.ObjNhacNho;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.AlarmService;
import com.boxyzvn.vndrisc.utils.DateUtils;
import com.boxyzvn.vndrisc.utils.LocalNotificationReceiver;
import com.boxyzvn.vndrisc.utils.MyApplication;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static android.app.NotificationManager.IMPORTANCE_DEFAULT;
import static com.boxyzvn.vndrisc.utils.LocalNotificationReceiver.NOTIFICATION_MESSAGE;
import static com.boxyzvn.vndrisc.utils.LocalNotificationReceiver.NOTIFICATION_TIME;


public class BaseActivity extends AppCompatActivity {
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    public static final float APP_BASE_WIDTH = 960f;
    public static final String REQUEST_URL = "request_url";
    public static final String REQUEST_TITLE = "request_title";
    protected int displayWidth;
    protected int displayHeight;
    public float scale;
    //layout id
    protected int mLayoutId;
    protected MenuView menuView;
    //drawer layout
    protected DrawerLayout mDrawLayout;
    //mainlayout
    protected ViewGroup mainLayout;
    private boolean mAlreadyStartedService = false;

    /**
     * Manage worker-threads on single queue for main functions (serializing works)
     */
    private volatile ExecutorService mExecutor = null;
    /**
     * Used to manage the wake-lock.
     */
    private PowerManager.WakeLock mWakeLock;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_menu);
        ViewStub viewStub = (ViewStub) findViewById(R.id.viewStub);
        viewStub.setLayoutResource(mLayoutId);
        mainLayout = (ViewGroup) viewStub.inflate();
        Window window = this.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        Point size = new Point();
        final WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        final Display disp = wm.getDefaultDisplay();
        SupportDisplay.getSize(disp, size);
        displayWidth = size.x;
        displayHeight = size.y;
        menuView = (MenuView) findViewById(R.id.menu_view);
        mDrawLayout = (DrawerLayout) findViewById(R.id.drawerLayoutMain);
        mDrawLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);


        //横向きと縦向きでscaleの値を変更
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // 横向きの場合
            scale = displayHeight / APP_BASE_WIDTH;

        }

        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            // 縦向きの場合
            scale = displayWidth / APP_BASE_WIDTH;
        }
    }

    public void setTitleTwoLine(String header, String title) {
        TextView mHeaderTitle = (TextView) findViewById(R.id.header_title);
        TextView mHeaderContent = (TextView) findViewById(R.id.header_content);
        if (title.isEmpty()) {
            mHeaderContent.setVisibility(View.GONE);
        } else {
            mHeaderContent.setText(title);
        }
        mHeaderTitle.setText(header);
        ImageView mBackBtn = (ImageView) findViewById(R.id.btn_back);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }

    public void setTextTitle(String title, boolean isShowPlusIcon) {
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        ImageView ivBack = (ImageView) findViewById(R.id.ivBack);
        tvTitle.setText(title);
        ImageView ivAddLich = (ImageView) findViewById(R.id.ivAddLich);
        if (isShowPlusIcon) {
            ivAddLich.setVisibility(View.VISIBLE);
        } else {
            ivAddLich.setVisibility(View.GONE);
        }
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });
    }

    public void openWebview(String url, String title){
        Intent intent =  new Intent(this, WebviewActivity.class);
        intent.putExtra(REQUEST_URL, url);
        intent.putExtra(REQUEST_TITLE, title);
        startActivity(intent);
    }

    public void initializeFullHeader(String header, String content) {
        TextView mHeaderTitle = (TextView) findViewById(R.id.header_title);
        TextView mHeaderContent = (TextView) findViewById(R.id.header_content);
        mHeaderTitle.setText(header);
        mHeaderContent.setText(content);
        ImageView mBackBtn = (ImageView) findViewById(R.id.btn_back);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    /**
     * Return the current state of the permissions needed.
     */
    public boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionState2 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("alarm");
        LocalBroadcastManager.getInstance(this).registerReceiver(localReceiver, intentFilter);

    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(localReceiver);
    }

    /**
     * Start permissions requests.
     */
    public void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        if (shouldProvideRationale || shouldProvideRationale2) {

        } else {

            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the img_user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(BaseActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }


    public void cancleLocalNotification(int requestCode) {
        Intent intent = new Intent(this, LocalNotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, requestCode, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }


    // CheckInUtilsに判定を集約したため廃止。
//    /**
//     * set an alarm nextDay for cancle Tagcast
//     */
//    public void scheduleTurnOffTagcastService() {
//        Intent turnOffTagcastService = new Intent(this, LocalNotificationReceiver.class);
//        turnOffTagcastService.putExtra(TURN_OFF_TAGCAST, true);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, TAGCAST_SERVICE_CANCLE_REQUEST_CODE, turnOffTagcastService, PendingIntent.FLAG_CANCEL_CURRENT
//        );
//
//        long futureInMillis = SystemClock.elapsedRealtime() + DateUtil.getNextDateMilli(1, 0, 0, 0);
//        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
//    }

    /**
     * create notification
     *
     * @param message
     * @return
     */
    public Notification getNotification(String message, String notificationID) {
//        Intent intent = new Intent(this, HomeActivity.class);
//        intent.setAction(Intent.ACTION_MAIN);
//        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//        intent.putExtra("PUSH_NOTIFY_ID", notificationID);
//        if (message.equals(LocalNotificationReceiver.FOR_MY_CARD)) {
//            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.putExtra("SEND_TO", 7);
//        } else {
//
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        }
//        Bitmap iconLarge = BitmapFactory.decodeResource(this.getResources(),
//                R.drawable.icon_close);
//
//        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);
//        int iconId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
//        String appName = this.getString(R.string.app_name);
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "CHANNEL_VNDRISC")
//                .setSmallIcon(iconId)
//                //.setLargeIcon(iconLarge)
//                .setContentTitle(appName)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
//                .setContentText(message);
//        builder.setContentIntent(contentIntent);
//        builder.setAutoCancel(true);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//            builder.setColor(Color.BLACK);
//
//
//        Notification notification = builder.build();
//        // LED点灯のフラグを追加する
//        notification.flags |= Notification.FLAG_SHOW_LIGHTS;
//        //デフォルトの音と振動を指定
//        notification.defaults |= Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;

        final String CHANNEL_ID = "vndrisc.channelId";
        Intent notificationIntent = new Intent(this, HomeActivity.class);
        notificationIntent.putExtra("check_notifi", true);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(SplashActivity.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);

        Notification notification = builder
                .setContentText(message)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.icon_close)
                .setContentIntent(pendingIntent).build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID);
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    "Notification",
                    IMPORTANCE_DEFAULT
            );
            notificationManager.createNotificationChannel(channel);
        }
        return notification;
    }

    public void scheduleNotification(Notification notification, String message, String time, long delay, int requestCode,
                                     boolean lapLai) {
        Intent notificationIntent = new Intent(BaseActivity.this, LocalNotificationReceiver.class);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION, notification);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_MESSAGE, message);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_TIME, time);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_REQUEST_CODE, requestCode);
        notificationIntent.putExtra(LocalNotificationReceiver.NOTIFICATION_LAPLAI, lapLai);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(BaseActivity.this, requestCode, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long futureInMillis = Calendar.getInstance().getTimeInMillis() + delay;
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, futureInMillis, pendingIntent);

    }

    MessageBox messageBox;
    private BroadcastReceiver localReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case "alarm":
                    String msg = intent.getExtras().getString(NOTIFICATION_MESSAGE);
                    String time = intent.getExtras().getString(NOTIFICATION_TIME);
                    //String popUpID = intent.getExtras().getString("id");
                    fcmMassage(msg, time);
                    Log.e("HUNG loi show", msg);
                    //startServiceAlarm();
                    break;
                case "notification":
                    String msg1 = intent.getExtras().getString("mess");
                    String title1 = intent.getExtras().getString("title");
                    String popUpID1 = intent.getExtras().getString("id");
                    //fcmMassage(msg1, title1);
                    Log.e("HUNG loi show", msg1);
                    //setAlarm();
                    break;
            }
        }
    };

    public void setAlarm() {
        ArrayList<ObjNhacNho> mArrayNhacNho = new Gson().fromJson(SharePref.getInstance().getListLoiNhac(), new TypeToken<ArrayList<ObjNhacNho>>() {
        }.getType());
        for (int i = 0; i < mArrayNhacNho.size(); i++) {
            cancleLocalNotification(mArrayNhacNho.get(i).getCode());
            Date date = DateUtils.string2Date(mArrayNhacNho.get(i).getTime(), "HH:mm");
            Calendar calendar = Calendar.getInstance();
            String hour = mArrayNhacNho.get(i).getTime().substring(0, 2);
            String min = mArrayNhacNho.get(i).getTime().substring(3, 5);
            Log.e("HUNG loi nhac code", hour + min);
            int minute = Integer.parseInt(min);
            Log.e("HUNG loi nhac code min ", minute + "");
            int day[] = mArrayNhacNho.get(i).getDay();
            for (int j = 0; j <= 6; j++) {
                int dayOfWeek;
                if (j == 6) {
                    dayOfWeek = 1;

                } else {
                    dayOfWeek = j + 2;
                }
                long milli = DateUtils.getNextDateMilli(dayOfWeek, Integer.parseInt(hour), Integer.parseInt(min), 0);
                scheduleNotification(getNotification(mArrayNhacNho.get(i).getLoiNhac(), "CHANNEL"),
                        mArrayNhacNho.get(i).getLoiNhac(),
                        mArrayNhacNho.get(i).getTime(),
                        milli, mArrayNhacNho.get(i).getCode(),
                        true);
            }
        }
    }

    public void startServiceAlarm() {
        Intent intent2 = new Intent(this, AlarmService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent2);
        } else {
            startService(intent2);
        }
    }


    private void fcmMassage(final String msg, final String time) {
        messageBox = new MessageBox(BaseActivity.this, msg, time, "");
        messageBox.show();
    }

    private void fcm(String title, String mess) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(mess)

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    /**
     * get main executor.
     *
     * @return executor instance.
     */
    private synchronized ExecutorService getExecutor() {
        if (mExecutor == null) mExecutor = Executors.newSingleThreadExecutor();
        return mExecutor;
    }

    public boolean checkNetworkStatus() {
        if (!isNetworkEnable()) {
            //Toast.makeText(this, "Vui lòng kết nối internet", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public boolean isNetworkEnable() {
        final ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

}
