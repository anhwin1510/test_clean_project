package com.boxyzvn.vndrisc.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.boxyzvn.vndrisc.ProgressLoading;
import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.login.LoginActivity;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBack;
import com.boxyzvn.vndrisc.model.UserInfo;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.UIHelper;
import com.google.gson.Gson;

import static com.boxyzvn.vndrisc.activity.SanLocActivity.KHUYEN_NGHI;
import static com.boxyzvn.vndrisc.sharedpreferences.Constants.MAX_RETRY;

public class HomeActivity extends BaseActivity {
    private ProgressLoading progressLoading;
    private LinearLayout thongTinVeBenhLayout, layoutFoodInfo, layoutQLB, layoutNhacNho, layoutHuongDan;
    private ImageView imgMenu;
    private DrawerLayout mDrawLayout;
    private TextView tvHome, tvTermOfUse, tvPolicy, tvAppInfo, tvNhacNho, tvLogout, tvUserName;
    //mainlayout
    protected MenuView menuView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mLayoutId = (R.layout.activity_home_layout);
        super.onCreate(savedInstanceState);

        setViewID();
        thongTinVeBenhLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ThongTinVeBenhActivity.class));
            }
        });

        layoutQLB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, QuanLyBenhActivity.class));
            }
        });

        layoutNhacNho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, NhacNhoActivity.class));
            }
        });
        layoutHuongDan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, HuongDanActivity.class));
            }
        });
        //long milli = DateUtils.getNextDateMilli2(1, 0, 0, 10);
        //scheduleNotification(getNotification(LocalNotificationReceiver.AFTER_DL_NEXT_DAY, "2_1"),LocalNotificationReceiver.AFTER_DL_NEXT_DAY, milli, 101);

    }

    public void setViewID() {
        progressLoading = new ProgressLoading(this);
        thongTinVeBenhLayout = (LinearLayout) findViewById(R.id.layoutTTVB);
        layoutQLB = (LinearLayout) findViewById(R.id.layoutQLB);
        layoutHuongDan = (LinearLayout) findViewById(R.id.layoutHuongDan);
        layoutNhacNho = (LinearLayout) findViewById(R.id.layoutNhacNho);
        imgMenu = findViewById(R.id.imgMenu);
        menuView = (MenuView) findViewById(R.id.menu_view);
        mDrawLayout = (DrawerLayout) findViewById(R.id.drawerLayoutMain);
        tvUserName = findViewById(R.id.txt_UserMenu);
        tvHome = findViewById(R.id.tvHome);
        tvTermOfUse = findViewById(R.id.tvTermOfUse);
        tvAppInfo = findViewById(R.id.tvAppInfo);
        tvPolicy = findViewById(R.id.tvPolicy);
        tvLogout = findViewById(R.id.tvLogout);
        tvHome.setOnClickListener(mOncOnClickListener);
        tvTermOfUse.setOnClickListener(mOncOnClickListener);
        tvAppInfo.setOnClickListener(mOncOnClickListener);
        tvPolicy.setOnClickListener(mOncOnClickListener);
        tvLogout.setOnClickListener(mOncOnClickListener);
        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mDrawLayout.openDrawer(GravityCompat.START);
            }
        });
        boolean isSendChuaChanDoanSuccess = SharePref.getInstance().isSendChuaChanDoanSuccess();
        boolean isSendDaChanDoanSuccess = SharePref.getInstance().isSendDaChanDoanSuccess();

        if (checkNetworkStatus()) {
            getUserInfo();
            if (!isSendChuaChanDoanSuccess) {
                callAPISendChuaChanDoan();
            }
            if (!isSendDaChanDoanSuccess) {
                callAPISendDaChanDoan();
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean isSendChuaChanDoanSuccess = SharePref.getInstance().isSendChuaChanDoanSuccess();
        boolean isSendDaChanDoanSuccess = SharePref.getInstance().isSendDaChanDoanSuccess();
        if (checkNetworkStatus()) {
            if (!isSendChuaChanDoanSuccess) {
                callAPISendChuaChanDoan();
            }
            if (!isSendDaChanDoanSuccess) {
                callAPISendDaChanDoan();

            }

        }
    }

    public void getUserInfo() {
        String token = SharePref.getInstance().getTokenLogin();
        UIHelper.showProgress(this);
        String appVer = "";
        try {
            appVer = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().getUserInfo(token, androidId, appVer, new CallBack() {
            @Override
            public void onComplete() {
                UIHelper.hideProgress();
                UserInfo userInfo = SharePref.getInstance().getInfoLogin();
                tvUserName.setText(userInfo.getName());
            }

            @Override
            public void onError(String errorMsg) {
                UIHelper.hideProgress();
                //Toast.makeText(HomeActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void callAPISendChuaChanDoan() {
        String ketQua = SharePref.getInstance().getJsonKetQuaChuaChanDoan();
        int totalScore = SharePref.getInstance().getPoint();
        Log.e("HomeActivity", "callAPISendChuaChanDoan " + ketQua);
        Log.e("HomeActivity", "callAPISendChuaChanDoan point " + totalScore);
        String token = SharePref.getInstance().getTokenLogin();
        UIHelper.showProgress(this);
        String appVer = "";
        try {
            appVer = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().sendChuaChanDoan(token, androidId, appVer, ketQua, totalScore, new CallBack() {
            @Override
            public void onComplete() {
                SharePref.getInstance().setSendChuaChanDoanSuccess(true);
                SharePref.getInstance().saveJsonKetQuaDaChanDoan("");
                UIHelper.hideProgress();


            }

            @Override
            public void onError(String errorMsg) {
                Toast.makeText(HomeActivity.this, errorMsg, Toast.LENGTH_LONG).show();
                UIHelper.hideProgress();
            }
        });
    }

    private void callAPISendDaChanDoan() {
        String ketQua = SharePref.getInstance().getJsonKetQuaDaChanDoan();
        Log.e("HomeActivity", "callAPISendDaChanDoan " + ketQua);
        String token = SharePref.getInstance().getTokenLogin();
        UIHelper.showProgress(this);
        String appVer = "";
        try {
            appVer = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().sendDaChanDoan(token, androidId, appVer, ketQua, new CallBack() {
            @Override
            public void onComplete() {
                SharePref.getInstance().setSendDaChanDoanSuccess(true);
                SharePref.getInstance().saveJsonKetQuaChuaChanDoan("");
                UIHelper.hideProgress();
            }

            @Override
            public void onError(String errorMsg) {
                Toast.makeText(HomeActivity.this, errorMsg, Toast.LENGTH_LONG).show();
                UIHelper.hideProgress();
            }
        });
    }

    private View.OnClickListener mOncOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mDrawLayout.closeDrawer((GravityCompat.START));
            switch (v.getId()) {
                case R.id.tvHome:
                    break;
                case R.id.tvTermOfUse:
                    openWebview("https://termofuse.boxyzvn.com", "");
                    break;
                case R.id.tvAppInfo:
                    startActivity(new Intent(HomeActivity.this, AppInfoActivity.class));
                    break;
                case R.id.tvPolicy:
                    openWebview("https://privacy.boxyzvn.com", "");
                    break;
                case R.id.tvLogout:
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    SharePref.getInstance().setLoginStatus(false);
                    SharePref.getInstance().setCTVStatus(false);
                    break;
                default:

                    break;
            }
        }
    };


}
