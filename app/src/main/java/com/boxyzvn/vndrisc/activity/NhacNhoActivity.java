package com.boxyzvn.vndrisc.activity;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.adapter.LoiNhacAdapter;
import com.boxyzvn.vndrisc.model.ObjNhacNho;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.DateUtils;
import com.boxyzvn.vndrisc.utils.alarm.MyAlarmManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class NhacNhoActivity extends BaseActivity implements LoiNhacAdapter.ItemClickListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    public static final int REQUEST_EDIT_NHAC_NHO = 99;
    public static final String OBJ_NHAC_NHO = "ObjNhacNho";
    public static final String POSITION_CLICKED = "POSITION_CLICKED";
    private RecyclerView rcNhacNho;
    private LoiNhacAdapter mAdapterNhacNho;
    private ArrayList<ObjNhacNho> mArrayNhacNho;
    private TextView tvNoContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_loi_nhac;
        super.onCreate(savedInstanceState);
        setTextTitle("Nhắc nhở", true);
        tvNoContent = (TextView) findViewById(R.id.tvNoContent);
        ImageView ivBack = (ImageView) findViewById(R.id.ivBack);
        ImageView ivAddLich = (ImageView) findViewById(R.id.ivAddLich);
        ivAddLich.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NhacNhoActivity.this, ChonGioActivity.class);
                intent.setAction("ADD");
                startActivityForResult(intent, REQUEST_EDIT_NHAC_NHO);
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(this, ChonGioActivity.class);
        intent.putExtra(OBJ_NHAC_NHO, mArrayNhacNho.get(position));
        intent.putExtra(POSITION_CLICKED, position);
        startActivityForResult(intent, REQUEST_EDIT_NHAC_NHO);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!SharePref.getInstance().getListLoiNhac().isEmpty()) {
            mArrayNhacNho = new Gson().fromJson(SharePref.getInstance().getListLoiNhac(), new TypeToken<ArrayList<ObjNhacNho>>() {
            }.getType());
            Log.e("HUNG loi nhac", SharePref.getInstance().getListLoiNhac());
            rcNhacNho = findViewById(R.id.rcLoiNhac);
            mAdapterNhacNho = new LoiNhacAdapter(this, mArrayNhacNho);
            rcNhacNho.setAdapter(mAdapterNhacNho);
            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rcNhacNho);
            mAdapterNhacNho.setClickListener(this);
            rcNhacNho.setLayoutManager(new LinearLayoutManager(this));
            refreshAlarm();

        }
    }

    public void refreshAlarm() {
        MyAlarmManager myAlarmManager;
        for (int i = 0; i < mArrayNhacNho.size(); i++) {
            if (mArrayNhacNho.get(i).isEnable()) {
                myAlarmManager = new MyAlarmManager(this);
                Date date = DateUtils.string2Date(mArrayNhacNho.get(i).getTime(), "HH:mm");
                Calendar calendar = Calendar.getInstance();
                String hour = mArrayNhacNho.get(i).getTime().substring(0, 2);
                String min = mArrayNhacNho.get(i).getTime().substring(3, 5);

                int minute = Integer.parseInt(min);
                Log.e("HUNG loi nhac code min ", minute + "");
                int day[] = mArrayNhacNho.get(i).getDay();

                //Biến kiểm tra chọn ngày lặp lại
                boolean isRepeat = false;
                String message = mArrayNhacNho.get(i).getLoiNhac() +
                        ". \n" + mArrayNhacNho.get(i).getLoiNhacDefault();
                for (int j = 0; j <= 6; j++) {
                    int dayOfWeek = -1;
                    if (j == 6) {
                        dayOfWeek = 1;
                    } else {
                        dayOfWeek = j + 2;
                    }
                    cancleLocalNotification(Integer.parseInt(mArrayNhacNho.get(i).getCode() + "" + dayOfWeek));
                    if (day[j] == 1) {
                        isRepeat = true;
                        Log.e("HUNG loi nhac code", mArrayNhacNho.get(i).getCode() + "" + dayOfWeek);
                        Log.e("HUNG time", mArrayNhacNho.get(i).getTime());
                        long milli = DateUtils.getNextDateMilli(dayOfWeek, Integer.parseInt(hour), Integer.parseInt(min), 0);
                        scheduleNotification(getNotification(message, "CHANNEL"),
                                message,
                                mArrayNhacNho.get(i).getTime(),
                                milli,
                                Integer.parseInt(mArrayNhacNho.get(i).getCode() + "" + dayOfWeek),
                                true);
//                        myAlarmManager = new MyAlarmManager(this);
//                        myAlarmManager.clearAlarm(mArrayNhacNho.get(i).getCode() + dayOfWeek);
//                        long milli = DateUtils.getNextDateMilli(dayOfWeek, Integer.parseInt(hour), Integer.parseInt(min), 0);
//                        myAlarmManager.startAlarm(Integer.parseInt(mArrayNhacNho.get(i).getCode()+""+ dayOfWeek), milli, mArrayNhacNho.get(i).getLoiNhac());

                    }

                }
                if (!isRepeat) {
                    int currentDayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
                    long milli = DateUtils.getNextDateMilli(currentDayOfWeek, Integer.parseInt(hour), Integer.parseInt(min), 0);
                    scheduleNotification(getNotification(message, "CHANNEL"),
                            message,
                            mArrayNhacNho.get(i).getTime(),
                            milli,
                            Integer.parseInt(mArrayNhacNho.get(i).getCode() + "" + currentDayOfWeek),
                            false);
                }
            } else {
                cancelNoti(i);
            }
        }

        showTextNoContent();
    }

    private void showTextNoContent() {
        if (mArrayNhacNho.size() == 0) {
            tvNoContent.setVisibility(View.VISIBLE);
        } else {
            tvNoContent.setVisibility(View.GONE);
        }
    }

    private void cancelNoti(int i) {
        int day[] = mArrayNhacNho.get(i).getDay();
        for (int j = 0; j <= 6; j++) {
            int dayOfWeek = -1;
            if (day[j] == 1) {
                if (j == 6) {
                    dayOfWeek = 1;
                } else {
                    dayOfWeek = j + 2;
                }
                Log.e("HUNG cancel code", mArrayNhacNho.get(i).getCode() + "" + dayOfWeek);
                cancleLocalNotification(Integer.parseInt(mArrayNhacNho.get(i).getCode() + "" + dayOfWeek));
            }
        }
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof LoiNhacAdapter.NhacNhoViewHolder) {
            String name = mArrayNhacNho.get(viewHolder.getAdapterPosition()).getLoiNhac();

            final ObjNhacNho deletedItem = mArrayNhacNho.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
            cancelNoti(position);
            mArrayNhacNho.remove(mArrayNhacNho.get(position));
            SharePref.getInstance().setListLoiNhac(new Gson().toJson(mArrayNhacNho));
            mAdapterNhacNho.notifyDataSetChanged();
            showTextNoContent();

//            Snackbar snackbar = Snackbar.make(coordinatorLayout, name + " removed from cart!", Snackbar.LENGTH_LONG);
//            snackbar.setAction("UNDO", new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    adapter.restoreItem(deletedItem, deletedIndex);
//                }
//            });
//            snackbar.setActionTextColor(Color.YELLOW);
//            snackbar.show();
        }
    }

//    private void setScheduleAlarm(ObjNhacNho objNhacNho) {
//        Calendar calendar = Calendar.getInstance();
//        Date date = calendar.getTime();
//        int days[] = objNhacNho.getDay();
//        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
//        //convert sang position trong array
//        if (dayOfWeek == 1) {
//            dayOfWeek = 6;
//        } else
//            dayOfWeek = dayOfWeek - 2;
//        dayOfWeek =5;
//        for (int i = 0; i < 7; i++) {
//            if()
//        }
//        int dayNextAlarm = objNhacNho.getDay()[3];
//    }
}
