package com.boxyzvn.vndrisc.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.boxyzvn.vndrisc.R;

public class BookmarkActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mLayoutId = (R.layout.activity_bookmark);
        super.onCreate(savedInstanceState);
        setTitleTwoLine("Bookmarks", "");
    }
}
