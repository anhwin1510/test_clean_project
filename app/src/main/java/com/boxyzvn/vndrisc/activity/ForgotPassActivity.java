package com.boxyzvn.vndrisc.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.login.LoginActivity;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBackCategorySecurityQuestion;
import com.boxyzvn.vndrisc.api.callback.CallBackCheck;
import com.boxyzvn.vndrisc.fragment.forgot.ForgotPassFragment;
import com.boxyzvn.vndrisc.fragment.register.RegisterEmailFragment;
import com.boxyzvn.vndrisc.model.CategorySecurityQuestion;
import com.boxyzvn.vndrisc.model.UserInfo;

import java.util.ArrayList;

public class ForgotPassActivity extends BaseActivity {

    public static final String TAG = "register";
    FragmentManager fm;
    private RelativeLayout popupError;
    private Button btnRetry;
    private int securityCode, viewLogin;
    private String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_forgot_pass;
        super.onCreate(savedInstanceState);
        initializeFullHeader("Quên mật khẩu", "Điền vào những thông tin sau");

        popupError = (RelativeLayout) findViewById(R.id.popupErrorSecurity);
        btnRetry = (Button) findViewById(R.id.btnRetry);
        ImageView mBackBtn = (ImageView) findViewById(R.id.btn_back);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ForgotPassActivity.this, LoginActivity.class));
                finish();
            }
        });
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidePopupError();
            }
        });

        Bundle bundle = getIntent().getBundleExtra("data");
        if (bundle != null) {
            viewLogin = bundle.getInt("view");
            setViewLogin(viewLogin);
        }

        fm = getSupportFragmentManager();
        FragmentTransaction ft_add = fm.beginTransaction();
        ft_add.add(R.id.frame_layout, ForgotPassFragment.newInstance());
        ft_add.commit();

    }

    public void showPopupError() {
        popupError.setVisibility(View.VISIBLE);
    }

    public void hidePopupError() {
        popupError.setVisibility(View.GONE);
    }

    public int getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(int securityCode) {
        this.securityCode = securityCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getViewLogin() {
        return viewLogin;
    }

    public void setViewLogin(int viewLogin) {
        this.viewLogin = viewLogin;
    }
}
