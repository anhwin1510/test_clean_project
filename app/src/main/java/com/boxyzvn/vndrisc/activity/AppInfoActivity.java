package com.boxyzvn.vndrisc.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import com.boxyzvn.vndrisc.R;

public class AppInfoActivity extends BaseActivity {
    TextView tvVersion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_app_info;
        super.onCreate(savedInstanceState);
        setTextTitle("Thông tin về app", false);
        tvVersion = findViewById(R.id.tvBuild);
        try {
            int versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            tvVersion.setText("Phiên bản "+ versionName + " build " + versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
