package com.boxyzvn.vndrisc.activity.login;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.boxyzvn.vndrisc.ProgressLoading;
import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.activity.BaseActivity;
import com.boxyzvn.vndrisc.activity.ForgotPassActivity;
import com.boxyzvn.vndrisc.activity.HomeActivity;
import com.boxyzvn.vndrisc.activity.register.user.RegisterActivity;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBackLogin;
import com.boxyzvn.vndrisc.model.ObjMap;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.GUIUtils;
import com.boxyzvn.vndrisc.utils.UIHelper;
import com.boxyzvn.vndrisc.utils.Validator;

import java.util.ArrayList;

public class LoginActivity extends BaseActivity {

    public ProgressLoading progressLoading;
    public Button btnLogin, btnRegister;
    public TextView tvChangeMode, tvSignup, tvQuen;
    private EditText edtPass, edtPhoneNumber;
    private CheckBox cbSaveLogin;
    ArrayList<ObjMap> arrayList = new ArrayList<>();
    private ImageView imgHidePass;
    private boolean checkPass = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_login;
        super.onCreate(savedInstanceState);
        setViewID();
        setViewClick();

        Window window = this.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.gray));
    }

    public void setViewID() {
        progressLoading = new ProgressLoading(this);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvChangeMode = (TextView) findViewById(R.id.tvForgot);
        tvQuen = (TextView) findViewById(R.id.tvQuen);
        edtPass = findViewById(R.id.edtPassword);
        edtPhoneNumber = findViewById(R.id.edtUsername);
        btnRegister = findViewById(R.id.btnRegister);
        cbSaveLogin = findViewById(R.id.cbSaveLogin);
        imgHidePass = (ImageView) findViewById(R.id.imgHidePass);
        cbSaveLogin.setButtonTintList(ColorStateList.valueOf(getColor(R.color.colorAccent)));
    }

    private Validator validatePhone() {
        return new Validator()
                .validatePhone(edtPhoneNumber.getText().toString());

    }

    private Validator validatePass() {
        return new Validator()
                .validatePassword(edtPass.getText().toString());

    }

    public void setViewClick() {
        btnLogin.setOnClickListener(view -> {
            Validator validatorPhone = validatePhone();
            Validator validatorPass = validatePass();
            if (edtPass.getText().toString().isEmpty() || edtPhoneNumber.getText().toString().isEmpty()) {
                GUIUtils.showAlertDialog(LoginActivity.this, getString(R.string.validate_phone_and_password_empty));
            } else if (!validatorPhone.isValid()) {
                GUIUtils.showAlertDialog(LoginActivity.this, getString(validatorPhone.getStringError()));
            } else {
                if (!validatorPass.isValid()) {
                    GUIUtils.showAlertDialog(LoginActivity.this, getString(R.string.validate_password_error));
                } else {
                    if (checkNetworkStatus()) {
                        btnLogin.setEnabled(false);
                        callAPI(edtPhoneNumber.getText().toString(), edtPass.getText().toString());
                    } else {
                        GUIUtils.showAlertDialog(LoginActivity.this, getString(R.string.network_error));
                    }

                    //
//                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    if (cbSaveLogin.isChecked()) {
//                        SharePref.getInstance().setLoginStatus(true);
//                    }
//                    startActivity(intent);
//                    finish();
                }
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }

        });
        tvChangeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, LoginCTVActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        imgHidePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkStatusEdtPass(edtPass, imgHidePass);
            }
        });
        tvQuen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPassActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("view", 0);
                intent.putExtra("data", bundle);
                startActivity(intent);
            }
        });
    }

    private void checkStatusEdtPass(EditText edtPass, ImageView imgHidePass) {
        if (checkPass) {
            edtPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            imgHidePass.setImageResource(R.drawable.login_icon_eye_on_a);
            checkPass = false;
        } else {
            edtPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
            imgHidePass.setImageResource(R.drawable.login_icon_eye_off_a);
            checkPass = true;
        }
    }

    public void callAPI(String phone, String password) {
        UIHelper.showProgress(this);
        String appVer = "";
        try {
            appVer = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().login(phone, password, androidId, appVer, new CallBackLogin() {
            @Override
            public void onComplete(String token) {
                SharePref.getInstance().setTokenLogin(token);
                if (cbSaveLogin.isChecked()) {
                    SharePref.getInstance().setLoginStatus(true);
                }
                UIHelper.hideProgress();
                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                finish();
            }

            @Override
            public void onError(String errorMsg) {
                UIHelper.hideProgress();
                btnLogin.setEnabled(true);
                GUIUtils.showAlertDialog(LoginActivity.this, getString(R.string.login_error));
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (progressLoading.isShow()) {
            progressLoading.hide();
        }
    }
}
