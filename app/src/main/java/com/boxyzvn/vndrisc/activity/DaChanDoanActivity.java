package com.boxyzvn.vndrisc.activity;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boxyzvn.vndrisc.NonSwipeableViewPager;
import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.api.ApiServiceManager;
import com.boxyzvn.vndrisc.api.callback.CallBackGetJson;
import com.boxyzvn.vndrisc.model.ObjQuestion;
import com.boxyzvn.vndrisc.model.ObjQuestionOption;
import com.boxyzvn.vndrisc.utils.UIHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;

import static com.boxyzvn.vndrisc.activity.SanLocActivity.DA_CHAN_DOAN;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.HEIGH_VIEW;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.KET_QUA;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.KET_QUA_CHECK_BOX;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.KHUYEN_NGHI;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.QUESTION_LIST;
import static com.boxyzvn.vndrisc.utils.StringUtils.loadJSONFromAsset;

public class DaChanDoanActivity extends BaseActivity {
    private NonSwipeableViewPager viewPager;
    private DaChanDoanActivity.MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private ArrayList<ObjQuestion> listQuestion = new ArrayList<>();
    private ArrayList<String> listAnswer = new ArrayList<>();
    private ArrayList<String> listAnswerCheckBox = new ArrayList<>();
    private View viewProcess;
    private String answer;
    private Calendar selectedDate;
    private String questionData;
    JSONArray khuyenNghi = null;
    private Button btnForget;
    private EditText edtText;
    private int pageCurrent = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_da_chan_doan;
        super.onCreate(savedInstanceState);
        setTitleTwoLine("Đã Được Chẩn Đoán", "Trả lời các câu hỏi sau");
        initView();
        if (checkNetworkStatus()) {
            callAPI();
        } else {
            try {
                JSONObject data = new JSONObject(loadJSONFromAsset(this, "dachandoan.json"));
                setUpJsonData(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        View.OnClickListener nextScreen = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchHomeScreen();
            }
        };

    }

    private void initView() {
        viewPager = (NonSwipeableViewPager) findViewById(R.id.pagerWalkthrough);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        viewProcess = (View) findViewById(R.id.viewProcess);

    }

    private void callAPI() {
        String token = com.boxyzvn.vndrisc.sharedpreferences.SharePref.getInstance().getTokenLogin();
        String appVer = "";
        try {
            appVer = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ApiServiceManager.getInstance().getJSONDaChanDoan(token, androidId, appVer, new CallBackGetJson() {
            @Override
            public void onComplete(JSONObject jsonObject) {
                setUpJsonData(jsonObject);
            }

            @Override
            public void onError(String errorMsg) {

            }
        });

    }

    private void setUpJsonData(JSONObject jsonObject) {
        JSONObject data = jsonObject;
        JSONArray dataQuestion = null;

        try {
            dataQuestion = data.getJSONArray("questions");
            khuyenNghi = data.getJSONArray("recommendations");
        } catch (JSONException e) {
            Log.d("HUNG ", e.toString());
        }
        Type type = new TypeToken<ArrayList<ObjQuestion>>() {
        }.getType();
        questionData = dataQuestion.toString();
        ArrayList<ObjQuestion> listShakeData = new Gson().fromJson(questionData, type);
        listQuestion.addAll(listShakeData);
        float percent = (float) 1 / (float) listQuestion.size();
        Log.e("HUNGG", percent + "");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, 10, (float) percent);

        viewProcess.setLayoutParams(params);
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        viewPager.setOffscreenPageLimit(10);
    }

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (viewPager.getCurrentItem() > 0) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
            } else {
                finish();
            }
            return true;
        } else {
            return false;
        }
    }*/

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        startActivity(new Intent(DaChanDoanActivity.this, KetQuaActivity.class)
                .putExtra(KET_QUA, listAnswer)
                .putExtra(DA_CHAN_DOAN, DA_CHAN_DOAN)
                .putExtra(KHUYEN_NGHI, khuyenNghi.toString())
                .putExtra(KET_QUA_CHECK_BOX, listAnswerCheckBox)
                .putExtra(QUESTION_LIST, questionData));
        finish();
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;
        Button btnNext;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.layout_question, container, false);
            container.addView(view);
            String titleQuestion = listQuestion.get(position).getTitle();
            String content = listQuestion.get(position).getContent();

            btnNext = view.findViewById(R.id.btnNext);
            btnNext.setVisibility(View.GONE);

            String text = "<font color=#068855>" + titleQuestion + ": " + "</font> <font color=#707070>" + content + "</font>";

            TextView title = view.findViewById(R.id.tvTitleQuestion);
            title.setText(Html.fromHtml(text));
            LinearLayout linearLayoutRoot = view.findViewById(R.id.layoutContentQuestion);
            ArrayList<ObjQuestionOption> listOption = listQuestion.get(position).getOptions();
            for (int i = 0; i < listOption.size(); i++) {
                if (listOption.get(i).getType().equals("button")) {
                    createButton(position, linearLayoutRoot, listOption, i, btnNext);
                }
                if (listOption.get(i).getType().equals("input")) {
                    createEdittextView(linearLayoutRoot, listOption, i, btnNext, position);
                }

                if (listOption.get(i).getType().equals("checkbox")) {
                    createCheckbox(linearLayoutRoot, listOption, i, btnNext);
                    if (i == listOption.size() - 1) {
                        //listAnswer.add("textbox");
                    }
                }
                if (listOption.get(i).getType().equals("date")) {
                    createTextView(linearLayoutRoot, listOption, i, btnNext);
                }

            }

            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int current = getItem(+1);
                    float percent = (float) getItem(+2) / (float) listQuestion.size();
                    Log.e("HUNGG", percent + "");
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, 10, (float) percent);
                    viewProcess.setLayoutParams(params);
                    viewPager.setCurrentItem(current);
                    UIHelper.hideKeyboard(DaChanDoanActivity.this);
                    if (current < listQuestion.size()) {
                        // move to next screen
                        viewPager.setCurrentItem(current);
                        listAnswer.add(answer);
                    } else {
                        listAnswer.add(answer);
                        launchHomeScreen();
                    }
                }
            });

            return view;
        }

        private void createButton(int position, LinearLayout linearLayoutRoot,
                                  ArrayList<ObjQuestionOption> listOption, int i, Button btnNext) {
            Button button = new Button(getApplicationContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    Math.round(HEIGH_VIEW * scale)
            );
            params.gravity = Gravity.CENTER_HORIZONTAL;

            params.setMargins(Math.round(50 * scale), 0, Math.round(50 * scale), Math.round(50 * scale));
            button.setLayoutParams(params);
            button.setPadding(0, 0, 0, 0);
            button.setId(Integer.parseInt(position + "" + i));
            button.setBackgroundResource(R.drawable.main_button);
            button.setTextColor(getResources().getColorStateList(R.color.color_text_change));
            //button.setTextColor(R.drawable.text_white_to_green);
            button.setText(listOption.get(i).getName());
            int finalI = i;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnNext.setVisibility(View.VISIBLE);
                    resetButton(listOption, position, finalI);
                    int positionEditext = i - 1;
                    try {
                        //lấy edittext với ID trong cùng option
                        EditText editText = findViewById(Integer.parseInt(position + "" + positionEditext));
                        if (editText != null) {
                            editText.setText("");
                        }
                    } catch (Exception e) {
                    }
                }
            });
            linearLayoutRoot.addView(button);
        }

        private void createEdittextView(LinearLayout linearLayoutRoot, ArrayList<ObjQuestionOption> listOption, int i,
                                        Button btnNext, int position) {
            EditText edtInput = new EditText(getApplicationContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    Math.round(130 * scale)
            );

            params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
            params.setMargins(Math.round(50 * scale), 0, Math.round(50 * scale), Math.round(50 * scale));
            edtInput.setLayoutParams(params);
            edtInput.setId(Integer.parseInt(position + "" + i));
            edtInput.setPadding(Math.round(50 * scale), 0, Math.round(50 * scale), 0);
            edtInput.setBackgroundResource(R.drawable.custom_edit_text);
            edtInput.setTextColor(getResources().getColorStateList(R.color.color_text_change));
            edtInput.setInputType(InputType.TYPE_CLASS_NUMBER);
            edtInput.setHint(listOption.get(i).getName());
            edtInput.setGravity(Gravity.CENTER_VERTICAL);
            edtInput.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE && !edtInput.getText().toString().equals("")) {
                        //do here your stuff f
                        UIHelper.hideKeyboard(DaChanDoanActivity.this);
                        return true;
                    }
                    return false;
                }
            });
            edtInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if (!s.toString().equals("")) {
                        btnNext.setVisibility(View.VISIBLE);
                        //lấy button với ID trong cùng option
                        int positionButton = i + 1;
                        Button button = findViewById(Integer.parseInt(position + "" + positionButton));
                        if (button != null) {
                            button.setBackgroundResource(R.drawable.main_button);
                            button.setTextColor(getResources().getColorStateList(R.color.color_text_change));
                        }
                        answer = s.toString();
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            linearLayoutRoot.addView(edtInput);
        }

        private void createTextView(LinearLayout linearLayoutRoot, ArrayList<ObjQuestionOption> listOption, int i,
                                    Button btnNext) {
            TextView textView = new TextView(getApplicationContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    Math.round(130 * scale)
            );

            params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
            params.setMargins(Math.round(50 * scale), 0, Math.round(50 * scale), Math.round(50 * scale));
            textView.setLayoutParams(params);
            textView.setPadding(Math.round(50 * scale), 0, Math.round(50 * scale), 0);
            textView.setBackgroundResource(R.drawable.custom_edit_text);
            textView.setTextColor(getResources().getColorStateList(R.color.color_text_change));
            textView.setHint(listOption.get(i).getName());
            textView.setGravity(Gravity.CENTER_VERTICAL);
            textView.setInputType(InputType.TYPE_CLASS_TEXT);

            final DatePickerDialog.OnDateSetListener callbackSetDate = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    selectedDate.set(Calendar.YEAR, year);
                    selectedDate.set(Calendar.MONTH, monthOfYear);
                    selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    monthOfYear++;
                    textView.setText("" + year + "-" + monthOfYear + "-" + dayOfMonth);
                    answer = textView.getText().toString();
                    btnNext.setVisibility(View.VISIBLE);
                }

            };

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    selectedDate = Calendar.getInstance();
                    DatePickerDialog datePickerDialog = new DatePickerDialog(DaChanDoanActivity.this,
                            android.R.style.Theme_Holo_Light_Dialog_NoActionBar, callbackSetDate,
                            selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH),
                            selectedDate.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    datePickerDialog.show();
                }
            });


            linearLayoutRoot.addView(textView);
        }


        private void resetButton(ArrayList<ObjQuestionOption> listOption, int position, int currentButton) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < listOption.size(); i++) {
                        if (listOption.get(i).getType().equals("button")) {
                            int id = position;
                            Button button = viewPager.findViewById(Integer.parseInt(id + "" + i));
                            //button.setId(R.id.btnLv1);
                            if (currentButton != i) {
                                button.setBackgroundResource(R.drawable.main_button);
                                button.setTextColor(getResources().getColorStateList(R.color.color_text_change));
                            } else {
                                answer = listOption.get(i).getName();
                                button.setBackgroundResource(R.drawable.button_clicked);
                                button.setTextColor(getColor(R.color.white));
                            }
                            try {
                                EditText editText = viewPager.findViewById(Integer.parseInt(id + "" + i));
                                if (editText != null) {
                                    editText.setText("");
                                    editText.setHint(listOption.get(i).getName());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
            });

        }

        @Override
        public int getCount() {
            return listQuestion.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    private void createCheckbox(LinearLayout linearLayoutRoot, ArrayList<ObjQuestionOption> listOption, int i
            , Button btnNext) {
        CheckedTextView checkBox = new CheckedTextView(getApplicationContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                Math.round(130 * scale)
        );
        checkBox.setCheckMarkDrawable(R.drawable.no);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);
                    checkBox.setCheckMarkDrawable(R.drawable.no);
                    listAnswerCheckBox.remove(checkBox.getText().toString());
                } else {
                    checkBox.setChecked(true);
                    checkBox.setCheckMarkDrawable(R.drawable.yes_no);
                    listAnswerCheckBox.add(checkBox.getText().toString());
                }
                if (listAnswerCheckBox.size() > 0) {
                    btnNext.setVisibility(View.VISIBLE);
                } else {
                    btnNext.setVisibility(View.GONE);
                }
            }
        });

        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.setMargins(Math.round(50 * scale), 0, Math.round(50 * scale), Math.round(50 * scale));
        checkBox.setLayoutParams(params);
        checkBox.setPadding(30, 0, 30, 0);
        checkBox.setTextColor(getResources().getColorStateList(R.color.color_text_change));
        checkBox.setText(listOption.get(i).getName());
        checkBox.setGravity(Gravity.LEFT);
        linearLayoutRoot.addView(checkBox);
    }
}
