package com.boxyzvn.vndrisc.activity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.utils.UIHelper;

public class WebviewActivity extends BaseActivity {
    public static final String PREVENTION_LOW = "http://vndrisc.boxyzvn.com/prevention-low";
    public static final String PREVENTION_LIGHT = "http://vndrisc.boxyzvn.com/prevention-light";
    public static final String PREVENTION_MEDIUM = "http://vndrisc.boxyzvn.com/prevention-medium";
    public static final String PREVENTION_HIGH = "http://vndrisc.boxyzvn.com/prevention-high";
    public static final String PREVENTION_VERY_HIGH = "http://vndrisc.boxyzvn.com/prevention-very-high";
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        mLayoutId = R.layout.activity_webview;
        super.onCreate(savedInstanceState);
        UIHelper.showProgress(this);
        String requestUrl = getIntent().getStringExtra(REQUEST_URL);
        String title = getIntent().getStringExtra(REQUEST_TITLE);
        //setTextTitle(title, false);
        webView = findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(webViewClient);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.clearCache(true);
        webView.loadUrl(requestUrl);
    }

    private WebViewClient webViewClient = new WebViewClient() {

        @Override
        public void onPageFinished(WebView view, String url) {
            if(view.getTitle().contains("/post")){
                setTextTitle("", false);
            }
            else {
                setTextTitle(view.getTitle(), false);

            }
            UIHelper.hideProgress();

        }

    };
}
