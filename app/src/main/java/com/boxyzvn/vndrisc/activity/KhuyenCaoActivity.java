package com.boxyzvn.vndrisc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.model.ObjRecommend;
import com.boxyzvn.vndrisc.model.ObjSumaryRuleExplain;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.boxyzvn.vndrisc.activity.SanLocActivity.JSON_CHUA_CHAN_DOAN;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.SUMARY_OBJ;
import static com.boxyzvn.vndrisc.activity.SanLocActivity.TOTAL_SCORE;

public class KhuyenCaoActivity extends BaseActivity {

    String jsonChuaChanDoan;
    private int totalScore = 0;
    private ObjSumaryRuleExplain objSumary;
    private TextView tvLV1, tvLV2, tvLV3, tvLV4, tvLV5, tvPoint, tvLoiKhuyen, tvTitleKhuyenCao;
    private LinearLayout layoutLine;
    private Button btnDaRo, btnKiemtraLai;
    ArrayList<ObjRecommend> objRecommend = new ArrayList<>();
    String recommend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_khuyen_cao;
        super.onCreate(savedInstanceState);
        //setTextTitle("Khám sàn lọc", false);
        jsonChuaChanDoan = getIntent().getStringExtra(JSON_CHUA_CHAN_DOAN);
        if (jsonChuaChanDoan != null) {
            JSONObject jsonObject = null;
            objSumary = (ObjSumaryRuleExplain) getIntent().getSerializableExtra(SUMARY_OBJ);
            totalScore = getIntent().getIntExtra(TOTAL_SCORE, 0);
            try {
                jsonObject = new JSONObject(jsonChuaChanDoan);
                JSONArray dataSumary = jsonObject.getJSONArray("recommendations");
                Type typeRule = new TypeToken<ArrayList<ObjRecommend>>() {
                }.getType();
                objRecommend = new Gson().fromJson(dataSumary.toString(), typeRule);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            SharePref.getInstance().saveObjecSummary(objSumary);
            SharePref.getInstance().setPoint(totalScore);
        }
        else {
            objSumary = SharePref.getInstance().getObjecSummary();
            totalScore = SharePref.getInstance().getPoint();
            recommend = SharePref.getInstance().getRecommend();
        }

        tvPoint = findViewById(R.id.tvPoint);
        tvLV1 = findViewById(R.id.tvLv1);
        tvLV2 = findViewById(R.id.tvLv2);
        tvLV3 = findViewById(R.id.tvLv3);
        tvLV4 = findViewById(R.id.tvLv4);
        tvLV5 = findViewById(R.id.tvLv5);
        btnDaRo = findViewById(R.id.btnDaRo);
        btnKiemtraLai = findViewById(R.id.btnKiemtraLai);
        btnDaRo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KhuyenCaoActivity.this, ChuaDuocChanDoanActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        });
        btnKiemtraLai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KhuyenCaoActivity.this, SanLocActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        });

        layoutLine = findViewById(R.id.layoutLine);
        tvLoiKhuyen = findViewById(R.id.tvContentKhuyenCao);
        tvTitleKhuyenCao = findViewById(R.id.tvTitleKhuyenCao);
        tvPoint.setText(totalScore + "  Điểm");

        switch (objSumary.getPoint().getRisk_level_id()) {
            case 1:
                setTextRecommend(tvLV1, R.color.lv1, 0);
                break;
            case 2:
                setTextRecommend(tvLV2, R.color.lv2, 1);
                break;
            case 3:
                setTextRecommend(tvLV3, R.color.lv3, 2);
                break;
            case 4:
                setTextRecommend(tvLV4, R.color.lv4, 3);
                break;
            case 5:
                setTextRecommend(tvLV5, R.color.lv5, 4);
                break;

        }
    }

    private void setTextRecommend(TextView tvLV5, int p, int i) {
        tvLV5.setBackgroundTintList(getColorStateList(p));
        layoutLine.setBackgroundTintList(getColorStateList(p));
        if (objRecommend.size() != 0) {
            recommend = objRecommend.get(i).getInstruction();
            SharePref.getInstance().setRecommend(recommend);
        }
        String text5 = "<font color=#068855>" + "Chỉ dẫn: " + "</font> <font color=#707070>" + recommend + "</font>";
        tvLoiKhuyen.setText(Html.fromHtml(text5));
        String textPercent5 = "<font color=#707070>" + "Nguy cơ mắc bệnh trong 10 năm tới: "
                + "</font> <font color=#068855>" + objSumary.getPoint().getProgression_rate() + "</font>";
        tvTitleKhuyenCao.setText(Html.fromHtml(textPercent5));
    }
}
