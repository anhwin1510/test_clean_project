package com.boxyzvn.vndrisc.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.boxyzvn.vndrisc.R;

import java.util.Random;

import static com.boxyzvn.vndrisc.activity.ChonGioActivity.NOI_DUNG_DEFAULT;
import static com.boxyzvn.vndrisc.activity.ChonGioActivity.NOI_DUNG_NHAC_NHO;

public class NhapThongTinNhacNhoActivity extends BaseActivity {

    private Button btnSave;
    private EditText edtNoiDung;
    private TextView edtDefault;
    private String[] Textlist = {
            "Kiểm tra đường huyết 1-3 lần/tháng",
            "Kiểm tra đường huyết 1-3 lần/tháng",
            "Kiểm tra đường huyết 1-3 lần/tháng",
            "Kiểm tra đường huyết 1-3 lần/tháng",
            "Kiểm tra đường huyết 1-3 lần/tháng"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_nhap_thong_tin_nhac_nho;
        super.onCreate(savedInstanceState);
        btnSave = findViewById(R.id.btnSaveNhacNho);
        edtNoiDung = findViewById(R.id.edtInfoNhacNho);
        edtDefault = findViewById(R.id.edtDefault);
        setTextTitle("Nội dung nhắc nhở", false);
        Intent intent = getIntent();
        String nhacNho = intent.getStringExtra(NOI_DUNG_NHAC_NHO);
        if (!nhacNho.isEmpty()) {
            edtNoiDung.setText(nhacNho);
        }
        if (nhacNho.equals("Nhập lời nhắc")) {
            edtNoiDung.setText("");
            edtNoiDung.setHint("Nhập lời nhắc");
        }
        Random random = new Random();
        String randomText = Textlist[random.nextInt(Textlist.length)];
        edtDefault.setText(randomText);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(NOI_DUNG_NHAC_NHO, edtNoiDung.getText().toString());
                intent.putExtra(NOI_DUNG_DEFAULT, edtDefault.getText().toString());
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }
}
