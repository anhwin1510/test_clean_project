package com.boxyzvn.vndrisc.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.boxyzvn.vndrisc.R;
import com.boxyzvn.vndrisc.adapter.ThongTinVeBenhAdapter;
import com.boxyzvn.vndrisc.model.ObjDiseaseInfo;

import java.util.ArrayList;

public class FoodInfoActivity extends BaseHomeActivity implements ThongTinVeBenhAdapter.ItemClickListener {

    private RecyclerView listFoodInfo;
    private ThongTinVeBenhAdapter mAdapterDiseaseInfo;
    private ArrayList<ObjDiseaseInfo> mArrayDiseaseInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_layout);
        setTextTitle(getString(R.string.phong_chua_benh), false);
        setViewID();
        mArrayDiseaseInfo = new ArrayList<>();
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));
        mArrayDiseaseInfo.add(new ObjDiseaseInfo("Những thứ không nên ăn và những thứ", "Táo nho có hàm lượng vitamin C quá cao so với", "10/9/2019", ""));

        mAdapterDiseaseInfo = new ThongTinVeBenhAdapter(this, mArrayDiseaseInfo);
        listFoodInfo.setAdapter(mAdapterDiseaseInfo);
        mAdapterDiseaseInfo.setClickListener(this);
        listFoodInfo.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setViewID() {
        listFoodInfo = (RecyclerView) findViewById(R.id.listFoodInfo);
    }

    @Override
    public void onItemClick(View view, int position) {
    }
}
