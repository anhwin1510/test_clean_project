package com.boxyzvn.vndrisc.api.callback;

public  interface CallBackLogin{
    void onComplete(String token);
    void onError(String errorMsg);
}