package com.boxyzvn.vndrisc.api.callback;

public  interface CallBack {
    void onComplete();
    void onError(String errorMsg);
}