package com.boxyzvn.vndrisc.api;

public interface ConstantApi { ;
    String BASE_URL_DEV = "http://vndrisc.boxyzvn.com";
    String BASE_URL_LIVE = "https://vndrisc.com";
    String API_LOGIN = "/api/login";
    String API_LOGIN_CTV = "/api/collaborator/login";
    String API_REGISTER = "/api/register";
    String API_REGISTER_CTV = "/api/collaborator/register";
    String API_SECURITY_QUESTIONS = "/api/security-questions";
    String API_CHECK_SECURITY = "/api/check-security";
    String API_RESET_PASSWORD = "/api/reset-password";
    String API_GET_INFO = "/api/info";
    String API_LIST_BENH_NHAN = "/api/patients";
    String API_INFO_DISEASE_LIST = "/api/posts";
    String API_DA_CHAN_DOAN = "/api/diagnosed";
    String API_CHUA_CHAN_DOAN = "/api/undiagnosed";
    String JSON_CHUA_CHAN_DOAN = "/api/json/chuachandoan.json";
    String JSON_DA_CHAN_DOAN = "/api/json/dachandoan.json";

}
