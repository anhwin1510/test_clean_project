package com.boxyzvn.vndrisc.api;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIServiceDao {
    @POST(ConstantApi.API_LOGIN)
    @FormUrlEncoded
    Call<ResponseBase> login(@Field("uuid") String uu_id,
                             @Field("os_type") String os_type, @Field("os_version") String os_ver,
                             @Field("app_ver") String app_ver, @Field("device") String device,
                             @Field("username") String phone, @Field("password") String password);

    @POST(ConstantApi.API_LOGIN_CTV)
    @FormUrlEncoded
    Call<ResponseBase> loginCTV(@Field("uuid") String uu_id,
                                @Field("os_type") String os_type, @Field("os_version") String os_ver,
                                @Field("app_ver") String app_ver, @Field("device") String device,
                                @Field("username") String phone, @Field("password") String password);

    @POST(ConstantApi.API_GET_INFO)
    @FormUrlEncoded
    Call<ResponseBase> getInfo(@Field("uuid") String uu_id,
                               @Field("os_type") String os_type, @Field("os_version") String os_ver,
                               @Field("app_ver") String app_ver, @Field("device") String device,
                               @Field("token") String token);

    @POST(ConstantApi.API_LIST_BENH_NHAN)
    @FormUrlEncoded
    Call<ResponseBase> getListBenhNhan(@Field("uuid") String uu_id,
                                       @Field("os_type") String os_type, @Field("os_version") String os_ver,
                                       @Field("app_ver") String app_ver, @Field("device") String device,
                                       @Field("token") String token, @Field("ward_id") String ward_id,
                                       @Field("residential") String residential, @Field("page") int page);

    @POST(ConstantApi.API_REGISTER)
    @FormUrlEncoded
    Call<ResponseBase> register(@Field("uuid") String uu_id,
                                @Field("os_type") String os_type, @Field("os_version") String os_ver,
                                @Field("app_ver") String app_ver, @Field("device") String device,
                                @Field("phone") String phone, @Field("password") String password,
                                @Field("name") String name, @Field("address") String address,
                                @Field("date_of_birth") String day, @Field("province") String province,
                                @Field("district") String district, @Field("ward") String ward,
                                @Field("residential") String residential,
                                @Field("security_question_id") int security_question_id,
                                @Field("security_answer") String security_answer);

    @POST(ConstantApi.API_REGISTER_CTV)
    @FormUrlEncoded
    Call<ResponseBase> registerCTV(@Field("uuid") String uu_id,
                                   @Field("os_type") String os_type, @Field("os_version") String os_ver,
                                   @Field("app_ver") String app_ver, @Field("device") String device,
                                   @Field("phone") String phone, @Field("password") String password,
                                   @Field("name") String name, @Field("address") String address,
                                   @Field("date_of_birth") String day, @Field("province") String province,
                                   @Field("district") String district, @Field("ward") String ward,
                                   @Field("residential") String residential, @Field("code") String code);

    @POST(ConstantApi.API_INFO_DISEASE_LIST)
    @FormUrlEncoded
    Call<ResponseBase> infoDiseaseList(@Field("uuid") String uu_id,
                                       @Field("os_type") String os_type,
                                       @Field("os_version") String os_ver,
                                       @Field("app_ver") String app_ver,
                                       @Field("device") String device,
                                       @Field("token") String token,
                                       @Field("page") int page);

    @POST(ConstantApi.API_DA_CHAN_DOAN)
    @FormUrlEncoded
    Call<ResponseBase> sendDaChanDoan(@Field("uuid") String uu_id,
                                      @Field("os_type") String os_type,
                                      @Field("os_version") String os_ver,
                                      @Field("app_ver") String app_ver,
                                      @Field("device") String device,
                                      @Field("token") String token,
                                      @Field("answers") String answers);

    @POST(ConstantApi.API_CHUA_CHAN_DOAN)
    @FormUrlEncoded
    Call<ResponseBase> sendChuaChanDoan(@Field("uuid") String uu_id,
                                        @Field("os_type") String os_type,
                                        @Field("os_version") String os_ver,
                                        @Field("app_ver") String app_ver,
                                        @Field("device") String device,
                                        @Field("token") String token,
                                        @Field("answers") String answers,
                                        @Field("point") int point);

    @POST(ConstantApi.JSON_CHUA_CHAN_DOAN)
    @FormUrlEncoded
    Call<ResponseBody> getJsonChuaChanDoan(@Field("uuid") String uu_id,
                                           @Field("os_type") String os_type,
                                           @Field("os_version") String os_ver,
                                           @Field("app_ver") String app_ver,
                                           @Field("device") String device,
                                           @Field("token") String token);

    @POST(ConstantApi.JSON_DA_CHAN_DOAN)
    @FormUrlEncoded
    Call<ResponseBody> getJsonDaChanDoan(@Field("uuid") String uu_id,
                                         @Field("os_type") String os_type,
                                         @Field("os_version") String os_ver,
                                         @Field("app_ver") String app_ver,
                                         @Field("device") String device,
                                         @Field("token") String token);

    @POST(ConstantApi.API_SECURITY_QUESTIONS)
    @FormUrlEncoded
    Call<ResponseBase> infoSecurityQuestion(@Field("uuid") String uu_id,
                                            @Field("os_type") String os_type,
                                            @Field("os_version") String os_ver,
                                            @Field("app_ver") String app_ver,
                                            @Field("device") String device);

    @POST(ConstantApi.API_CHECK_SECURITY)
    @FormUrlEncoded
    Call<ResponseBase> checkSecurity(@Field("uuid") String uu_id,
                                     @Field("os_type") String os_type,
                                     @Field("os_version") String os_ver,
                                     @Field("app_ver") String app_ver,
                                     @Field("device") String device,
                                     @Field("phone") String phone,
                                     @Field("security_question_id") int security_question_id,
                                     @Field("security_answer") String security_answer);

    @POST(ConstantApi.API_RESET_PASSWORD)
    @FormUrlEncoded
    Call<ResponseBase> resetPass(@Field("uuid") String uu_id,
                                 @Field("os_type") String os_type,
                                 @Field("os_version") String os_ver,
                                 @Field("app_ver") String app_ver,
                                 @Field("device") String device,
                                 @Field("phone") String phone,
                                 @Field("security_code") int security_code,
                                 @Field("new_password") String new_password,
                                 @Field("conf_password") String conf_password);
}
