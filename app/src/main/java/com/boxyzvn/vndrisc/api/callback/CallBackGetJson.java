package com.boxyzvn.vndrisc.api.callback;

import com.boxyzvn.vndrisc.model.ObjDiseaseInfo;

import org.json.JSONObject;

import java.util.ArrayList;

public interface CallBackGetJson {
    void onComplete(JSONObject json);
    void onError(String errorMsg);
}