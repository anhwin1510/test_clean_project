package com.boxyzvn.vndrisc.api;


import android.os.Build;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.boxyzvn.vndrisc.api.callback.CallBack;
import com.boxyzvn.vndrisc.api.callback.CallBackCategorySecurityQuestion;
import com.boxyzvn.vndrisc.api.callback.CallBackCheck;
import com.boxyzvn.vndrisc.api.callback.CallBackGetJson;
import com.boxyzvn.vndrisc.api.callback.CallBackInfoDiseaseList;
import com.boxyzvn.vndrisc.api.callback.CallBackListPatients;
import com.boxyzvn.vndrisc.api.callback.CallBackLogin;
import com.boxyzvn.vndrisc.model.CategorySecurityQuestion;
import com.boxyzvn.vndrisc.model.ObjDiseaseInfo;
import com.boxyzvn.vndrisc.model.UserInfo;
import com.boxyzvn.vndrisc.sharedpreferences.SharePref;
import com.boxyzvn.vndrisc.utils.UtilJson;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.boxyzvn.vndrisc.utils.UtilJson.toJson;


public class ApiServiceManager extends AppCompatActivity {
    public static final String DANANG = "Đà Nẵng";
    private static ApiServiceManager INSTANCE;
    public static final String SALT_STRING = "@4%a4vN90Zjkzz1-1";
    private APIServiceDao mApiServiceDao;
    // other instance variables can be here

    private ApiServiceManager() {
        mApiServiceDao = new RetrofitClient().getAPIServiceBase();
    }

    public static ApiServiceManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ApiServiceManager();
        }
        return INSTANCE;
    }

    public void login(final String phone, final String pass, final String uuid, final String appVer, final CallBackLogin callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        Log.d("ApiServiceManager",
                model + "|"
                        + appVer + "|"
                        + uuid + "|"
                        + phone + "|"
                        + pass + "|"
                        + os_ver + "|");
        mApiServiceDao.login(uuid, "1", os_ver, appVer, model, phone, pass).enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                if (response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        jObjError.getString("err_msg");
                        callBack.onError(jObjError.getString("err_msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // callBack.onError(response.errorBody().toString());
                } else {
                    if (response.code() == 200) {
                        Gson gson = new Gson();
                        try {
                            JSONObject jsonObject = new JSONObject(toJson(response.body()));
                            String token = jsonObject.getString("data");
                            Log.d("ApiServiceManager", " token: " + token);
                            callBack.onComplete(token);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void loginCTV(final String phone, final String pass, final String uuid, final String appVer, final CallBackLogin callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        Log.d("ApiServiceManager",
                model + "|"
                        + appVer + "|"
                        + uuid + "|"
                        + phone + "|"
                        + pass + "|"
                        + os_ver + "|");
        mApiServiceDao.loginCTV(uuid, "1", os_ver, appVer, model, phone, pass).enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                if (response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        jObjError.getString("err_msg");
                        callBack.onError(jObjError.getString("err_msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // callBack.onError(response.errorBody().toString());
                } else {
                    if (response.code() == 200) {
                        Gson gson = new Gson();
                        try {
                            JSONObject jsonObject = new JSONObject(toJson(response.body()));
                            JSONObject objectData = jsonObject.getJSONObject("data");
                            String token = objectData.getString("token");
                            Log.d("ApiServiceManager", " token: " + token);
                            callBack.onComplete(token);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getUserInfo(final String token, final String uuid, final String appVer, final CallBack callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        Log.d("ApiServiceManager",
                model + "|"
                        + appVer + "|"
                        + uuid + "|"
                        + token + "|"
                        + os_ver + "|");
        mApiServiceDao.getInfo(uuid, "1", os_ver, appVer, model, token).enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                if (response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        jObjError.getString("err_msg");
                        callBack.onError(jObjError.getString("err_msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    }
                    // callBack.onError(response.errorBody().toString());
                } else {
                    if (response.code() == 200) {
                        Gson gson = new Gson();
                        try {
                            JSONObject jsonObject = new JSONObject(toJson(response.body()));
                            UserInfo userInfo = new Gson().fromJson(jsonObject.getString("data"), new TypeToken<UserInfo>() {
                            }.getType());
                            SharePref.getInstance().saveInfoLogin(userInfo);
                            Log.d("ApiServiceManager ", "getUserInfo" + userInfo.getName());
                            callBack.onComplete();
                        } catch (JSONException e) {
                            callBack.onError("Loading Failure");
                            e.printStackTrace();
                        }
                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getListBenhNhan(final String token, final String uuid, final String appVer, int currentPage, String wardID, final CallBackListPatients callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        Log.d("ApiServiceManager",
                model + "|"
                        + appVer + "|"
                        + uuid + "|"
                        + token + "|"
                        + os_ver + "|");
        mApiServiceDao.getListBenhNhan(uuid, "1", os_ver, appVer, model, token, wardID, "", currentPage).enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                if (response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        jObjError.getString("err_msg");
                        callBack.onError(jObjError.getString("err_msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    }
                    // callBack.onError(response.errorBody().toString());
                } else {
                    if (response.code() == 200) {
                        Gson gson = new Gson();
                        try {
                            JSONObject jsonObject = new JSONObject(toJson(response.body()));
                            JSONObject jsonData = new JSONObject(jsonObject.getString("data"));

                            JSONArray jsonArray = jsonData.getJSONArray("users");
                            ArrayList<UserInfo> userInfo = new Gson().fromJson(jsonArray.toString(), new TypeToken<ArrayList<UserInfo>>() {
                            }.getType());

                            Log.d("ApiServiceManager ", "list benh nhan" + userInfo.toString());
                            Log.d("ApiServiceManager ", "list benh nhan" + jsonData.getInt("last_page"));
                            callBack.onComplete(userInfo, jsonData.getInt("last_page"));
                        } catch (JSONException e) {
                            callBack.onError("Loading Failure");
                            e.printStackTrace();
                        }
                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void register(UserInfo userInfo, String appVer, String androidId, final CallBack callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        Log.d("ApiServiceManager",
                model + "|"
                        + appVer + "|"
                        + androidId + "|"
                        + model + "|"
                        + os_ver + "|"
                        + new Gson().toJson(userInfo));

        mApiServiceDao.register(androidId, "1", os_ver, appVer, model, userInfo.getPhone(),
                userInfo.getPass(), userInfo.getName(), userInfo.getAddress(),
                userInfo.getDate_of_birth(), DANANG, userInfo.getDistrict(), userInfo.getWard(),
                userInfo.getResidential(), userInfo.getSecurityId(), userInfo.getSecurityAnswer()).enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                if (response.errorBody() != null) {
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        jObjError.getString("err_msg");
                        callBack.onError(jObjError.getString("err_msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    if (response.code() == 200) {
                        Gson gson = new Gson();
                        try {
                            JSONObject jsonObject = new JSONObject(toJson(response.body()));
                            jsonObject.toString();
                            Log.d("ApiServiceManager", jsonObject.toString());
                            callBack.onComplete();
                        } catch (JSONException e) {
                            callBack.onError("Loading Failure");
                            e.printStackTrace();
                        }
                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void registerCTV(UserInfo userInfo, String appVer, String androidId, final CallBack callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        Log.d("ApiServiceManager",
                model + "|"
                        + appVer + "|"
                        + androidId + "|"
                        + model + "|"
                        + os_ver + "|"
                        + new Gson().toJson(userInfo));

        mApiServiceDao.registerCTV(androidId, "1", os_ver, appVer, model, userInfo.getPhone(), userInfo.getPass(),
                userInfo.getName(),
                userInfo.getAddress(),
                userInfo.getDate_of_birth(),
                DANANG,
                userInfo.getDistrict(),
                userInfo.getWard(),
                userInfo.getResidential(),
                userInfo.getCode())
                .enqueue(new Callback<ResponseBase>() {
                    @Override
                    public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                        if (response.errorBody() != null) {
                            JSONObject jObjError = null;
                            try {
                                jObjError = new JSONObject(response.errorBody().string());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            try {
                                jObjError.getString("err_msg");
                                callBack.onError(jObjError.getString("err_msg"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            if (response.code() == 200) {
                                Gson gson = new Gson();
                                try {
                                    JSONObject jsonObject = new JSONObject(toJson(response.body()));
                                    jsonObject.toString();
                                    Log.d("ApiServiceManager", jsonObject.toString());
                                    callBack.onComplete();
                                } catch (JSONException e) {
                                    callBack.onError("Loading Failure");
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    callBack.onError(new JSONObject(response.errorBody().string()).getString("err_msg"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBase> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    public void getInfoDiseaseList(final String token, final String uuid, final String appVer,
                                   int currentPage, final CallBackInfoDiseaseList callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        Log.d("ApiServiceManager",
                model + "|"
                        + appVer + "|"
                        + uuid + "|"
                        + token + "|"
                        + os_ver + "|");
        mApiServiceDao.infoDiseaseList(uuid, "1", os_ver, appVer, model, token, currentPage).enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                if (response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        jObjError.getString("err_msg");
                        callBack.onError(jObjError.getString("err_msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    }
                    // callBack.onError(response.errorBody().toString());
                } else {
                    if (response.code() == 200) {
                        try {
                            JSONObject jsonObject = new JSONObject(toJson(response.body()));
                            JSONObject jsonData = new JSONObject(jsonObject.getString("data"));
                            JSONArray jsonArray = jsonData.getJSONArray("list");

                            ArrayList<ObjDiseaseInfo> list = new Gson().fromJson(jsonArray.toString(),
                                    new TypeToken<ArrayList<ObjDiseaseInfo>>() {
                                    }.getType());

                            Log.d("ApiServiceManager ", "list info disease" + list.toString());
                            callBack.onComplete(list, jsonData.getInt("current_page"), jsonData.getInt("last_page"));
                        } catch (JSONException e) {
                            callBack.onError("Loading Failure");
                            e.printStackTrace();
                        }
                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void sendDaChanDoan(final String token, final String uuid, final String appVer,
                               String data, final CallBack callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        Log.d("ApiServiceManager",
                model + "|"
                        + appVer + "|"
                        + uuid + "|"
                        + token + "|"
                        + os_ver + "|");
        mApiServiceDao.sendDaChanDoan(uuid, "1", os_ver, appVer, model, token, data).enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                if (response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        jObjError.getString("err_msg");
                        callBack.onError(jObjError.getString("err_msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    }
                    // callBack.onError(response.errorBody().toString());
                } else {
                    if (response.code() == 200) {
//                            JSONObject jsonObject = new JSONObject(UtilJson.toJson(response.body()));
//                            JSONObject jsonData = new JSONObject(jsonObject.getString("data"));
//                            JSONArray jsonArray = jsonData.getJSONArray("list");
//
//                            ArrayList<ObjDiseaseInfo> list = new Gson().fromJson(jsonArray.toString(),
//                                    new TypeToken<ArrayList<ObjDiseaseInfo>>() {
//                                    }.getType());
//
//                            Log.d("ApiServiceManager ", "list info disease" + list.toString());
                        callBack.onComplete();
                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void sendChuaChanDoan(final String token, final String uuid, final String appVer,
                                 String data, int point, final CallBack callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        Log.d("ApiServiceManager",
                model + "|"
                        + appVer + "|"
                        + uuid + "|"
                        + token + "|"
                        + os_ver + "|");
        mApiServiceDao.sendChuaChanDoan(uuid, "1", os_ver, appVer, model, token, data, point).enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                if (response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        jObjError.getString("err_msg");
                        callBack.onError(jObjError.getString("err_msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    }
                    // callBack.onError(response.errorBody().toString());
                } else {
                    if (response.code() == 200) {
//                            JSONObject jsonObject = new JSONObject(UtilJson.toJson(response.body()));
//                            JSONObject jsonData = new JSONObject(jsonObject.getString("data"));
//                            JSONArray jsonArray = jsonData.getJSONArray("list");
//
//                            ArrayList<ObjDiseaseInfo> list = new Gson().fromJson(jsonArray.toString(),
//                                    new TypeToken<ArrayList<ObjDiseaseInfo>>() {
//                                    }.getType());
//
//                            Log.d("ApiServiceManager ", "list info disease" + list.toString());
                        callBack.onComplete();
                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getJSONDaChanDoan(final String token, final String uuid, final String appVer,
                                  final CallBackGetJson callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        Log.d("ApiServiceManager",
                model + "|"
                        + appVer + "|"
                        + uuid + "|"
                        + token + "|"
                        + os_ver + "|");
        mApiServiceDao.getJsonDaChanDoan(uuid, "1", os_ver, appVer, model, token).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        JSONObject json = new JSONObject(response.body().string());
                        callBack.onComplete(json);
                        Log.d("ApiServiceManager ", "JSON da chan doan" + json.toString());
                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }

        });
    }

    public void getJSONChuaChanDoan(final String token, final String uuid, final String appVer,
                                    final CallBackGetJson callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        Log.d("ApiServiceManager",
                "chua chan doan \n" + model + "|"
                        + appVer + "|"
                        + uuid + "|"
                        + token + "|"
                        + os_ver + "|");
        mApiServiceDao.getJsonChuaChanDoan(uuid, "1", os_ver, appVer, model, token).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("ApiServiceManager ", "JSON chua chan doan" + json.toString());
                        callBack.onComplete(json);

                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }

        });
    }

    public void getInfoSecurityQuestions(final String uuid, final String appVer,
                                         final CallBackCategorySecurityQuestion callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        mApiServiceDao.infoSecurityQuestion(uuid, "1", os_ver, appVer, model).enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                ArrayList<CategorySecurityQuestion> list = new ArrayList<>();
                if (response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        jObjError.getString("err_msg");
                        callBack.onError(jObjError.getString("err_msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    }
                } else {
                    if (response.code() == 200) {
                        Gson gson = new Gson();
                        CategorySecurityQuestion[] a = gson.fromJson(toJson(response.body().data),
                                CategorySecurityQuestion[].class);
                        if (a.length > 0) {
                            for (CategorySecurityQuestion obj : a) {
                                list.add(obj);
                            }
                        }
                        callBack.onComplete(list);
                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void checkSecurityQuestion(final String uuid, final String appVer, final String phone,
                                      final int securityID, final String securityAnswer,
                                      final CallBackCheck callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        mApiServiceDao.checkSecurity(uuid, "1", os_ver, appVer, model,
                phone, securityID, securityAnswer).enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                if (response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        jObjError.getString("err_msg");
                        callBack.onError(jObjError.getString("err_msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    }
                } else {
                    if (response.code() == 200) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(toJson(response.body().data));
                            int code = jsonObject.getInt("code");
                            callBack.onComplete(code);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            callBack.onError(response.errorBody().toString());
                        }
                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void resetPass(final String uuid, final String appVer, final String phone,
                          final int securityCode, final String newPass, final String confirmPass,
                          final CallBack callBack) {
        String model = Build.MODEL;
        String os_ver = Build.VERSION.RELEASE;
        mApiServiceDao.resetPass(uuid, "1", os_ver, appVer, model,
                phone, securityCode, newPass, confirmPass).enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                if (response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        jObjError.getString("err_msg");
                        callBack.onError(jObjError.getString("err_msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                        callBack.onError(e.toString());
                    }
                } else {
                    if (response.code() == 200) {
                        callBack.onComplete();
                    } else {
                        callBack.onError(response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
