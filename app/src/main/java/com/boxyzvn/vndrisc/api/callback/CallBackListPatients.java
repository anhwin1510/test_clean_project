package com.boxyzvn.vndrisc.api.callback;

import com.boxyzvn.vndrisc.model.UserInfo;

import java.util.ArrayList;

public  interface CallBackListPatients {
    void onComplete(ArrayList<UserInfo> userInfoArrayList, int lastPage);
    void onError(String errorMsg);
}