package com.boxyzvn.vndrisc.api.callback;

import com.boxyzvn.vndrisc.model.CategorySecurityQuestion;
import com.boxyzvn.vndrisc.model.ObjDiseaseInfo;

import java.util.ArrayList;

public interface CallBackCategorySecurityQuestion {
    void onComplete(ArrayList<CategorySecurityQuestion> list);

    void onError(String errorMsg);
}