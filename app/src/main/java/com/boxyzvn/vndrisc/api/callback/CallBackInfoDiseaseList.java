package com.boxyzvn.vndrisc.api.callback;

import com.boxyzvn.vndrisc.model.ObjDiseaseInfo;

import java.util.ArrayList;

public interface CallBackInfoDiseaseList {
    void onComplete(ArrayList<ObjDiseaseInfo> userInfoArrayList, int pageCurent, int lastPage);

    void onError(String errorMsg);
}