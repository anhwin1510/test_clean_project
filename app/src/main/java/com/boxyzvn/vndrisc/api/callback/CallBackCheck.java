package com.boxyzvn.vndrisc.api.callback;

public  interface CallBackCheck {
    void onComplete(int securityCode);
    void onError(String errorMsg);
}